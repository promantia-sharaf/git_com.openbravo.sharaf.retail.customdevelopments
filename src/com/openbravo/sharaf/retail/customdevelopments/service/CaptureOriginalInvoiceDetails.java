/*
 ************************************************************************************
 * Copyright (C) 2012-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.customdevelopments.service;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.service.json.JsonConstants;

public class CaptureOriginalInvoiceDetails extends JSONProcessSimple {
  
  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {

    JSONObject result = new JSONObject();
    JSONObject response = new JSONObject();
    JSONArray listpaidReceiptsLines = new JSONArray();

    try {
      OBContext.setAdminMode(true);
      String clientId = OBContext.getOBContext().getCurrentClient().getId();
      String orderid = jsonsent.getString("orderid");
      List<OrderLine> orderLineList = new ArrayList<OrderLine>();
      String hql = " FROM OrderLine p WHERE p.salesOrder.id = :orderid AND p.client.id = :clientId"
          + " AND p.custshaOriginvoicecaptured = :isOrigInvCaptured AND p.obposIsDeleted = :obposIsDeleted "
          + " AND p.active = :isactive ORDER BY p.lineNo";
      Query query = OBDal.getInstance().getSession().createQuery(hql);
      query.setParameter("orderid", orderid);
      query.setParameter("clientId", clientId);
      query.setParameter("isOrigInvCaptured", false);
      query.setParameter("obposIsDeleted", false);
      query.setParameter("isactive", true);
      orderLineList = query.list();
      if (!orderLineList.isEmpty()) {
        for (int i = 0; i < orderLineList.size(); i++) {
          JSONObject method = new JSONObject();
          method.put("lineId", orderLineList.get(i).getId());
          method.put("lineNo", orderLineList.get(i).getLineNo().toString());
          method.put("obposSerialNumber", orderLineList.get(i).getObposSerialNumber());
          method.put("productname", orderLineList.get(i).getProduct().getName());
          method.put("originalInvoiceNo",  orderLineList.get(i).getSalesOrder().getDocumentNo());
          listpaidReceiptsLines.put(method);
        }
      }
      response.put("data", listpaidReceiptsLines);
      result.put(JsonConstants.RESPONSE_DATA, response);
      result.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
    } catch (Exception e) {
      result.put(JsonConstants.RESPONSE_ERROR, e.getMessage());
      result.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_FAILURE);
    } finally {
      OBContext.restorePreviousMode();
    }
    return result;
  }
}
