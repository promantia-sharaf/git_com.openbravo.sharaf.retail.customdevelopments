package com.openbravo.sharaf.retail.customdevelopments.service;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.mobile.core.process.ProcessHQLQuery;

public class InitialFloatAmountDeposit extends ProcessHQLQuery {

  private static Logger log = Logger.getLogger(InitialFloatAmountDeposit.class);

  @Inject
  @Any

  private Instance<ModelExtension> extensions;

  @Override
  protected List<String> getQuery(JSONObject jsonsent) throws JSONException {
    String cashupid = jsonsent.getString("cashupid");
    String finAccountid = jsonsent.getString("financcountid");
    String glfloatid = jsonsent.getString("glitemid");

    List<String> hqlQueries = new ArrayList<String>();

    String hql = "select fin.depositAmount from FIN_Finacc_Transaction as fin where fin.obposAppCashup = '"
        + cashupid + "' and fin.account ='" + finAccountid + "' and fin.gLItem ='" + glfloatid
        + "'order by fin.creationDate desc ";

    log.info("hql query" + hql);
    hqlQueries.add(hql);
    return hqlQueries;
  }
}
