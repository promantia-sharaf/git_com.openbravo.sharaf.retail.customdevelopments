package com.openbravo.sharaf.retail.customdevelopments.service;

import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;


class AccountCodeForPaymentMethods extends JSONProcessSimple {

	 private static Logger log = Logger.getLogger(AccountCodeForPaymentMethods.class);
	
    
	@Override
	public JSONObject exec(JSONObject jsonsent) throws JSONException,
			ServletException {
		// TODO Auto-generated method stub
		JSONObject result = new JSONObject();
	    JSONObject data = new JSONObject();
		
	    try {
	    	OBContext.setAdminMode(true);	    	
	    	HashMap<String, HashMap<String, String>> map = new HashMap<String, HashMap<String, String>>();
	    	HashMap<String, String> map2 = null;
			OBCriteria<FIN_PaymentMethod> obCriteriaPM = OBDal.getInstance()
					.createCriteria(FIN_PaymentMethod.class);
			obCriteriaPM.add(Restrictions.eq(FIN_PaymentMethod.PROPERTY_ORGANIZATION,
					OBDal.getInstance().get(Organization.class, "0")));
			List<FIN_PaymentMethod> paymentMethodList = obCriteriaPM.list();
			if (!paymentMethodList.isEmpty()) {
				for (int i=0 ; i < paymentMethodList.size(); i++) {
					map2 = new HashMap<String, String>();
					if (paymentMethodList.get(i).getCustshaAccountcode()!= null && (!paymentMethodList.get(i).getCustshaAccountcode().isEmpty())) {
						OBCriteria<BusinessPartner> bpCriteria = OBDal.getInstance().createCriteria(BusinessPartner.class);
						bpCriteria.add(Restrictions.eq(BusinessPartner.PROPERTY_SEARCHKEY, paymentMethodList.get(i).getCustshaAccountcode()));
						List<BusinessPartner> bpList = bpCriteria.list();
						if (!bpList.isEmpty()) {
							map2.put(paymentMethodList.get(i).getCustshaAccountcode(), bpList.get(0).getId());
						} else {
							map2.put(paymentMethodList.get(i).getCustshaAccountcode(), "");
						}
					}
					map.put(paymentMethodList.get(i).getId(), map2);
				}
			}
	    	    	
	    	data.put("AccountCode", map);    
		    result.put("status", 0);
		    result.put("data", data);
	    } catch (Exception e) {
	      log.error("Error getting Account Code for Payment Methods: " + e.getMessage(), e);
	      result.put("status", 1);
	      result.put("message", e.getMessage());
	    } finally {
	        OBContext.restorePreviousMode();
	    }

	    return result;	
	}
}