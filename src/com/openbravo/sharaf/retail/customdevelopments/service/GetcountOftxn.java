package com.openbravo.sharaf.retail.customdevelopments.service;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.mobile.core.process.ProcessHQLQuery;

public class GetcountOftxn extends ProcessHQLQuery {

  private static Logger log = Logger.getLogger(GetcountOftxn.class);
  @Inject
  @Any

  private Instance<ModelExtension> extensions;

  @Override
  protected List<String> getQuery(JSONObject jsonsent) throws JSONException {
    String posterminal = jsonsent.getString("posterminal");

    List<String> hqlQueries = new ArrayList<String>();

    String hql = "select txn.count from custsha_transcount as txn where txn.obposApplications = '"
        + posterminal + "'order by txn.creationDate desc limit 1";

    hqlQueries.add(hql);
    return hqlQueries;
  }

}
