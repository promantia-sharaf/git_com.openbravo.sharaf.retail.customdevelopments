package com.openbravo.sharaf.retail.customdevelopments.service;



import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.retail.posterminal.ProcessHQLQuery;

class RestrictPaymentMethodConfigModel extends ProcessHQLQuery {
  
  // private static Logger log = Logger.getLogger(RestrictPaymentMethodConfigModel.class);
  
  @Override
  protected List<String> getQuery(JSONObject jsonsent) throws JSONException {
    List<String> hqlQueries = new ArrayList<String>();
    String hqlQuery = "select cpm.id as id, cpm.client.id as clientId, cpm.organization.id as orgId, " +
        " cpm.product.id as productId, cpm.fINPaymentmethod.id as paymtId,  cpm.active as active " + 
        " from CUSTSHA_PaymentMethod cpm "
        + " where cpm.active = 'Y'";
    hqlQueries.add(hqlQuery);
    // log.info(hqlQuery);
    return hqlQueries;
  }
}

