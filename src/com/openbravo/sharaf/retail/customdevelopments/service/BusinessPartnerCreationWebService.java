package com.openbravo.sharaf.retail.customdevelopments.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.ad.utility.Sequence;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Category;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.geography.City;
import org.openbravo.model.common.geography.Country;
import org.openbravo.model.common.geography.Region;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.PaymentTerm;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.service.json.JsonConstants;
import org.openbravo.service.web.WebService;

public class BusinessPartnerCreationWebService implements WebService {
  private static final Logger log = Logger.getLogger(BusinessPartnerCreationWebService.class);
  private static String phoneNo = "phoneNo";
  private static final String SAP_STORES_BUSINESSPARTNER_SEQ = "SAP_BusinessPartnerSequence";
  JSONObject responseJson = new JSONObject();

  @Override
  public void doPost(String path, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    final JSONObject result = new JSONObject();
    try {
      OBContext.setAdminMode(false);
      String requestBody = readRequestBody(request);
      JSONObject jsonObject = extractJSONObject(requestBody);

      // validation of input data
      validateUpdateRecordInput(jsonObject);

      // Extract fields from the JSON object
      String client = jsonObject.getString("client");
      String organization = jsonObject.getString("organization");

      String searchKey = getSearchKey(jsonObject);
      // Check if a record with the provided fields exists in the business partner table
      BusinessPartner recordExists = checkRecordExists(client, organization, searchKey);
      if (recordExists != null) {
        JSONObject objResponse = updateRecords(jsonObject);
        result.put("Response", objResponse);
      } else {
        JSONObject objResponse = insertRecord(jsonObject, searchKey);
        result.put("Response", objResponse);
      }
      OBDal.getInstance().flush();
      response.setContentType(JsonConstants.JSON_CONTENT_TYPE);
      response.setHeader("Content-Type", JsonConstants.JSON_CONTENT_TYPE);
      response.getWriter().write(result.toString());
    } catch (OBException e) {
      log.error(e.getMessage(), e);
      result.put("error", e.getMessage());
      response.setContentType(JsonConstants.JSON_CONTENT_TYPE);
      response.setHeader("Content-Type", JsonConstants.JSON_CONTENT_TYPE);
      response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
      response.getWriter().write(result.toString());
      return;
    } finally {
      OBContext.restorePreviousMode();
    }
  }

  private String readRequestBody(HttpServletRequest request) throws IOException {
    StringBuilder strBuilder = new StringBuilder();
    try (BufferedReader bReader = request.getReader()) {
      String line;
      while ((line = bReader.readLine()) != null) {
        strBuilder.append(line);
      }
    }
    return strBuilder.toString();
  }

  private JSONObject extractJSONObject(String requestBody) throws JSONException {
    JSONArray jsonArray = new JSONArray(requestBody);
    return jsonArray.getJSONObject(0);
  }

  private String getSearchKey(JSONObject jsonObject) throws Exception {
    String bpNextSequenceNumber = null;
    try {
      String sequenceName = SAP_STORES_BUSINESSPARTNER_SEQ;
      OBCriteria<Sequence> obcSeq = OBDal.getInstance().createCriteria(Sequence.class);
      obcSeq.add(Restrictions.eq(Sequence.PROPERTY_NAME, sequenceName));
      obcSeq.setFilterOnActive(false);
      obcSeq.setFilterOnReadableOrganization(false);
      List<Sequence> seqList = obcSeq.list();
      if (jsonObject.has("searchKey")) {
        bpNextSequenceNumber = jsonObject.getString("searchKey");
      } else {
        if (seqList.size() == 1) {
          bpNextSequenceNumber = FIN_Utility.getDocumentNo(true, seqList.get(0));
        } else if (seqList.size() > 1) {
          String errorMessage = "There is more than one sequence with " + sequenceName
              + " name in the System";
          throw new OBException(errorMessage);
        } else {
          String errorMessage = "There is no sequence with " + sequenceName + " name in the System";
          throw new OBException(errorMessage);
        }
      }
    } catch (OBException e) {
      log.error(e.getMessage(), e);
      throw new OBException("Error while fetching searchKey", e);
    }
    return bpNextSequenceNumber;
  }

  private BusinessPartner checkRecordExists(String client, String organization, String searchKey) {
    Query query = OBDal.getInstance().getSession().createQuery(
        "FROM BusinessPartner bp WHERE bp.client.name = :client and bp.organization.name = :organization and bp.searchKey = :searchKey");
    query.setParameter("client", client);
    query.setParameter("organization", organization);
    query.setParameter("searchKey", searchKey);
    List results = query.list();
    if (!results.isEmpty()) {
      return (BusinessPartner) results.get(0);
    }
    return null;
  }

  private JSONObject insertRecord(JSONObject jsonObject, String searchKey) throws Exception {
    try {
      BusinessPartner businessPartner = createBusinessPartner(jsonObject, searchKey);
      saveLocations(jsonObject.getJSONArray("locations"), businessPartner);
      saveContactInformation(jsonObject.getJSONArray("contactInformation"), businessPartner);

      String businessPartnerId = businessPartner.getId();
      responseJson.put("businessPartnerId", businessPartnerId);
      responseJson.put("searchKey", searchKey);
      return responseJson;
    } catch (IllegalArgumentException e) {
      log.error(e.getMessage(), e);
      OBDal.getInstance().rollbackAndClose();
      responseJson.put("error", e.getMessage());
      return responseJson;
    } catch (OBException e) {
      log.error(e.getMessage(), e);
      OBDal.getInstance().rollbackAndClose();
      throw new OBException("Error while inserting Business Partner Details", e);
    }
  }

  private BusinessPartner createBusinessPartner(JSONObject jsonObject, String searchKey)
      throws Exception {
    BusinessPartner businessPartner = OBProvider.getInstance().get(BusinessPartner.class);
    try {
      String orgName = jsonObject.getString("organization");
      String clientName = jsonObject.getString("client");
      String bpCatName = jsonObject.getString("bpCategory");
      PaymentTerm storePaymentTerm = null;
      String paymentMethod = jsonObject.has("paymentmethod") ? jsonObject.getString("paymentmethod")
          : "";
      String paymentTerm = jsonObject.has("paymentterm") ? jsonObject.getString("paymentterm") : "";
      String pricelist = jsonObject.has("pricelist") ? jsonObject.getString("pricelist") : "";
      String currency = jsonObject.has("currency") ? jsonObject.getString("currency") : "";
      String creditLimit = jsonObject.has("creditLineLimit")
          ? jsonObject.getString("creditLineLimit")
          : "0";
      BigDecimal creditLineLimit = new BigDecimal(creditLimit);

      OBCriteria<Organization> obCriteriaorg = OBDal.getInstance()
          .createCriteria(Organization.class);
      obCriteriaorg.add(Restrictions.eq(Organization.PROPERTY_NAME, orgName));
      if (!obCriteriaorg.list().isEmpty()) {
        businessPartner.setOrganization(obCriteriaorg.list().get(0));
        storePaymentTerm = obCriteriaorg.list().get(0).getObretcoDbpPtermid();
      } else {
        throw new IllegalArgumentException("Invalid Organization: " + orgName);
      }
      OBCriteria<Client> obCriteriaclient = OBDal.getInstance().createCriteria(Client.class);
      obCriteriaclient.add(Restrictions.eq(Client.PROPERTY_NAME, clientName));
      if (!obCriteriaclient.list().isEmpty()) {
        businessPartner.setClient(obCriteriaclient.list().get(0));
      } else {
        throw new IllegalArgumentException("Invalid Client: " + clientName);
      }
      OBCriteria<Category> obCriteriaBpCat = OBDal.getInstance().createCriteria(Category.class);
      obCriteriaBpCat.add(Restrictions.eq(Category.PROPERTY_NAME, bpCatName));
      if (!obCriteriaBpCat.list().isEmpty()) {
        businessPartner.setBusinessPartnerCategory(obCriteriaBpCat.list().get(0));
      } else {
        throw new IllegalArgumentException("Invalid BusinessPartner Category: " + bpCatName);
      }

      OBCriteria<FIN_PaymentMethod> obCriteriapaymentmethod = OBDal.getInstance()
          .createCriteria(FIN_PaymentMethod.class);
      obCriteriapaymentmethod.add(Restrictions.eq(FIN_PaymentMethod.PROPERTY_NAME, paymentMethod));
      if (!obCriteriapaymentmethod.list().isEmpty()) {
        businessPartner.setPaymentMethod(obCriteriapaymentmethod.list().get(0));
      } else {
        throw new IllegalArgumentException("Invalid Payment Method: " + paymentMethod);
      }

      if (storePaymentTerm != null) {
        businessPartner.setPaymentTerms(storePaymentTerm);
      } else {
        OBCriteria<PaymentTerm> obCriteriapaymentTerm = OBDal.getInstance()
            .createCriteria(PaymentTerm.class);
        obCriteriapaymentTerm.add(Restrictions.eq(PaymentTerm.PROPERTY_NAME, paymentTerm));
        if (!obCriteriapaymentTerm.list().isEmpty()) {
          businessPartner.setPaymentTerms(obCriteriapaymentTerm.list().get(0));
        } else {
          throw new IllegalArgumentException("Invalid Payment Term: " + paymentTerm);
        }
      }

      OBCriteria<PriceList> obCriteriapricelist = OBDal.getInstance()
          .createCriteria(PriceList.class);
      obCriteriapricelist.add(Restrictions.eq(PriceList.PROPERTY_NAME, pricelist));
      if (!obCriteriapricelist.list().isEmpty()) {
        businessPartner.setPriceList(obCriteriapricelist.list().get(0));
      } else {
        throw new IllegalArgumentException("Invalid Price List: " + pricelist);
      }

      OBCriteria<Currency> obCriteriaCurrency = OBDal.getInstance().createCriteria(Currency.class);
      obCriteriaCurrency.add(Restrictions.eq(Currency.PROPERTY_ISOCODE, currency));
      if (!obCriteriaCurrency.list().isEmpty()) {
        businessPartner.setCurrency(obCriteriaCurrency.list().get(0));
      } else {
        throw new IllegalArgumentException("Invalid currency: " + currency);
      }
      // Create and populate the BusinessPartner object
      businessPartner.setNewOBObject(true);
      businessPartner.setUpdated(new Date());
      businessPartner.setUpdatedBy(OBContext.getOBContext().getUser());
      businessPartner.setCreatedBy(OBContext.getOBContext().getUser());
      businessPartner.setCreationDate(new Date());
      businessPartner.setActive(true);
      businessPartner.setSearchKey(searchKey);
      businessPartner.setGcnvUniquecreditnote(false);
      businessPartner.setName(jsonObject.getString("commercialName"));
      if (jsonObject.has("fiscalName")) {
        businessPartner.setName2(jsonObject.getString("fiscalName"));
      }
      if (jsonObject.has("taxID")) {
        businessPartner.setTaxID(jsonObject.getString("taxID"));
      }
      businessPartner.setCreditLimit(creditLineLimit);
      if (jsonObject.has("invoiceterm")) {
        if (jsonObject.getString("invoiceterm").equals("I")
            || jsonObject.getString("invoiceterm").equals("D")
            || jsonObject.getString("invoiceterm").equals("N")
            || jsonObject.getString("invoiceterm").equals("O")
            || jsonObject.getString("invoiceterm").equals("S")) {
          businessPartner.setInvoiceTerms(jsonObject.getString("invoiceterm"));
        } else {
          throw new IllegalArgumentException(
              "Invalid invoice term: " + jsonObject.getString("invoiceterm"));
        }
      }

      // Save the BusinessPartner object
      OBDal.getInstance().save(businessPartner);
    } catch (IllegalArgumentException e) {
      log.error(e.getMessage(), e);
      OBDal.getInstance().rollbackAndClose();
      throw new IllegalArgumentException(e.getMessage(), e);
    } catch (OBException e) {
      log.error(e.getMessage(), e);
      OBDal.getInstance().rollbackAndClose();
      throw new OBException("Error while inserting Business Partner Details", e);
    }
    return businessPartner;
  }

  private void saveLocations(JSONArray locationsArray, BusinessPartner businessPartner)
      throws Exception {
    try {
      for (int j = 0; j < locationsArray.length(); j++) {
        JSONObject locationObject = locationsArray.getJSONObject(j);
        String countryname = locationObject.getString("country");
        String city = locationObject.getString("city");
        String region = locationObject.has("region") ? locationObject.getString("region") : "";

        Location location = OBProvider.getInstance().get(Location.class);
        org.openbravo.model.common.geography.Location loc = OBProvider.getInstance()
            .get(org.openbravo.model.common.geography.Location.class);

        OBCriteria<Country> obCriteriacountry = OBDal.getInstance().createCriteria(Country.class);
        obCriteriacountry.add(Restrictions.eq(Country.PROPERTY_NAME, countryname));
        if (!obCriteriacountry.list().isEmpty()) {
          loc.setCountry(obCriteriacountry.list().get(0));
        } else {
          throw new IllegalArgumentException("Invalid countryname: " + countryname);
        }

        OBCriteria<City> obCriteriaCity = OBDal.getInstance().createCriteria(City.class);
        obCriteriaCity.add(Restrictions.eq(City.PROPERTY_NAME, city));
        if (!obCriteriaCity.list().isEmpty()) {
          loc.setCity(obCriteriaCity.list().get(0));
        } else {
          throw new IllegalArgumentException("Invalid City: " + city);
        }

        OBCriteria<Region> obCriteriaRegion = OBDal.getInstance().createCriteria(Region.class);
        obCriteriaRegion.add(Restrictions.eq(Region.PROPERTY_NAME, region));
        if (!obCriteriaRegion.list().isEmpty()) {
          loc.setRegion(obCriteriaRegion.list().get(0));
        } else {
          throw new IllegalArgumentException("Invalid Region: " + region);
        }
        loc.setAddressLine1(locationObject.getString("address"));
        location.setName(locationObject.getString("address"));
        loc.setCityName(locationObject.getString("city"));
        if (locationObject.has("postalcode")) {
          loc.setPostalCode(locationObject.getString("postalcode"));
        }
        OBDal.getInstance().save(loc);

        location.setNewOBObject(true);
        location.setLocationAddress(loc);
        location.setUpdated(new Date());
        location.setUpdatedBy(OBContext.getOBContext().getUser());
        location.setCreatedBy(OBContext.getOBContext().getUser());
        location.setCreationDate(new Date());
        location.setActive(true);
        location.setOrganization(businessPartner.getOrganization());
        location.setClient(businessPartner.getClient());
        location.setBusinessPartner(businessPartner);

        // Save the Location object
        OBDal.getInstance().save(location);
      }
    } catch (IllegalArgumentException e) {
      log.error(e.getMessage(), e);
      OBDal.getInstance().rollbackAndClose();
      throw new IllegalArgumentException(e.getMessage(), e);
    } catch (OBException e) {
      log.error(e.getMessage(), e);
      OBDal.getInstance().rollbackAndClose();
      throw new OBException("Error while inserting location Details", e);
    }
  }

  private void saveContactInformation(JSONArray userArray, BusinessPartner businessPartner)
      throws Exception {
    String phone = null;
    String countryCode = null;
    Integer countryCodeLength = 0;
    Long phoneLength = null;
    try {
      for (int j = 0; j < userArray.length(); j++) {
        JSONObject userObject = userArray.getJSONObject(j);
        User user = new User();
        user.setNewOBObject(true);
        user.setUpdated(new Date());
        user.setUpdatedBy(OBContext.getOBContext().getUser());
        user.setCreatedBy(OBContext.getOBContext().getUser());
        user.setCreationDate(new Date());
        user.setOrganization(businessPartner.getOrganization());
        user.setClient(businessPartner.getClient());
        user.setBusinessPartner(businessPartner);
        user.setActive(true);
        if (userObject.has("email") && !userObject.getString("email").isEmpty()) {
          user.setEmail(userObject.getString("email"));
        }
        if (userObject.has("phone")) {
          phone = userObject.getString("phone");
        }
        if (userObject.has("countryIdd") && !userObject.getString("countryIdd").isEmpty()) {
          countryCode = userObject.getString("countryIdd");
          countryCodeLength = countryCode.length();
          OBCriteria<Country> obCriteriacountry = OBDal.getInstance().createCriteria(Country.class);
          obCriteriacountry
              .add(Restrictions.eq(Country.PROPERTY_CUSTSHACOUNTRYIDDCODE, countryCode));
          if (!obCriteriacountry.list().isEmpty()) {
            Country country = obCriteriacountry.list().get(0);
            phoneLength = country.getCustshaMobilenoLength();
          } else {
            throw new IllegalArgumentException(
                "Provided countryIdd code is not present in OB Database");
          }
        } else {
          throw new IllegalArgumentException("Please provide countryIdd code");
        }
        boolean isValid = validateMobileNumber(countryCode, countryCodeLength, phoneLength, phone);
        if (isValid) {
          user.setPhone(userObject.getString("phone"));
        } else {
          throw new IllegalArgumentException(
              "Invalid phoneNumber: " + userObject.getString("phone"));
        }
        if (userObject.has("firstName")) {
          user.setFirstName(userObject.getString("firstName"));
        }
        if (userObject.has("lastName")) {
          user.setLastName(userObject.getString("lastName"));
        }
        if (userObject.has("name")) {
          user.setName(userObject.getString("name"));
        }
        // Save the User object
        OBDal.getInstance().save(user);
      }
    } catch (IllegalArgumentException e) {
      log.error(e.getMessage(), e);
      OBDal.getInstance().rollbackAndClose();
      throw new IllegalArgumentException(e.getMessage(), e);
    } catch (OBException e) {
      log.error(e.getMessage(), e);
      OBDal.getInstance().rollbackAndClose();
      throw new OBException("Error while inserting user Details", e);
    }
  }

  public static boolean validateMobileNumber(String countryCode, int countryIddLength,
      Long phoneNolength, String PhoneNO) {
    // Check if the phone number is null or empty
    if (PhoneNO == null || PhoneNO.isEmpty()) {
      return false;
    }

    // Remove any leading or trailing whitespace
    PhoneNO = PhoneNO.trim();

    // Check if the phone number length matches the expected length
    int expectedLength = (int) (countryIddLength + phoneNolength);
    if (PhoneNO.length() != expectedLength) {
      return false;
    }

    // Check if the phone number starts with the country code
    if (!PhoneNO.startsWith(countryCode)) {
      return false;
    }
    // Check if the remaining part of the phone number consists only of digits
    String phoneNoWithoutCountryCode = PhoneNO.substring(countryCode.length());
    if (!phoneNoWithoutCountryCode.matches("\\d+")) {
      return false;
    }
    // If all checks pass, the phone number is valid
    return true;
  }

  public JSONObject updateRecords(JSONObject jsonObject) throws Exception {
    try {
      validateUpdateRecordInput(jsonObject);
      String searchKey = jsonObject.getString("searchKey");
      String client = jsonObject.getString("client");
      String organization = jsonObject.getString("organization");

      BusinessPartner businessPartner = checkRecordExists(client, organization, searchKey);
      updateBusinessPartnerFields(businessPartner, jsonObject);
      saveLocations(jsonObject.getJSONArray("locations"), businessPartner);
      updateUsers(businessPartner, jsonObject.getJSONArray("contactInformation"));

      String businessPartnerId = businessPartner.getId();
      responseJson.put("businessPartnerId", businessPartnerId);
      responseJson.put("searchKey", searchKey);
      return responseJson;
    } catch (IllegalArgumentException e) {
      log.error(e.getMessage(), e);
      OBDal.getInstance().rollbackAndClose();
      responseJson.put("error", e.getMessage());
      return responseJson;
    } catch (OBException e) {
      log.error(e.getMessage(), e);
      OBDal.getInstance().rollbackAndClose();
      throw new OBException("Error while updating Business partner", e);
    }
  }

  private void validateUpdateRecordInput(JSONObject jsonObject) throws Exception {
    if (!jsonObject.has("client")) {
      throw new IllegalArgumentException("Client is mandatory to update record");
    }
    if (!jsonObject.has("organization")) {
      throw new IllegalArgumentException("Organization is mandatory to update record");
    }
    if (!jsonObject.has("searchKey")) {
      if (!jsonObject.has("bpCategory")) {
        throw new IllegalArgumentException("bpCategory is Mandatory field");
      }
      if (!jsonObject.has("commercialName")) {
        throw new IllegalArgumentException("commercialName is mandatory");
      }
    }
    JSONArray locationsArray = jsonObject.getJSONArray("locations");
    for (int j = 0; j < locationsArray.length(); j++) {
      JSONObject locationObject = locationsArray.getJSONObject(j);
      if (!locationObject.has("city") && locationObject.getString("city").isEmpty()) {
        throw new IllegalArgumentException("city is mandatory filed");
      }
      if (!locationObject.has("country") && locationObject.getString("country").isEmpty()) {
        throw new IllegalArgumentException("country is mandatory filed");
      }
      if (!locationObject.has("address")) {
        throw new IllegalArgumentException("address is mandatory field");
      }
    }
  }

  private void updateBusinessPartnerFields(BusinessPartner businessPartner, JSONObject jsonObject)
      throws Exception {
    try {
      if (businessPartner == null) {
        throw new IllegalArgumentException("Business partner not found");
      } else {
        if (jsonObject.has("bpCategory")) {
          OBCriteria<Category> obCriteriaBpCat = OBDal.getInstance().createCriteria(Category.class);
          obCriteriaBpCat
              .add(Restrictions.eq(Category.PROPERTY_NAME, jsonObject.getString("bpCategory")));
          businessPartner.setBusinessPartnerCategory(
              obCriteriaBpCat.list().isEmpty() ? null : obCriteriaBpCat.list().get(0));
        }

        if (jsonObject.has("paymentmethod")) {
          OBCriteria<FIN_PaymentMethod> obCriteriapaymentmethod = OBDal.getInstance()
              .createCriteria(FIN_PaymentMethod.class);
          obCriteriapaymentmethod.add(Restrictions.eq(FIN_PaymentMethod.PROPERTY_NAME,
              jsonObject.getString("paymentmethod")));
          businessPartner.setPaymentMethod(obCriteriapaymentmethod.list().isEmpty() ? null
              : obCriteriapaymentmethod.list().get(0));
        }

        if (jsonObject.has("paymentterm")) {
          OBCriteria<PaymentTerm> obCriteriapaymentTerm = OBDal.getInstance()
              .createCriteria(PaymentTerm.class);
          obCriteriapaymentTerm
              .add(Restrictions.eq(PaymentTerm.PROPERTY_NAME, jsonObject.getString("paymentterm")));
          obCriteriapaymentTerm.add(Restrictions.eq(PaymentTerm.PROPERTY_DEFAULT, true));
          businessPartner.setPaymentTerms(
              obCriteriapaymentTerm.list().isEmpty() ? null : obCriteriapaymentTerm.list().get(0));
        }

        if (jsonObject.has("pricelist")) {
          OBCriteria<PriceList> obCriteriapricelist = OBDal.getInstance()
              .createCriteria(PriceList.class);
          obCriteriapricelist
              .add(Restrictions.eq(PriceList.PROPERTY_NAME, jsonObject.getString("pricelist")));
          businessPartner.setPriceList(
              obCriteriapricelist.list().isEmpty() ? null : obCriteriapricelist.list().get(0));
        }

        if (jsonObject.has("currency")) {
          OBCriteria<Currency> obCriteriaCurrency = OBDal.getInstance()
              .createCriteria(Currency.class);
          obCriteriaCurrency
              .add(Restrictions.eq(Currency.PROPERTY_ISOCODE, jsonObject.getString("currency")));
          businessPartner.setCurrency(
              obCriteriaCurrency.list().isEmpty() ? null : obCriteriaCurrency.list().get(0));
        }

        businessPartner.setUpdated(new Date());
        businessPartner.setUpdatedBy(OBContext.getOBContext().getUser());
        // Update only the fields provided in the JSON object
        businessPartner.setName(jsonObject.optString("commercialName", businessPartner.getName()));
        businessPartner.setName2(jsonObject.optString("fiscalName", businessPartner.getName2()));
        businessPartner.setTaxID(jsonObject.optString("taxID", businessPartner.getTaxID()));
        businessPartner.setCreditLimit(new BigDecimal(
            jsonObject.optString("creditLineLimit", businessPartner.getCreditLimit().toString())));
        if (jsonObject.has("invoiceterm")) {
          if (jsonObject.getString("invoiceterm").equals("I")
              || jsonObject.getString("invoiceterm").equals("D")
              || jsonObject.getString("invoiceterm").equals("N")
              || jsonObject.getString("invoiceterm").equals("O")
              || jsonObject.getString("invoiceterm").equals("S")) {
            businessPartner.setInvoiceTerms(jsonObject.getString("invoiceterm"));
          }
        }

        // Update BusinessPartner object
        OBDal.getInstance().save(businessPartner);
      }
    } catch (OBException e) {
      log.error(e.getMessage(), e);
      throw new OBException("error while updating business partner details", e);
    }
  }

  private void updateUsers(BusinessPartner businessPartner, JSONArray userArray) throws Exception {
    // Iterate over users and update them
    try {
      for (int j = 0; j < userArray.length(); j++) {
        JSONObject userObject = userArray.getJSONObject(j);

        // Query existing User based on email and BusinessPartner
        User user = null;
        OBCriteria<User> obCriteriaUser = OBDal.getInstance().createCriteria(User.class);
        obCriteriaUser.add(Restrictions.eq(User.PROPERTY_BUSINESSPARTNER, businessPartner));
        List<User> existingUserList = obCriteriaUser.list();
        if (!existingUserList.isEmpty()) {
          user = existingUserList.get(0);
        }
        // Update User fields
        user.setUpdated(new Date());
        user.setUpdatedBy(OBContext.getOBContext().getUser());
        user.setEmail(userObject.optString("email", user.getEmail()));
        user.setPhone(userObject.optString("phone", user.getPhone()));
        user.setFirstName(userObject.optString("firstName", user.getFirstName()));
        user.setLastName(userObject.optString("lastName", user.getLastName()));
        user.setName(userObject.optString("name", user.getName()));
        user.setBusinessPartner(businessPartner);

        // Save User object
        OBDal.getInstance().save(user);
      }
    } catch (HibernateException e) {
      log.error(e.getMessage(), e);
      throw new HibernateException("Error while updating user details");
    } catch (OBException e) {
      log.error(e.getMessage(), e);
      throw new OBException("Error while updating user details", e);
    }
  }

  @Override
  public void doDelete(String path, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
  }

  @Override
  public void doPut(String path, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
  }

  @Override
  public void doGet(String path, HttpServletRequest request, HttpServletResponse response)
      throws Exception {
    String phoneNumber = null;
    String email = null;
    phoneNumber = request.getParameter(phoneNo);
    email = request.getParameter("emailId");
    final JSONObject jsonResponse = new JSONObject();
    final JSONObject jsonResult = new JSONObject();

    try {
      if (phoneNumber == null && email == null) {
        // Handle missing both parameters
        jsonResponse.put("error", "Either phone number or email parameter is required");
        jsonResult.put("response", jsonResponse);
        writeResult(response, jsonResult.toString());
        return;
      }
      OBCriteria<User> obCriteriaUser = OBDal.getInstance().createCriteria(User.class);
      obCriteriaUser.createAlias("businessPartner", "bp");
      obCriteriaUser.addOrder(Order.desc("bp.updated"));
      // Apply OR condition for phone number and email
      if (phoneNumber != null && email != null) {
        obCriteriaUser.add(Restrictions.or(Restrictions.eq(User.PROPERTY_PHONE, phoneNumber),
            Restrictions.eq(User.PROPERTY_EMAIL, email)));
      } else if (phoneNumber != null) {
        obCriteriaUser.add(Restrictions.eq(User.PROPERTY_PHONE, phoneNumber));
      } else {
        obCriteriaUser.add(Restrictions.eq(User.PROPERTY_EMAIL, email));
      }
      List<User> userlist = obCriteriaUser.list();

      JSONArray mainObjectArray = new JSONArray();

      if (userlist.isEmpty()) {
        throw new OBException("There is no record for provided phone number and Email");
      }
      for (User user : userlist) {
        JSONObject mainObject = new JSONObject();

        JSONObject contactInfo = new JSONObject();
        JSONArray contactInfoArray = new JSONArray();
        JSONArray locationsArray = new JSONArray(); // Initialize locationsArray here

        OBCriteria<Location> obCriteriaLocation = OBDal.getInstance()
            .createCriteria(Location.class);
        obCriteriaLocation
            .add(Restrictions.eq(Location.PROPERTY_BUSINESSPARTNER, user.getBusinessPartner()));
        List<Location> locationlist = obCriteriaLocation.list();

        for (Location loc : locationlist) {
          JSONObject location = new JSONObject(); // Initialize location object inside the loop
          location.put("address",
              loc.getLocationAddress().getAddressLine1() != null
                  ? loc.getLocationAddress().getAddressLine1()
                  : "");
          location.put("cityname",
              loc.getLocationAddress().getCityName() != null
                  ? loc.getLocationAddress().getCityName()
                  : "");
          location.put("country",
              loc.getLocationAddress().getCountry() != null
                  ? loc.getLocationAddress().getCountry().getName()
                  : "");
          location.put("postalcode",
              loc.getLocationAddress().getPostalCode() != null
                  ? loc.getLocationAddress().getPostalCode()
                  : "");

          locationsArray.put(location); // Add location to locationsArray
        }

        contactInfo.put("firstname", user.getFirstName() != null ? user.getFirstName() : "");
        contactInfo.put("lastname", user.getLastName() != null ? user.getLastName() : "");
        contactInfo.put("email", user.getEmail() != null ? user.getEmail() : "");
        contactInfo.put("phone", user.getPhone() != null ? user.getPhone() : "");
        contactInfoArray.put(contactInfo); // Add contactInfo to contactInfoArray

        mainObject.put("businesspartnerid", user.getBusinessPartner().getId());
        mainObject.put("searchkey", user.getBusinessPartner().getSearchKey());
        mainObject.put("organization",
            user.getBusinessPartner().getOrganization() != null
                ? user.getBusinessPartner().getOrganization().getName()
                : "");
        mainObject.put("client",
            user.getBusinessPartner().getClient() != null
                ? user.getBusinessPartner().getClient().getName()
                : "");
        mainObject.put("commercialName",
            user.getBusinessPartner().getName() != null ? user.getBusinessPartner().getName() : "");
        mainObject.put("fiscalName",
            user.getBusinessPartner().getEntityName() != null
                ? user.getBusinessPartner().getEntityName()
                : "");
        mainObject.put("bpCategory",
            user.getBusinessPartner().getBusinessPartnerCategory() != null
                ? user.getBusinessPartner().getBusinessPartnerCategory().getName()
                : "");
        mainObject.put("invoiceTerms",
            user.getBusinessPartner().getInvoiceTerms() != null
                ? user.getBusinessPartner().getInvoiceTerms()
                : "");
        mainObject.put("paymentMethod",
            user.getBusinessPartner().getPaymentMethod() != null
                ? user.getBusinessPartner().getPaymentMethod().getName()
                : "");
        mainObject.put("paymentTerms",
            user.getBusinessPartner().getPaymentTerms() != null
                ? user.getBusinessPartner().getPaymentTerms().getName()
                : "");
        mainObject.put("priceList",
            user.getBusinessPartner().getPriceList() != null
                ? user.getBusinessPartner().getPriceList().getName()
                : "");
        mainObject.put("taxID",
            user.getBusinessPartner().getTaxID() != null ? user.getBusinessPartner().getTaxID()
                : "");
        mainObject.put("locations", locationsArray); // Add locationsArray to mainObject
        mainObject.put("contactInformation", contactInfoArray); // Add contactInfoArray to
                                                                // mainObject

        mainObjectArray.put(mainObject); // Add mainObject to mainObjectArray
      }

      jsonResponse.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
      jsonResponse.put(JsonConstants.RESPONSE_DATA, mainObjectArray);
      jsonResult.put(JsonConstants.RESPONSE_RESPONSE, jsonResponse);
      writeResult(response, jsonResult.toString());
    } catch (OBException e) {
      log.error("Error while fetching Business Partner Details", e);
      jsonResponse.put("status", "error");
      jsonResponse.put("message", e.getMessage());
      jsonResult.put("response", jsonResponse);
      writeResult(response, jsonResult.toString());
    }
  }

  private void writeResult(HttpServletResponse response, String result) throws IOException {
    response.setContentType("application/json;charset=UTF-8");
    response.setHeader("Content-Type", "application/json;charset=UTF-8");
    final Writer w = response.getWriter();
    w.write(result);
    w.close();
  }

}
