/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.customdevelopments.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.retail.posterminal.ProcessHQLQuery;

public class PromotionalBasis extends ProcessHQLQuery {
	private static Logger log = Logger.getLogger(PromotionalBasis.class);

	@Override
	protected List<String> getQuery(JSONObject jsonsent) throws JSONException {
		List<String> hqlQueries = new ArrayList<String>();
		String hqlQry = "select pb.id as id, pb.fINPaymentmethod.id as PaymentmethodId, pb.client.id as client, pb.promotionalBasis as PromotionalBasis,"
				+ " pb.active as active from custsha_prombasis pb where pb.active = 'Y'" + "and pb.client='"
				+ OBContext.getOBContext().getCurrentClient().getId() + "'";
		hqlQueries.add(hqlQry);
		return hqlQueries;
	}

}