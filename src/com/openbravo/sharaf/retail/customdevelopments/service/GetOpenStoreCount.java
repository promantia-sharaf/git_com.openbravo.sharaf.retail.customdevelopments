package com.openbravo.sharaf.retail.customdevelopments.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Query;
import org.openbravo.client.kernel.BaseActionHandler;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.mobile.core.process.ProcessHQLQuery;
import org.openbravo.model.common.enterprise.Organization;

public class GetOpenStoreCount extends ProcessHQLQuery {

	private static Logger log = Logger.getLogger(GetOpenStoreCount.class);
	 @Inject
	  @Any

	private Instance<ModelExtension> extensions;

	@Override
	protected List<String> getQuery(JSONObject jsonsent) throws JSONException {
		
		String orgID = jsonsent.getString("organization");
		
	        List<String> hqlQueries = new ArrayList<String>();
			log.info("contents in jsonsent" +jsonsent.toString());

			
            log.info("before query write.............");
		    String hqlcheck = "select count(*) from OBPOS_Applications where organization.id = '" + orgID + "' and possSession = true";

		    hqlQueries.add(hqlcheck);
		
		return hqlQueries;
	}

}
