/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.customdevelopments.service;

import java.util.ArrayList;

import java.util.List;
import org.apache.log4j.Logger;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.dal.core.OBContext;
import org.openbravo.model.common.geography.Country;
import org.openbravo.retail.posterminal.ProcessHQLQuery;

public class CityModel extends ProcessHQLQuery {
    private static Logger log = Logger.getLogger(CityModel.class);
  
	@Override
	protected List<String> getQuery(JSONObject jsonsent) throws JSONException {
		List<String> hqlQueries = new ArrayList<String>();
		
		String hqlQry = "select st.id as id , st.name as name, st.client.id as client, st.active as active,st.country.id as country "
                    + " from City as st where st.active = 'Y'" 
                   // + " and st.country ='" + countryid + "'"
                    + " and st.client='" + OBContext.getOBContext().getCurrentClient().getId() + "'"
                    + " order by name asc";
		hqlQueries.add(hqlQry);
		return hqlQueries;
	}

}