package com.openbravo.sharaf.retail.customdevelopments.service;

import org.apache.log4j.Logger;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.servlet.ServletException;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.businessUtility.Preferences;
import org.openbravo.erpCommon.utility.PropertyException;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.service.json.JsonConstants;
import org.openbravo.retail.posterminal.ProcessHQLQuery;
import org.openbravo.retail.posterminal.TerminalType;

public class CaptureOriginalInvoiceDocSearch extends ProcessHQLQuery {
  private static org.apache.log4j.Logger log = Logger
      .getLogger(CaptureOriginalInvoiceDocSearch.class);

  @Override
  protected Map<String, Object> getParameterValues(JSONObject jsonsent) throws JSONException {
      Map<String, Object> paramValues = new HashMap<String, Object>();
      JSONObject json = jsonsent.getJSONObject("filters");
      if (!json.getString("filterText").isEmpty()) {
          paramValues.put("filterT1", json.getString("filterText").trim());
      }
      try {
        OBContext.setAdminMode(true);
        if (jsonsent.has("filters")) {
          if (!json.isNull("documentType")) {
              if (OBContext.getOBContext().getCurrentOrganization().getObposCrossStore() == null) {
                paramValues.put("documentTypes", json.getJSONArray("documentType"));
              } else {
                Set<String> orgTree = OBContext.getOBContext().getOrganizationStructureProvider()
                    .getNaturalTree(OBContext.getOBContext().getCurrentOrganization()
                        .getObposCrossStore().getId());
                String orgList = "";
                Set<String> docTypesList = new HashSet<String>();
                for (String orgId : orgTree) {
                  Organization org = OBDal.getInstance().get(Organization.class, orgId);
                  if (!orgList.equals("")) {
                    orgList += ",";
                  }
                  orgList += "'" + org.getId() + "'";
                  getTerminalTypeDocument(docTypesList, org, false, true);
                }

                paramValues.put("documentTypes", docTypesList);
                jsonsent.put("organizationList", orgList);
              }
          }
          try {
            if (!json.getString("startDate").isEmpty()) {
              paramValues.put("startDate",
                  getDateFormated(json.getString("startDate"), "yyyy-MM-dd"));
            }
            if (!json.getString("endDate").isEmpty()) {
              paramValues.put("endDate", getDateFormated(json.getString("endDate"), "yyyy-MM-dd"));
            }
          } catch (ParseException e) {
            log.error(e.getMessage(), e);
          }
        }
      }catch(Exception e) {
        log.error(e.getMessage(), e); 
      }finally {
        OBContext.restorePreviousMode();
      }
      return paramValues;
  }

  @Override
  protected List<String> getQuery(JSONObject jsonsent) throws JSONException {

    JSONObject json = jsonsent.getJSONObject("filters");
    String hqlPaidReceipts = "SELECT ord.id as id, ord.documentNo as documentNo, ord.orderDate as orderDate, "
        + " ord.businessPartner.name as businessPartner, ord.grandTotalAmount as totalamount, ord.documentType.id as documentTypeId "
        + " FROM Order as ord " + " WHERE ord.obposIsDeleted = false "
        + " AND ord.client.id = :client ";

    hqlPaidReceipts += " and ord.organization.id in (" + jsonsent.getString("organizationList")
        + ") ";
    if (!json.getString("filterText").isEmpty()) {
      hqlPaidReceipts += " AND ord.documentNo like :filterT1 ";
    }
    if (!json.isNull("documentType")) {
      hqlPaidReceipts += " and ( ord.documentType.id in (:documentTypes) ) ";
    }
    if (!json.getString("startDate").isEmpty()) {
      hqlPaidReceipts += " and ord.orderDate >= :startDate ";
    }
    if (!json.getString("endDate").isEmpty()) {
      hqlPaidReceipts += " and ord.orderDate <= :endDate ";
    }
    hqlPaidReceipts += " order by ord.orderDate desc";
    return Arrays.asList(new String[] { hqlPaidReceipts });
  }

  @Override
  protected boolean bypassPreferenceCheck() {
    return true;
  }

  private void getTerminalTypeDocument(Set<String> docTypesList, Organization org,
      boolean isQuotation, boolean includeReturns) {
    OBCriteria<TerminalType> terminalTypeCri = OBDal.getInstance()
        .createCriteria(TerminalType.class);
    terminalTypeCri.add(Restrictions.eq(TerminalType.PROPERTY_ORGANIZATION, org));
    terminalTypeCri.setFilterOnReadableOrganization(false);
    for (TerminalType typeObj : terminalTypeCri.list()) {
      if(!isQuotation && includeReturns) {
        docTypesList.add(typeObj.getDocumentType().getId());
      }
    }
  }
}