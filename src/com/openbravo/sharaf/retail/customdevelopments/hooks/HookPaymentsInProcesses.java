/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.customdevelopments.hooks;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.retail.posterminal.PaidReceiptsPaymentsInHook;
import org.openbravo.retail.posterminal.TerminalTypePaymentMethod;

public class HookPaymentsInProcesses extends PaidReceiptsPaymentsInHook {

  private static final Logger log = Logger.getLogger(CustomerLoaderHookCustomDevelopments.class);

  @Override
  public void exec(JSONObject paymentIn, String paymentId) throws Exception {

    FIN_Payment origPayment = OBDal.getInstance().get(FIN_Payment.class, paymentId);
    Object terminalType;
    Order order = OBDal.getInstance().get(
        Order.class,
        origPayment.getFINPaymentDetailList().get(0).getFINPaymentScheduleDetailList().get(0)
            .getOrderPaymentSchedule().getOrder().getId());
    terminalType = order.getObposApplications().getObposTerminaltype();

    OBCriteria<TerminalTypePaymentMethod> obcPaymentMethod = OBDal.getInstance().createCriteria(
        TerminalTypePaymentMethod.class);
    obcPaymentMethod.add(Restrictions.eq(TerminalTypePaymentMethod.PROPERTY_SEARCHKEY,
        paymentIn.getString("kind")));
    obcPaymentMethod.add(Restrictions.eq(TerminalTypePaymentMethod.PROPERTY_OBPOSTERMINALTYPE,
        terminalType));
    obcPaymentMethod.setFilterOnActive(false);
    TerminalTypePaymentMethod payMethod = (TerminalTypePaymentMethod) obcPaymentMethod
        .uniqueResult();
    if (payMethod != null) {
      paymentIn.put("sHFPTArabicDescription", payMethod.getSHFPTArabicDescription());
    }
  }
}
