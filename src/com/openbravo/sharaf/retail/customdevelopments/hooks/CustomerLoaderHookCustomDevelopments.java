/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.customdevelopments.hooks;

import java.util.List;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.advpaymentmngt.utility.FIN_Utility;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.utility.Sequence;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.retail.posterminal.CustomerLoaderHook;
import org.openbravo.retail.posterminal.OBPOSApplications;

public class CustomerLoaderHookCustomDevelopments implements CustomerLoaderHook {

  private static final Logger log = Logger.getLogger(CustomerLoaderHookCustomDevelopments.class);
  private static final String SAP_STORES_BUSINESSPARTNER_SEQ = "SAP_BusinessPartnerSequence";
  private static final String SAP_GITEX_BUSINESSPARTNER_SEQ = "SAP_GitexBusinessPartnerSequence";

  @Override
  public void exec(JSONObject jsonCustomer, BusinessPartner customer) throws Exception {
    // do not reset the searchKey for existing customers
    if (!customer.isNewOBObject()) {
      return;
    }
    OBPOSApplications posTerminal = OBDal.getInstance().get(OBPOSApplications.class,
        jsonCustomer.getString("posTerminal"));
    String sequenceName = SAP_STORES_BUSINESSPARTNER_SEQ;
    String bpNextSequenceNumber = null;
    if (jsonCustomer.has("shasyieBpKey")) {
      bpNextSequenceNumber = jsonCustomer.getString("shasyieBpKey");
    } else {
      if (posTerminal.getOrganization().isCustshaExportietocentral()) {
        sequenceName = SAP_GITEX_BUSINESSPARTNER_SEQ;
      }
      OBCriteria<Sequence> obcSeq = OBDal.getInstance().createCriteria(Sequence.class);
      obcSeq.add(Restrictions.eq(Sequence.PROPERTY_NAME, sequenceName));
      obcSeq.setFilterOnActive(false);
      obcSeq.setFilterOnReadableOrganization(false);
      List<Sequence> seqList = obcSeq.list();
      if (seqList.size() == 1) {
        bpNextSequenceNumber = FIN_Utility.getDocumentNo(true, seqList.get(0));
      } else if (seqList.size() > 1) {
        String errorMessage = "There is more than one sequence with " + sequenceName
            + " name in the System";
        log.error(errorMessage);
        throw new OBException(errorMessage, null);
      } else {
        String errorMessage = "There is no sequence with " + sequenceName + " name in the System";
        log.error(errorMessage);
        throw new OBException(errorMessage, null);
      }
    }
    customer.setSearchKey(bpNextSequenceNumber);
  }
}
