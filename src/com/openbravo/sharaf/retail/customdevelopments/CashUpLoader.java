package com.openbravo.sharaf.retail.customdevelopments;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.DataSynchronizationProcess.DataSynchronization;
import org.openbravo.retail.posterminal.OBPOSApplications;
import org.openbravo.retail.posterminal.POSDataSynchronizationProcess;
import org.openbravo.service.json.JsonConstants;

@DataSynchronization(entity = "custsha_transcount")
public class CashUpLoader extends POSDataSynchronizationProcess {
  private static Logger log = Logger.getLogger(CashUpLoader.class);

  @Override
  public JSONObject saveRecord(JSONObject jsonRecord) throws Exception {
    OBPOSApplications terminal = OBDal.getInstance()
        .get(OBPOSApplications.class, jsonRecord.optString("posterminal"));

    String count = jsonRecord.getString("count");

    CUSTSHATranscount tc = null;
    if (jsonRecord.has("id") && jsonRecord.getString("id") != null) {
      OBCriteria<CUSTSHATranscount> criteria = OBDal.getInstance()
          .createCriteria(CUSTSHATranscount.class);
      criteria.add(Restrictions.eq(CUSTSHATranscount.PROPERTY_OBPOSAPPLICATIONS, terminal));
      tc = (CUSTSHATranscount) criteria.uniqueResult();
      if (tc == null) {
        tc = new CUSTSHATranscount();
      }
    } else {
      tc = new CUSTSHATranscount();
    }
    tc.setObposApplications(terminal);
    tc.setCount(Long.parseLong(count));

    OBDal.getInstance().save(tc);
    JSONObject jsonResponse = new JSONObject();
    jsonResponse.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
    jsonResponse.put("result", "0");
    return jsonResponse;
  }

}
