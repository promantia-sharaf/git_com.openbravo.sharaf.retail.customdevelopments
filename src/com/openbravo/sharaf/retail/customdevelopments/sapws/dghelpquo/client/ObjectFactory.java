
package com.openbravo.sharaf.retail.customdevelopments.sapws.dghelpquo.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.openbravo.sharaf.retail.sapws.stock.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DGHelpSAPServiceInformationRequest_QNAME = new QName("http://sharafdg.com/OB/DGHelpServiceInformation", "DGHelpSAPServiceInformationRequest");
    private final static QName _DGHelpSAPServiceInformationResponse_QNAME = new QName("http://sharafdg.com/OB/DGHelpServiceInformation", "DGHelpSAPServiceInformationResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.openbravo.sharaf.retail.sapws.stock.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DGHelpSAPServiceInformationRequest }
     * 
     */
    public DGHelpSAPServiceInformationRequest createDGHelpSAPServiceInformationRequest() {
        return new DGHelpSAPServiceInformationRequest();
    }

    /**
     * Create an instance of {@link DGHelpSAPServiceInformationResponse }
     * 
     */
    public DGHelpSAPServiceInformationResponse createDGHelpSAPServiceInformationResponse() {
        return new DGHelpSAPServiceInformationResponse();
    }
    
    /**
     * Create an instance of {@link DGHelpSAPServiceInformationResponse.InvoiceDetails }
     * 
     */
    public DGHelpSAPServiceInformationResponse.InvoiceDetails createDGHelpSAPServiceInformationResponseInvoiceDetails() {
        return new DGHelpSAPServiceInformationResponse.InvoiceDetails();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DGHelpSAPServiceInformationResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sharafdg.com/OB/DGHelpServiceInformation", name = "DGHelp_SAP_ServiceInformation_Response")
    public JAXBElement<DGHelpSAPServiceInformationResponse> createDGHelpSAPServiceInformationResponse(DGHelpSAPServiceInformationResponse value) {
        return new JAXBElement<DGHelpSAPServiceInformationResponse>(_DGHelpSAPServiceInformationResponse_QNAME, DGHelpSAPServiceInformationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DGHelpSAPServiceInformationRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://sharafdg.com/OB/DGHelpServiceInformation", name = "DGHelp_SAP_ServiceInformation_Request")
    public JAXBElement<DGHelpSAPServiceInformationRequest> createDGHelpSAPServiceInformationRequest(DGHelpSAPServiceInformationRequest value) {
        return new JAXBElement<DGHelpSAPServiceInformationRequest>(_DGHelpSAPServiceInformationRequest_QNAME, DGHelpSAPServiceInformationRequest.class, null, value);
    }

}
