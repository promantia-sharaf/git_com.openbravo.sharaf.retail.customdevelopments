
package com.openbravo.sharaf.retail.customdevelopments.sapws.dghelpquo.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DGHelp_SAP_ServiceInformation_Response complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DGHelp_SAP_ServiceInformation_Response">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InvoiceDetails" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *                 &lt;sequence>
 *                   &lt;element name="Sold_to_party" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="SD_Document_Currency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="PO_Box" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Street" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Country_Key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Language_Key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Region" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="EMail_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Dial_In" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="LanguageCode_SAP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Language" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Invoice_Amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Vat_Amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Total_Amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DGHelp_SAP_ServiceInformation_Response", propOrder = {
    "invoiceDetails"
})
public class DGHelpSAPServiceInformationResponse {

    @XmlElement(name = "InvoiceDetails")
    protected List<DGHelpSAPServiceInformationResponse.InvoiceDetails> invoiceDetails;

    /**
     * Gets the value of the atpResponse property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the atpResponse property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInvoiceDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DGHelp_SAP_ServiceInformation_Response.InvoiceDetails }
     * 
     * 
     */
    public List<DGHelpSAPServiceInformationResponse.InvoiceDetails> getInvoiceDetails() {
        if (invoiceDetails == null) {
        	invoiceDetails = new ArrayList<DGHelpSAPServiceInformationResponse.InvoiceDetails>();
        }
        return this.invoiceDetails;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *       &lt;sequence>
     *          &lt;element name="Sold_to_party" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *          &lt;element name="SD_Document_Currency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *          &lt;element name="Name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *          &lt;element name="City" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *          &lt;element name="PostalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *          &lt;element name="PO_Box" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *          &lt;element name="Street" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *          &lt;element name="Country_Key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *          &lt;element name="Language_Key" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *          &lt;element name="Region" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *          &lt;element name="EMail_ID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *          &lt;element name="Dial_In" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *          &lt;element name="LanguageCode_SAP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *          &lt;element name="Language" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *          &lt;element name="Invoice_Amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *          &lt;element name="Vat_Amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *          &lt;element name="Total_Amount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
            "soldtoparty",
            "sDDocumentCurrency",
            "name",
            "city",
            "postalCode",
            "pOBox",
            "street",
            "countryKey",
            "languageKey",
            "region",
            "eMailID",
            "dialIn",
            "languageCodeSAP",
            "language",
            "invoiceAmount",
            "vatAmount",
            "totalAmount"
    })
    public static class InvoiceDetails {

        @XmlElement(name = "Sold_to_party")
        protected String soldtoparty;
        @XmlElement(name = "SD_Document_Currency")
        protected String sDDocumentCurrency;
        @XmlElement(name = "Name")
        protected String name;
        @XmlElement(name = "City")
        protected String city;
        @XmlElement(name = "PostalCode")
        protected String postalCode;
        @XmlElement(name = "PO_Box")
        protected String pOBox;
        @XmlElement(name = "Street")
        protected String street;
        @XmlElement(name = "Country_Key")
        protected String countryKey;
        @XmlElement(name = "Language_Key")
        protected String languageKey;
        @XmlElement(name = "Region")
        protected String region;
        @XmlElement(name = "EMail_ID")
        protected String eMailID;
        @XmlElement(name = "Dial_In")
        protected String dialIn;
        @XmlElement(name = "LanguageCode_SAP")
        protected String languageCodeSAP;
        @XmlElement(name = "Language")
        protected String language;
        @XmlElement(name = "Invoice_Amount")
        protected String invoiceAmount;
        @XmlElement(name = "Vat_Amount")
        protected String vatAmount;
        @XmlElement(name = "Total_Amount")
        protected String totalAmount;

        /**
         * Gets the value of the Sold_to_party property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSoldtoparty() {
            return soldtoparty;
        }

        /**
         * Sets the value of the Sold_to_party property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSoldtoparty(String value) {
            this.soldtoparty = value;
        }

        /**
         * Gets the value of the siteCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSDDocumentCurrency() {
            return sDDocumentCurrency;
        }

        /**
         * Sets the value of the SDDocumentCurrency property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSDDocumentCurrency(String value) {
            this.sDDocumentCurrency = value;
        }

        /**
         * Gets the value of the stock property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the value of the name property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Gets the value of the atpStock property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCity() {
            return city;
        }

        /**
         * Sets the value of the atpStock property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCity(String value) {
            this.city = value;
        }

        /**
         * Gets the value of the PostalCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPostalCode() {
            return postalCode;
        }

        /**
         * Sets the value of the PostalCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPostalCode(String value) {
            this.postalCode = value;
        }

        /**
         * Gets the value of the pOBox property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPOBox() {
            return pOBox;
        }

        /**
         * Sets the value of the POBox property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPOBox(String value) {
            this.pOBox = value;
        }

        /**
         * Gets the value of the pOBox property.
         * street
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStreet() {
            return street;
        }

        /**
         * Sets the value of the street property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStreet(String value) {
            this.street = value;
        }

        /**
         * Gets the value of the countryKey property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCountryKey() {
            return countryKey;
        }

        /**
         * Sets the value of the countryKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCountryKey(String value) {
            this.countryKey = value;
        }

        /**
         * Gets the value of the languageKey property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLanguageKey() {
            return languageKey;
        }

        /**
         * Sets the value of the languageKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLanguageKey(String value) {
            this.languageKey = value;
        }

        /**
         * Gets the value of the region property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegion() {
            return region;
        }

        /**
         * Sets the value of the region property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegion(String value) {
            this.region = value;
        }

        /**
         * Gets the value of the eMailID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEMailID() {
            return eMailID;
        }

        /**
         * Sets the value of the eMailID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEMailID(String value) {
            this.eMailID = value;
        }

        /**
         * Gets the value of the dialIn property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDialIn() {
            return dialIn;
        }

        /**
         * Sets the value of the dialIn property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDialIn(String value) {
            this.dialIn = value;
        }

        /**
         * Gets the value of the languageCodeSAP property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLanguageCodeSAP() {
            return languageCodeSAP;
        }

        /**
         * Sets the value of the languageCodeSAP property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLanguageCodeSAP(String value) {
            this.languageCodeSAP = value;
        }

        /**
         * Gets the value of the language property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLanguage() {
            return language;
        }

        /**
         * Sets the value of the language property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLanguage(String value) {
            this.language = value;
        }

        /**
         * Gets the value of the invoiceAmount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInvoiceAmount() {
            return invoiceAmount;
        }

        /**
         * Sets the value of the invoiceAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInvoiceAmount(String value) {
            this.invoiceAmount = value;
        }

        /**
         * Gets the value of the vatAmount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVatAmount() {
            return vatAmount;
        }

        /**
         * Sets the value of the vatAmount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVatAmount(String value) {
            this.vatAmount = value;
        }

        /**
         * Gets the value of the Total_Amount property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTotalAmount() {
            return totalAmount;
        }

        /**
         * Sets the value of the Total_Amount property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTotalAmount(String value) {
            this.totalAmount = value;
        }
    }

}
