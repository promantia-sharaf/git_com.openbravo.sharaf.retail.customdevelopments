package com.openbravo.sharaf.retail.customdevelopments.sapws.dghelpquo.processes;

import java.util.List;

import javax.inject.Inject;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.base.exception.OBException;
import org.openbravo.dal.core.OBContext;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.model.common.plm.Product;
import org.openbravo.retail.posterminal.JSONProcessSimple;
import org.openbravo.service.json.JsonConstants;
import org.apache.log4j.Logger;

import com.openbravo.sharaf.retail.customdevelopments.sapws.dghelpquo.DGHelpSAPService;
import com.openbravo.sharaf.retail.customdevelopments.sapws.dghelpquo.client.DGHelpSAPServiceInformationResponse;

public class DGHelpQuotation extends JSONProcessSimple {
	@Inject
	private DGHelpSAPService dgHelpSAPService;
        private static final Logger log = Logger.getLogger(DGHelpQuotation.class);

	public JSONObject exec(JSONObject jsonInfo) throws OBException, JSONException {
		JSONObject result = new JSONObject();
		try {
			OBContext.setAdminMode(true);
			JSONObject response = jsonInfo.getJSONObject("filters");
			String invoiceNumber = response.getString("filterText");

			// Request Stock
                        log.info("Requesting for Stock availability in SAP");
			List<DGHelpSAPServiceInformationResponse.InvoiceDetails> invoiceDetails = dgHelpSAPService
					.getInvoiceDetails(invoiceNumber);
			if (invoiceDetails != null) {
                                log.info("Fetching business partner from invoice");
				BusinessPartner bp = dgHelpSAPService.getBP(invoiceDetails);
                                log.info("Fetching product from invoice");
				Product product = dgHelpSAPService.getProduct();

				JSONObject respObject = new JSONObject();
				respObject.put("bp", bp.getId());
				respObject.put("product", product.getId());
				respObject.put("businessPartnerName", bp.getName());
				respObject.put("custshaWebsiterefno", invoiceNumber);
				respObject.put("totalNetAmount", invoiceDetails.get(0).getInvoiceAmount());
				respObject.put("totalamount", invoiceDetails.get(0).getTotalAmount());
				
				JSONArray respArray = new JSONArray();
				respArray.put(respObject);
				response.put(JsonConstants.RESPONSE_DATA, respArray);
				response.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_SUCCESS);
                                log.info("Response status successful");
			} else {
				response.put(JsonConstants.RESPONSE_STATUS, JsonConstants.RPCREQUEST_STATUS_FAILURE);
                                log.info("Failed response status");
			}
			return response;
		} catch (Exception e) {
			log.error("Error in DGHelp Quotation" +e.getMessage() +e);
			result.put("message", e.getMessage());
			result.put("status", 1);
			return result;
		} finally {
			OBContext.restorePreviousMode();
		}
	}
}
