
package com.openbravo.sharaf.retail.customdevelopments.sapws.dghelpquo.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DGHelp_SAP_ServiceInformation_Request complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DGHelp_SAP_ServiceInformation_Request">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InvoiceNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DGHelp_SAP_ServiceInformation_Request", propOrder = {
    "invoiceNumber"
})
public class DGHelpSAPServiceInformationRequest {

    @XmlElement(name = "InvoiceNumber")
    protected String invoiceNumber;

    /**
     * Gets the invoiceNumber value for this DGHelp_SAP_ServiceInformation_Request.
     * 
     * @return invoiceNumber
     */
    public java.lang.String getInvoiceNumber() {
        return invoiceNumber;
    }

    /**
     * Sets the invoiceNumber value for this DGHelp_SAP_ServiceInformation_Request.
     * 
     * @param invoiceNumber
     */
    public void setInvoiceNumber(java.lang.String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }
}
