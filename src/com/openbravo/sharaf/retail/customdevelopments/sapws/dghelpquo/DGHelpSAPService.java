/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.customdevelopments.sapws.dghelpquo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.servlet.ServletException;
import javax.xml.ws.BindingProvider;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.businesspartner.Category;
import org.openbravo.model.common.businesspartner.Location;
import org.openbravo.model.common.currency.Currency;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.geography.Country;
import org.openbravo.model.common.plm.Product;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
import org.openbravo.model.financialmgmt.payment.PaymentTerm;
import org.openbravo.model.pricing.pricelist.PriceList;
import org.openbravo.utils.FormatUtilities;
import org.apache.log4j.Logger;

import com.openbravo.sharaf.retail.customdevelopments.CUSTSHA_DGHelpQ_config;
import com.openbravo.sharaf.retail.customdevelopments.sapws.dghelpquo.client.DGHelpSAPServiceInformationOut;
import com.openbravo.sharaf.retail.customdevelopments.sapws.dghelpquo.client.DGHelpSAPServiceInformationOutService;
import com.openbravo.sharaf.retail.customdevelopments.sapws.dghelpquo.client.DGHelpSAPServiceInformationRequest;
import com.openbravo.sharaf.retail.customdevelopments.sapws.dghelpquo.client.DGHelpSAPServiceInformationResponse;
import com.openbravo.sharaf.retail.customdevelopments.sapws.dghelpquo.client.DGHelpSAPServiceInformationResponse.InvoiceDetails;

/**
 * Utility class to get available stock of a product in a warehouse from SAP web
 * service.
 */

@ApplicationScoped
public class DGHelpSAPService {
	private String user;
	private String passwd;
	private String endpoint;
	private String productSearchKey;

	DGHelpSAPServiceInformationOutService dgHelpService;
	DGHelpSAPServiceInformationOut dgHelpPort;
        private static final Logger log = Logger.getLogger(DGHelpSAPService.class);

	@PostConstruct
	private void initialize() throws ServletException {
		user = null;
		passwd = null;
		endpoint = null;

		OBContext.setAdminMode(false);
		try {
			// Read service configuration
			OBCriteria<CUSTSHA_DGHelpQ_config> criteria = OBDal.getInstance()
					.createCriteria(CUSTSHA_DGHelpQ_config.class);
			criteria.addOrderBy(CUSTSHA_DGHelpQ_config.PROPERTY_LINE, true);
			criteria.setMaxResults(1);
			CUSTSHA_DGHelpQ_config config = (CUSTSHA_DGHelpQ_config) criteria.uniqueResult();
			if (config != null) {
				log.info("Fetching the SAP DGHelp WebService Config details");
				user = config.getUsername();
				endpoint = config.getEndpoint();
				productSearchKey = config.getProduct();
				String password = config.getPassword();
				if (!StringUtils.isEmpty(password)) {
					passwd = FormatUtilities.encryptDecrypt(password, false);
				}
			} else {
				log.error("SAP DGHelp WebService Config not found");
			}

			dgHelpService = new DGHelpSAPServiceInformationOutService();
			dgHelpPort = dgHelpService.getHTTPPort();
			BindingProvider bp = (BindingProvider) dgHelpPort;

			// Set new endpoint
			if (!StringUtils.isEmpty(endpoint)) {
				bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint);
			}

			// Set Basic HTTP Authentication
			if (!StringUtils.isEmpty(user) && !StringUtils.isEmpty(passwd)) {
				bp.getRequestContext().put(BindingProvider.USERNAME_PROPERTY, user);
				bp.getRequestContext().put(BindingProvider.PASSWORD_PROPERTY, passwd);
			}
		} finally {
			OBContext.restorePreviousMode();
		}
	}

	public Product getProduct() {
		Product product = null;
		if (StringUtils.isNotEmpty(productSearchKey)) {
			OBCriteria<Product> obCriteria = OBDal.getInstance().createCriteria(Product.class);
			obCriteria.add(Restrictions.eq(Product.PROPERTY_SEARCHKEY, productSearchKey));
			obCriteria.add(
					Restrictions.eq(Product.PROPERTY_ORGANIZATION, OBDal.getInstance().get(Organization.class, "0")));
			obCriteria.add(Restrictions.eq(Product.PROPERTY_CLIENT, OBContext.getOBContext().getRole().getClient()));
			product = (Product) obCriteria.uniqueResult();
		}
		return product;
	}

	public List<DGHelpSAPServiceInformationResponse.InvoiceDetails> getInvoiceDetails(final String invoiceNumber) throws ServletException, Exception {

		try {
			// Create the request
			log.info("Creating DGHelp SAP request");
			DGHelpSAPServiceInformationRequest informationRequest = new DGHelpSAPServiceInformationRequest();
			informationRequest.setInvoiceNumber(invoiceNumber);
			log.info("DGHelp SAP Request Invoice number is: " +informationRequest.getInvoiceNumber());
			
			// Consume service method
			log.info("Getting response from SAP for DGHelp Quotation");
			DGHelpSAPServiceInformationResponse informationResponse = dgHelpPort
					.dgHelpSAPServiceInformationOut(informationRequest);
			List<DGHelpSAPServiceInformationResponse.InvoiceDetails> invoiceDetails = null;
			if (informationResponse != null) {
				invoiceDetails = informationResponse.getInvoiceDetails();
				log.info("Successfully obtained Response from SAP for DGHelp Quotation");
			} else {
				log.error("Response not available from SAP for DGHelp Quotation");
			}
			return invoiceDetails;
			} catch(Exception e) {
				log.error("No Records Found");
				throw new Exception("No Records Found");
            }
		}

	public BusinessPartner getBP(final List<InvoiceDetails> invoiceDetails) throws ServletException, Exception {
		log.info("Getting business partner from SAP");
                BusinessPartner bp = null;
		try {
			OBContext.setAdminMode(false);
			if (!invoiceDetails.isEmpty() && invoiceDetails.get(0).getSoldtoparty() != null
					&& invoiceDetails.get(0).getName() != null) {
				OBCriteria<BusinessPartner> bpCriteria = OBDal.getInstance().createCriteria(BusinessPartner.class);
				bpCriteria.add(
						Restrictions.eq(BusinessPartner.PROPERTY_SEARCHKEY, invoiceDetails.get(0).getSoldtoparty()));
				List<BusinessPartner> bpList = bpCriteria.list();
				if (!bpList.isEmpty()) {
                                        log.info("Getting business partner info from OB");
					log.info("BP Search key from invoice: " +invoiceDetails.get(0).getSoldtoparty());
					log.info("BP Search Key from OB: " +bpList.get(0).getSearchKey());
					log.info("BP name from OB: " +bpList.get(0).getName());
					bp = bpList.get(0);
					if(!bp.isActive()){
						bp.setActive(Boolean.TRUE);
						OBDal.getInstance().save(bp);
					}
					if(bp.getName() != invoiceDetails.get(0).getName()){
						bp.setName(invoiceDetails.get(0).getName());
						OBDal.getInstance().save(bp);
						log.info("Updated Invoice business partner name is: " +invoiceDetails.get(0).getName());
					}
				} else {
					try {
                        log.info("Creating new business partner for DG Help");
						bp = new BusinessPartner();
						bp.setActive(Boolean.TRUE);
						bp.setSearchKey(invoiceDetails.get(0).getSoldtoparty());
						bp.setName(invoiceDetails.get(0).getName());
                        log.info("Creating business partner category");
						OBCriteria<Category> obCriteriaBPG = OBDal.getInstance().createCriteria(Category.class);
						obCriteriaBPG.add(Restrictions.eq(Category.PROPERTY_ORGANIZATION,
								OBDal.getInstance().get(Organization.class, "0")));
						obCriteriaBPG.add(Restrictions.eq(Category.PROPERTY_DEFAULT, true));
						obCriteriaBPG.setMaxResults(1);
						List<Category> bpgList = obCriteriaBPG.list();
						if (!bpgList.isEmpty()) {
							bp.setBusinessPartnerCategory(bpgList.get(0));
						}
						bp.setClient(OBContext.getOBContext().getRole().getClient());
						Client client = OBDal.getInstance().get(Client.class, OBContext.getOBContext().getRole().getClient().getId());
						Currency currency = OBDal.getInstance().get(Currency.class, client.getCurrency().getId());
					
                        log.info("Setting country for bp");
						OBCriteria<Country> obCrCountry = OBDal.getInstance().createCriteria(Country.class);
						obCrCountry.add(Restrictions.eq(Country.PROPERTY_CURRENCY, currency));
						obCrCountry.setMaxResults(1);
						Country country = obCrCountry.list().get(0);
					
						OBCriteria<Organization> obCriteriaOrg = OBDal.getInstance().createCriteria(Organization.class);
						obCriteriaOrg.add(Restrictions.eq(Organization.PROPERTY_SEARCHKEY, country.getISOCountryCode()));
						obCriteriaOrg.setMaxResults(1);
						Organization org = obCriteriaOrg.list().get(0);
						log.info("Org list contents: " +org);
                        log.info("Setting org");
						bp.setOrganization(OBDal.getInstance().get(Organization.class, org.getId())); 
                        log.info("bp object contents: " +bp);
						bp.setCustomer(true);
						PaymentTerm paymentTerm = null;
						OBCriteria<PaymentTerm> obCriteria = OBDal.getInstance().createCriteria(PaymentTerm.class);
						obCriteria.add(Restrictions.eq(PaymentTerm.PROPERTY_ORGANIZATION,
								OBDal.getInstance().get(Organization.class, "0")));
						obCriteria.add(Restrictions.eq(PaymentTerm.PROPERTY_DEFAULT, true));
						obCriteria.setMaxResults(1);
						List<PaymentTerm> paymentTermList = obCriteria.list();
						if (!paymentTermList.isEmpty()) {
							paymentTerm = paymentTermList.get(0);
						}
						bp.setPaymentTerms(paymentTerm);

						FIN_PaymentMethod paymentMethod = null;
						OBCriteria<FIN_PaymentMethod> obCriteriaPM = OBDal.getInstance()
								.createCriteria(FIN_PaymentMethod.class);
						obCriteriaPM.add(Restrictions.eq(FIN_PaymentMethod.PROPERTY_ORGANIZATION,
								OBDal.getInstance().get(Organization.class, "0")));
						obCriteriaPM.add(Restrictions.eq(FIN_PaymentMethod.PROPERTY_NAME, "Cash"));
						obCriteriaPM.setMaxResults(1);
						List<FIN_PaymentMethod> paymentMethodList = obCriteriaPM.list();
						if (!paymentMethodList.isEmpty()) {
							paymentMethod = paymentMethodList.get(0);
						}
                        log.info("Setting payment method in business partner");
						bp.setPaymentMethod(paymentMethod);
						PriceList priceList = null;
						OBCriteria<PriceList> obCriteriaPL = OBDal.getInstance().createCriteria(PriceList.class);
						obCriteriaPL.add(Restrictions.eq(PriceList.PROPERTY_NAME, "Sharaf_Dummy_Pricelist"));
						obCriteriaPL.setFilterOnActive(false);
						priceList = (PriceList) obCriteriaPL.uniqueResult();
                        log.info("Setting price list in bp object");
						bp.setPriceList(priceList);
						OBDal.getInstance().save(bp);
                        log.info("Saved new business partner successfully");
					} catch (Exception e){
						throw new Exception("Error in saving business partner for DGHelp Service", e);
					}
					// User Contact
					User user = new User();
					if (invoiceDetails.get(0).getName() != null) {
                        log.info("Setting name in user window");
						user.setName(invoiceDetails.get(0).getName());
                        log.info("Setting first name in user window");
						user.setFirstName(invoiceDetails.get(0).getName());
					}
					if (invoiceDetails.get(0).getDialIn() != null) {
                        log.info("Setting contact no from invoice in user");
						user.setPhone(invoiceDetails.get(0).getDialIn());
					}
					if (invoiceDetails.get(0).getEMailID() != null) {
                        log.info("Setting email id in user");
						user.setEmail(invoiceDetails.get(0).getEMailID());
					}
					user.setActive(true);
					user.setClient(OBContext.getOBContext().getRole().getClient());
					user.setOrganization(OBDal.getInstance().get(Organization.class, "0"));
					user.setBusinessPartner(bp);
					OBDal.getInstance().save(user);
                    log.info("User details are saved successfully");
					getLocation(invoiceDetails, bp);
				}
			}
		return bp;
		} catch (Exception e){
			throw new Exception("Error in business partner for DGHelp Service", e);
        }

		finally
		{
		 OBDal.getInstance().flush();
		 OBContext.restorePreviousMode();
		}
	 }

	public Location getLocation(List<InvoiceDetails> invoiceDetails, BusinessPartner bp) throws ServletException, Exception {
		Location location;
		if (bp != null && !bp.getBusinessPartnerLocationList().isEmpty()) {
			location = bp.getBusinessPartnerLocationList().get(0);
		} else {
                        log.info("Creating business partner location");
			location = new Location();
			org.openbravo.model.common.geography.Location geoLocation = new org.openbravo.model.common.geography.Location();
			try {
				if (invoiceDetails.get(0).getCity() != null) {
					geoLocation.setCityName(invoiceDetails.get(0).getCity());
				}
				if (invoiceDetails.get(0).getPOBox() != null) {
					geoLocation.setAddressLine2(invoiceDetails.get(0).getPOBox());
				}
				if (invoiceDetails.get(0).getCountryKey() != null) {
					if (!invoiceDetails.get(0).getCountryKey().isEmpty()){
						log.info("Country key is: " +invoiceDetails.get(0).getCountryKey());
						OBCriteria<Country> countryCriteria = OBDal.getInstance().createCriteria(Country.class);
						countryCriteria
								.add(Restrictions.eq(Country.PROPERTY_ISOCOUNTRYCODE, invoiceDetails.get(0).getCountryKey()));
						List<Country> countryList = countryCriteria.list();
						if (!countryList.isEmpty()) {
							geoLocation.setCountry(countryList.get(0));
						}
					}
				} 
				if (invoiceDetails.get(0).getPostalCode() != null) {
					geoLocation.setPostalCode(invoiceDetails.get(0).getPostalCode());
				}
				if (invoiceDetails.get(0).getStreet() != null) {
					geoLocation.setAddressLine1(invoiceDetails.get(0).getStreet());
				}
				geoLocation.setActive(true);
				geoLocation.setClient(OBContext.getOBContext().getRole().getClient());
				geoLocation.setOrganization(OBDal.getInstance().get(Organization.class, "0"));
				OBDal.getInstance().save(geoLocation);
			
				location.setClient(OBContext.getOBContext().getRole().getClient());
				location.setOrganization(OBDal.getInstance().get(Organization.class, "0"));
				location.setActive(true);
				location.setInvoiceToAddress(true);
				location.setShipToAddress(true);
				location.setBusinessPartner(bp);
				location.setLocationAddress(geoLocation);
				OBDal.getInstance().save(location);
				log.info("Business partner location saved successfully");
			} catch (Exception e) {
				throw new Exception("Error in saving business partner location", e);
			} finally {
			}
		}
		return location;
	}
}
