/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.customdevelopments;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.ApplicationScoped;

import org.openbravo.client.kernel.BaseComponentProvider;
import org.openbravo.client.kernel.BaseComponentProvider.ComponentResource.ComponentResourceType;
import org.openbravo.client.kernel.Component;
import org.openbravo.client.kernel.ComponentProvider;
import org.openbravo.retail.posterminal.POSUtils;

@ApplicationScoped
@ComponentProvider.Qualifier(CUSTSHAComponentProvider.QUALIFIER)
public class CUSTSHAComponentProvider extends BaseComponentProvider {

  public static final String QUALIFIER = "CUSTSHA_Main";
  public static final String MODULE_JAVA_PACKAGE = "com.openbravo.sharaf.retail.customdevelopments";
  public static final String MODULE_JAVA_PACKAGE_DGHELP = "com.openbravo.sharaf.retail.customdevelopments.sapws.dghelpquo";

  @Override
  public Component getComponent(String componentId, Map<String, Object> parameters) {
    throw new IllegalArgumentException("Component id " + componentId + " not supported.");
  }

  @Override
  public List<ComponentResource> getGlobalComponentResources() {
    final List<ComponentResource> globalResources = new ArrayList<ComponentResource>();
    final String prefix = "web/" + MODULE_JAVA_PACKAGE + "/js/";
    final String prefixDGHelp = "web/" + MODULE_JAVA_PACKAGE_DGHELP + "/js/";

    String[] resourceList = {
        // components
        "components/actionButtonDiscountReason", "components/actionButtonReturns",
        "components/actionButtonStockType", "components/orderHeaderExtension",
        "components/popupDiscountReason", "components/popupReturnLines", "components/popupReturns",
        "components/popupStockType", "components/changeBpFilter",
        "components/popupReceiptProperties", "components/createcustomersExtension",
        "components/undoExtension", "components/restrictBusinessPartnerCategory",
        "components/actionButtonContainerHeight", "components/restrictCashUp",
        "components/popupBPSelectorExtension", "components/viewcustomerExtension",
        "components/modelForceCashUpPopUp", "components/forceCashUpPopUpForThresholdLimit",
        "lib/googleApiLib", "components/googlemap", "components/serviceFilterExtention",
        "components/promotionalBasisPopUp", "components/captureOriginalInvoiceDetail",
        "components/modalPaidReceiptCaptureOriginalInvoice", "components/customerFeedBack",
        // extend
        "extend/extendCountry", "extend/extendOrderHeader", "extend/extendPOSLayout",
        "extend/extendAdvancedFilterTableComponent",
        // hooks
        "hooks/OBPOS_preChangeBusinessPartner", "hooks/lineSelectedHook",
        "hooks/preAddProductToOrder", "hooks/renderOrderLineHook", "hooks/preSetPriceHook",
        "hooks/preDeleteLine", "hooks/prePrintHook", "hooks/preApplyDiscountHook", "hooks/setBPKey",
        "hooks/prePaymentHook", "hooks/preSetBusinessDateHook", "hooks/preOrderSave",
        "hooks/openReceiptPrePrintHook", "hooks/prePaymentHookForDGMember",
        "hooks/terminalLoadedFromBackend", "hooks/preAddPaymentHook",
        "hooks/OBRETUR_ReturnReceiptApplyHook", "hooks/postAddProductToOrderHook",
        "hooks/postAddPaymentHook", "hooks/OBPOS_preRemovePaymentHook",
        "hooks/prePaymentSalesRepHook", "hooks/OBMOBC_preWindowOpenHook",
        "hooks/OBMOBC_PreWindowNavigateHook", "hooks/OBPOS_PostPaymentDoneHook",
        "hooks/OBPOS_NewReceipt", "hooks/OBPOS_TerminalLoadedFromBackend",
        "hooks/OBPOS_PreDeleteCurrentOrder", "hooks/OBPOS_AfterCashUpSent",
        "hooks/OBPOS_NewReceiptCashupThresholdLimitValidation",
        "hooks/OBPOS_TerminalLoadedFromBackendCashup",
        "hooks/OBPOS_PreWindowNavigateforSystemDateCheck",
        "hooks/prePaymentHookForCityNameValidation", "hooks/OBPOS_PaymentSelectedForPromoBasis",
        "hooks/prePaymentHookforCaptureOriginalInvoice", "hooks/OBPOS_BarcodeScan",
        "hooks/OBPOS_preAddPaymentForCustomerFeedback",
        // model
        "model/addBusinessPartnerProperty", "model/addArabicProductNameProperty",
        "model/addBrandIndexInMproduct", "model/addBPartnerFilterProperty",
        "model/RestrictPaymentMethodConfigModel", "model/transactionCountModel",
        "model/BPLocationProperties", "model/CityModel", "model/addServiceArticleProductProperties",
        "model/CityModel", "model/addBBusinessPartnerPropertyforloyalitycard",
        "model/loadPromotionalBasis",
        // provider
        "provider/chequePaymentProvider",
        // js
        "systemDateAndBusinessdateCheck",
        // utils
        "utils/custshaUtils" };

    String[] resourceListDGHelp = { "components/menu", "components/modalDGHelpQuotationFinder", };

    for (String resource : resourceList) {
      globalResources.add(createComponentResource(ComponentResourceType.Static,
          prefix + resource + ".js", POSUtils.APP_NAME));
    }

    for (String resource : resourceListDGHelp) {
      globalResources.add(createComponentResource(ComponentResourceType.Static,
          prefixDGHelp + resource + ".js", POSUtils.APP_NAME));
    }

    return globalResources;
  }
}
