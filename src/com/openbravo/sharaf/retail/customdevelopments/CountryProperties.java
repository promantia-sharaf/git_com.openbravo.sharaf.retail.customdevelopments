package com.openbravo.sharaf.retail.customdevelopments;

import java.util.ArrayList;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.retail.posterminal.master.Country;

@Qualifier(Country.countryPropertyExtension)
public class CountryProperties extends ModelExtension {

	@Override
	public List<HQLProperty> getHQLProperties(Object params) {
		ArrayList<HQLProperty> list = new ArrayList<HQLProperty>() {
			 private static final long serialVersionUID = 1L;
			 {
				 add(new HQLProperty("c.name", "country"));
				 add(new HQLProperty("c.custshaCountryIddcode", "countryIDDCode"));
				 add(new HQLProperty("c.custshaMobilenoLength", "mobileNoLength"));
			 }		
		};
		return list;
	}
}