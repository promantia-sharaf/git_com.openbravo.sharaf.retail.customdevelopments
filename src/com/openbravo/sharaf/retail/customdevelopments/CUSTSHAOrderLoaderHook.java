/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.customdevelopments;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.base.provider.OBProvider;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBQuery;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.common.order.OrderLine;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.retail.posterminal.OrderLoaderHook;

public class CUSTSHAOrderLoaderHook implements OrderLoaderHook {
  private static Logger log = Logger.getLogger(CUSTSHAOrderLoaderHook.class);

  @Override
  public void exec(JSONObject jsonorder, Order order, ShipmentInOut shipment, Invoice invoice)
      throws Exception {
    boolean isVerifiedReturn = jsonorder.has("isVerifiedReturn")
        ? jsonorder.getBoolean("isVerifiedReturn")
        : false;
    String originalDocNo = null;
    if (jsonorder.has("custshaCustomerFeedback")) {
    String custshaCustomerFeedback = jsonorder.getString("custshaCustomerFeedback");

    if (!custshaCustomerFeedback.isEmpty() && jsonorder.has("documentNo")) {
        String documentNo = jsonorder.getString("documentNo");

        OBCriteria<Order> orderCriteria = OBDal.getInstance().createCriteria(Order.class);
        orderCriteria.add(Restrictions.eq(Order.PROPERTY_DOCUMENTNO, documentNo));

        Order orderObj = (Order) orderCriteria.uniqueResult();
        if (orderObj!= null) {
            orderObj.setCustshaCustomerFeedback(custshaCustomerFeedback);
            OBDal.getInstance().save(orderObj);
        }
    }
}
    if (jsonorder.has("lines")) {
      JSONArray jsonLines = jsonorder.getJSONArray("lines");
      for (int i = 0; i < jsonLines.length(); i++) {
        JSONObject jsonLine = jsonLines.getJSONObject(i);
        if (jsonLine.has("custshaChargeLines")) {
          OrderLine salesOrderline = getOrderLine(order, jsonLine.getString("id"));
          if (salesOrderline != null) {
            JSONArray jsonChargeLines = jsonLine.getJSONArray("custshaChargeLines");
            for (int j = 0; j < jsonChargeLines.length(); j++) {
              String chargeLineId = jsonChargeLines.getString(j);
              OrderLine orderline = getOrderLine(order, chargeLineId);
              if (orderline != null) {
                final CUSRSHA_LineCharge lineCharge = OBProvider.getInstance()
                    .get(CUSRSHA_LineCharge.class);
                lineCharge.setActive(true);
                lineCharge.setChargeOrderline(orderline);
                lineCharge.setSalesOrderLine(salesOrderline);
                OBDal.getInstance().save(lineCharge);
              }
            }
          }
        }
      }

      // for OBDEV- 285

      for (int i = 0; i < jsonLines.length(); i++) {
        JSONObject jsonLine = jsonLines.getJSONObject(i);
        if (isVerifiedReturn && originalDocNo == null) {
          originalDocNo = jsonLine.getString("originalDocumentNo");
          log.info("original doc no" + originalDocNo);
        }
        if (jsonLine.has("product") && jsonLine.getJSONObject("product").has("helpDeskTicket")
            && jsonLine.getJSONObject("product").getString("helpDeskTicket") != null) {
          String helpDiskTicket = jsonLine.getJSONObject("product").getString("helpDeskTicket");
          log.info("helpdesk ticket number" + helpDiskTicket.toString());
          for (OrderLine orderLine : order.getOrderLineList()) {
            if (orderLine.getId().equals(jsonLine.get("id"))) {
              orderLine.setCustshaHelpdeskticket(helpDiskTicket);
              OBDal.getInstance().save(orderLine);
            }
          }
        }
        if (jsonLine.has("custshaOriginvoiceno")) {
          String documentNo = jsonLine.getString("custshaOriginvoiceno").trim();
          String lineNo = jsonLine.has("custshaOriginvoicelineno")
              ? jsonLine.getString("custshaOriginvoicelineno").trim()
              : "";
          OBCriteria<Order> orderCriteria = OBDal.getInstance().createCriteria(Order.class);
          orderCriteria.add(Restrictions.eq(Order.PROPERTY_DOCUMENTNO, documentNo));

          if (orderCriteria.list().size() > 0) {
            Order ord = orderCriteria.list().get(0);
            OBCriteria<OrderLine> ordCriteriaQuery = OBDal.getInstance()
                .createCriteria(OrderLine.class);
            ordCriteriaQuery.add(Restrictions.eq(OrderLine.PROPERTY_SALESORDER, ord));
            ordCriteriaQuery
                .add(Restrictions.eq(OrderLine.PROPERTY_LINENO, Long.parseLong(lineNo)));

            if (ordCriteriaQuery.list().size() > 0) {
              OrderLine ordLine = ordCriteriaQuery.list().get(0);
              String lineId = ordLine.getId();
              OBQuery<OrderLine> origOrderline = OBDal.getInstance().createQuery(OrderLine.class,
                  " id = '" + lineId + "'");
              OrderLine originalLine = OBProvider.getInstance().get(OrderLine.class);
              originalLine = origOrderline.list().get(0);
              OrderLine orderline = getOrderLine(order, jsonLine.getString("id"));
              if (orderline != null) {
                orderline.setCustshaOriginvarticlecode(originalLine.getProduct().getSearchKey());
                orderline.setCustshaOriginvoicelineno(originalLine.getLineNo().toString());
                originalLine.setCustshaOriginvoicecaptured(true);
                OBDal.getInstance().save(originalLine);
                OBDal.getInstance().flush();
              }
            }
          }

        }
      }
      // End 285
    }
  }

  private OrderLine getOrderLine(Order order, String orderLineId) {
    for (OrderLine ol : order.getOrderLineList()) {
      if (orderLineId.equals(ol.getId())) {
        return ol;
      }
    }
    return null;
  }

}
