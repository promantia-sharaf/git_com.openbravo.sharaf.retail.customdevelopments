package com.openbravo.sharaf.retail.customdevelopments.process;

import java.util.List;
import javax.servlet.ServletException;
import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.JSONProcessSimple;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.common.businesspartner.BusinessPartner;

public class CheckEmailAndPhoneNoValidation extends JSONProcessSimple {
  private static Logger log = Logger.getLogger(CheckEmailAndPhoneNoValidation.class);

  @Override
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {
    JSONObject result = new JSONObject();
    JSONObject data = new JSONObject();
    String phoneNo = null;
    String email = null;
    BusinessPartner businessPartner = null;

    log.info("Received JSON object: " + jsonsent.toString());

    if (jsonsent.has("phoneNo") && jsonsent.getString("phoneNo") != null) {
      phoneNo = jsonsent.getString("phoneNo");
      log.info("Phone number to validate: " + phoneNo);
    }

    if (jsonsent.has("email") && jsonsent.getString("email") != null) {
      email = jsonsent.getString("email");
      log.info("Email to validate: " + email);
    }

    try {
      OBContext.setAdminMode(true);

      // Validate Email
      if (email != null) {
        OBCriteria<User> userCritemail = OBDal.getInstance().createCriteria(User.class);
        userCritemail.add(Restrictions.eq(User.PROPERTY_EMAIL, email));
        List<User> emailUserList = userCritemail.list();

        log.info("User list for email check: " + emailUserList);
        if (!emailUserList.isEmpty()) {
          for (User user : emailUserList) {
            businessPartner = user.getBusinessPartner();
            if (businessPartner != null) {
              data.put("isEmailPresent", true);
              break; // Exit loop if one match is found
            }
          }
        }
      }

      // Validate Phone Number
      if (phoneNo != null && !phoneNo.isEmpty()) {
        OBCriteria<User> userCritphoneno = OBDal.getInstance().createCriteria(User.class);
        userCritphoneno.add(Restrictions.eq(User.PROPERTY_PHONE, phoneNo));
        List<User> phoneUserList = userCritphoneno.list();

        log.info("User list for phone number check: " + phoneUserList);
        if (!phoneUserList.isEmpty()) {
          for (User user : phoneUserList) {
            businessPartner = user.getBusinessPartner();
            if (businessPartner != null) {
              data.put("isPhoneNoPresent", true);
              break; // Exit loop if one match is found
            }
          }
        }
      }

      result.put("status", 0);
      result.put("data", data);
    } catch (Exception e) {
      log.error("Error while validating Email and Phone No.: " + e.getMessage(), e);
      result.put("status", 1);
      result.put("message", e.getMessage());
    } finally {
      OBContext.restorePreviousMode();
    }
    return result;
  }
}

