/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.customdevelopments.process;

import java.math.RoundingMode;
import java.util.List;

import javax.servlet.ServletException;

import org.codehaus.jettison.json.JSONArray;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;

import com.openbravo.sharaf.retail.discounts.service.PromotionLimitService;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.hibernate.criterion.Restrictions;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBCriteria;
import org.openbravo.dal.service.OBDal;
import org.openbravo.mobile.core.process.JSONProcessSimple;


public class PaymentMethodUtils extends JSONProcessSimple {
  private static Logger log = Logger.getLogger(PaymentMethodUtils.class);
  public JSONObject exec(JSONObject jsonsent) throws JSONException, ServletException {
    String paymentmethodid = null;
    String paymentName = null;
    JSONObject result = new JSONObject();
    JSONObject data = new JSONObject();
    if (jsonsent.has("paymentmethodid")) {
      paymentmethodid = jsonsent.getString("paymentmethodid");
    }

  
  try {
    OBContext.setAdminMode(true);
    FIN_PaymentMethod paymentMethod = OBDal.getInstance().get(FIN_PaymentMethod.class, paymentmethodid);
    if (paymentMethod != null) {
      paymentName = paymentMethod.getName();
    }
    data.put("paymentName", paymentName);
    result.put("status", 0);
    result.put("data", data);
    log.info("Payment Methood"+result.toString());

  } catch (Exception e) {
    log.error("Error in fetching Payment Name:", e);
    result.put("status", 1);
    result.put("message", e.getMessage());
  } finally {
    OBContext.restorePreviousMode();
  }
  return result;
 }
}
