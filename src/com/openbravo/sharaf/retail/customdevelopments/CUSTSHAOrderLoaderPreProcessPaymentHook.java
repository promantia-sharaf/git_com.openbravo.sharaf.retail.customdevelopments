/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.customdevelopments;

import javax.enterprise.context.ApplicationScoped;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.retail.posterminal.OrderLoaderPreProcessPaymentHook;

@ApplicationScoped
public class CUSTSHAOrderLoaderPreProcessPaymentHook extends OrderLoaderPreProcessPaymentHook {
	  private static Logger log = Logger.getLogger(CUSTSHAOrderLoaderPreProcessPaymentHook.class);

  public void exec(JSONObject jsonorder, Order order, JSONObject jsonpayment, FIN_Payment payment)
      throws Exception {

    if (jsonpayment.has("custsharCustomerName")) {
      String custsharCustomerName = jsonpayment.getString("custsharCustomerName");
      payment.setCustshaCustomerName(custsharCustomerName);
    }
    if (jsonpayment.has("custsharChequeNumber")) {
      String custsharChequeNumber = jsonpayment.getString("custsharChequeNumber");
      payment.setCustshaChequeNumber(custsharChequeNumber);
    }
    if (jsonpayment.has("name") && jsonorder.has("promobasisLines")) {
        String paymentName = jsonpayment.getString("name");

        for (int i = 0; i < jsonorder.getJSONArray("promobasisLines").length(); i++) {
            JSONObject promoLine = jsonorder.getJSONArray("promobasisLines").getJSONObject(i);

            if (promoLine.has("paymentname") && promoLine.getString("paymentname").equals(paymentName)) {
                if (promoLine.has("PromoBasis") && promoLine.has("remarks")) {
                    String promoBasis = promoLine.getString("PromoBasis");
                    payment.setCustshaPromotionalBasis(promoBasis);

                    String remarks = promoLine.getString("remarks");
                    payment.setCustshaRemarks(remarks);
                }
            }
        }
    }

  }
}
