package com.openbravo.sharaf.retail.customdevelopments;

import java.util.Arrays;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.retail.posterminal.term.Terminal;

@Qualifier(Terminal.terminalPropertyExtension)
public class TerminalProperties extends ModelExtension {

	@Override
	public List<HQLProperty> getHQLProperties(Object params) {
				return Arrays.asList(
			            new HQLProperty("pos.organization.obretcoDbpCountryid.custshaMobilenoLength", "defaultbp_bpMobileNoLength")); 
				}
}