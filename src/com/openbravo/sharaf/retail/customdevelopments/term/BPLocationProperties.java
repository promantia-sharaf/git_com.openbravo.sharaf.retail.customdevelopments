/*
 ************************************************************************************
 * Copyright (C) 2023 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, moment, _ */

package com.openbravo.sharaf.retail.customdevelopments.term;

import java.util.Arrays;
import org.apache.log4j.Logger;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.retail.posterminal.master.BPLocation;

@Qualifier(BPLocation.bpLocationPropertyExtension)
public class BPLocationProperties extends ModelExtension {
  private static final Logger log = Logger.getLogger(BPLocationProperties.class);

  @Override
  public List<HQLProperty> getHQLProperties(Object params) {
    return Arrays.asList(new HQLProperty("bploc.custshaLongitude", "custshaLongitude"),
        new HQLProperty("bploc.custshaLatitude", "custshaLatitude"));
  }

}
