/*
 ************************************************************************************
 * Copyright (C) 2023 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, moment, _ */

package com.openbravo.sharaf.retail.customdevelopments.term;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.dal.core.OBContext;
import org.openbravo.erpCommon.businessUtility.Preferences;
import org.openbravo.erpCommon.utility.PropertyException;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.retail.posterminal.master.BPartnerFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

@Qualifier(BPartnerFilter.bPartnerFilterPropertyExtension)
public class BPartnerFilterProperties extends ModelExtension {
  private static final Logger log = Logger.getLogger(BPartnerFilterProperties.class);

  @Override
  public List<HQLProperty> getHQLProperties(Object params) {
    ArrayList<HQLProperty> list = new ArrayList<HQLProperty>();
    try {
      ObjectMapper mapper = new ObjectMapper();
      Map<String, Object> paramsMap = mapper.convertValue(params, Map.class);;
      Boolean location = (Boolean) paramsMap.get("location");
      if(location) {
        list.add(new HQLProperty("bpl.custshaLongitude", "custshaLongitude"));
        list.add(new HQLProperty("bpl.custshaLatitude", "custshaLatitude"));
        return list;
      }
    }catch(Exception e) {
      log.error("Error while fetching BP:", e);
    }
     return list; 
  }

}
