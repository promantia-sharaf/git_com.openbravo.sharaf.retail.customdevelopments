package com.openbravo.sharaf.retail.customdevelopments.term;

import java.util.ArrayList;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.retail.posterminal.master.Product;

@Qualifier(Product.productPropertyExtension)
public class ServiceArticleProductProperties extends ModelExtension {

  @Override
  public List<HQLProperty> getHQLProperties(Object params) {
    ArrayList<HQLProperty> list = new ArrayList<HQLProperty>();
    list.add(new HQLProperty("product.custshaExtdwtyEnabled", "custshaExtdwtyEnabled"));
    list.add(new HQLProperty("product.custshaDgshieldEnabled", "custshaDgshieldEnabled"));
    list.add(new HQLProperty("product.custshaDgspremiumEnabled", "custshaDgspremiumEnabled"));
    list.add(new HQLProperty("product.custshaServiceType", "custshaServiceType"));
    list.add(new HQLProperty("product.custshaApplecareEnabled", "custshaApplecareEnabled"));
    list.add(new HQLProperty("product.custshaDgsplusEnabled", "custshaDgsplusEnabled"));
    list.add(new HQLProperty("product.custshaEwdpdoctypeEnabled", "custshaEwdpdoctypeEnabled"));
    return list;
  }
}