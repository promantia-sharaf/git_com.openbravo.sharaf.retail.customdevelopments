/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
package com.openbravo.sharaf.retail.customdevelopments.term;

import java.util.Arrays;
import java.util.List;

import org.openbravo.client.kernel.ComponentProvider.Qualifier;
import org.openbravo.mobile.core.model.HQLProperty;
import org.openbravo.mobile.core.model.ModelExtension;
import org.openbravo.retail.posterminal.PaidReceipts;

@Qualifier(PaidReceipts.paidReceiptsLinesPropertyExtension)
public class CUSTSHAPaidReceiptsLinesProperties extends ModelExtension {

  @Override
  public List<HQLProperty> getHQLProperties(Object params) {
    return Arrays.asList(new HQLProperty("ordLine.cUSTSHAStockType", "cUSTSHAStockType"),
        new HQLProperty("ordLine.custshaDigiApiResponse", "custshaDigiApiResponse"),
        new HQLProperty("ordLine.custshaDiscountReason.id", "custshaDiscountReason"),
        new HQLProperty("ordLine.custshaOriginvoiceno", "custshaOriginvoiceno"),
        new HQLProperty("ordLine.custshaOriginvoiceserialno", "custshaOriginvoiceserialno"),
        new HQLProperty("ordLine.custshaOriginvoiceimeino", "custshaOriginvoiceimeino"),
        new HQLProperty("ordLine.custshaOriginvoicelineno", "custshaOriginvoicelineno"),
        new HQLProperty("ordLine.custshaOriginvoicecaptured     ", "custshaOriginvoicecaptured"),
        new HQLProperty("ordLine.custshaOriginvarticlecode", "custshaOriginvarticlecode"));

  }
}