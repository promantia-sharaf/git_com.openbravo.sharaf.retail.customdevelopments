/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

package com.openbravo.sharaf.retail.customdevelopments.term;

import java.util.Arrays;
import java.util.List;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.openbravo.retail.posterminal.term.QueryTerminalProperty;

public class DiscountReason extends QueryTerminalProperty {

  @Override
  protected boolean isAdminMode() {
    return true;
  }

  @Override
  protected List<String> getQuery(JSONObject jsonsent) throws JSONException {
    return Arrays
        .asList(new String[] { "select id as id, discountReason as name "
            + "from CUSTSHA_DiscountReason "
            + "where $incrementalUpdateCriteria AND $naturalOrgCriteria and $readableSimpleClientCriteria "
            + "order by sequenceNumber asc" });
  }

  @Override
  public String getProperty() {
    return "sharafDiscountReason";
  }

  @Override
  public boolean returnList() {
    return false;
  }
}