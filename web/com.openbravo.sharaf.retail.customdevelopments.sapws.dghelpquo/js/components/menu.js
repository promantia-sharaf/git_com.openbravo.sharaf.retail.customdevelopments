/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {

  enyo.kind({
    name: 'CUSTSHA.UI.MenuAddDGHelpQuotation',
    kind: 'OB.UI.MenuAction',
    i18nLabel: 'CUSTSHA_DGHelpQuotation',
    permission: 'CUSTSHA_menuDGHelpQuotation',
    events: {
      onShowPopup: ''
    },
    tap: function () {
        var me = this;
        var connectedCallback = function () {
            if (OB.MobileApp.model.hasPermission(me.permission)) {
              me.doShowPopup({
                popup: 'CUSTSHA_ModalDGHelpQuotationFinder'
              });
            }
            };
        var notConnectedCallback = function () {
            OB.UTIL.showError(OB.I18N.getLabel('OBPOS_OfflineWindowRequiresOnline'));
            return;
            };
        if (this.disabled) {
          return true;
        }
        this.inherited(arguments); // Manual dropdown menu closure
        if (!OB.MobileApp.model.get('connectedToERP')) {
          OB.UTIL.checkOffLineConnectivity(500, connectedCallback, notConnectedCallback);
        } else {
          connectedCallback();
        }
    }
  });

  // Register menu items
  OB.OBPOSPointOfSale.UI.LeftToolbarImpl.prototype.menuEntries.push({
    kind: 'CUSTSHA.UI.MenuAddDGHelpQuotation'
  });

}());