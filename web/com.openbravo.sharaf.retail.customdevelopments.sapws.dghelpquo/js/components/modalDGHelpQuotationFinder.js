/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, Backbone, _ */

enyo.kind({
  name: 'CUSTSHA.UI.DGHelpQuotationScrollableHeader',
  kind: 'OB.UI.ScrollableTableHeader',
  events: {
	    onSearchAction: '',
	    onClearAction: ''
	  },
	  handlers: {
	  },
	  components: [{
	    style: 'padding: 10px;',
	    components: [{
	      style: 'display: table;',
	      components: [{
	        style: 'display: table-cell; width: 100%;',
	        components: [{
	          kind: 'OB.UI.SearchInputAutoFilter',
	          name: 'filterText',
	          style: 'width: 100%',
	          skipAutoFilterPref: 'OBPOS_remote.order'

	        }]
	      }, {
	        style: 'display: table-cell;',
	        components: [{
	          kind: 'OB.UI.SmallButton',
	          classes: 'btnlink-gray btn-icon-small btn-icon-clear',
	          style: 'width: 100px; margin: 0px 5px 8px 19px;',
	          ontap: 'clearAction'
	        }]
	      }, {
	        style: 'display: table-cell;',
	        components: [{
	          kind: 'OB.UI.SmallButton',
	          classes: 'btnlink-yellow btn-icon-small btn-icon-search',
	          style: 'width: 100px; margin: 0px 0px 8px 5px;',
	          ontap: 'searchAction'
	        }]
	      }]
	    }]
	  }],
	  disableFilterText: function (value) {
	    this.$.filterText.setDisabled(value);
	  },
	  clearAction: function () {
	    if (!this.$.filterText.disabled) {
	      this.$.filterText.setValue('');
	    }
	    this.doClearAction();
	    //this.searchAction();
	  },
	  searchAction: function () {
	    this.filters = {
	      filterText: this.$.filterText.getValue()
	    };
	    this.doSearchAction({
	      filters: this.filters
	    });
	    return true;
	  },
	  initSearchAction: function () {
	    this.disableFilterText(false);
	    this.clearAction();
	   // this.searchAction();
	  }
});

enyo.kind({
  name: 'CUSTSHA.UI.DGHelpQuotationLine',
  kind: 'OB.UI.listItemButton',
  events: {
    onHideThisPopup: ''
  },
  tap: function () {
	    this.inherited(arguments);
	    this.doHideThisPopup();
  },
  components: [{
    name: 'line',
    style: 'line-height: 23px;',
    components: [{
      name: 'topLine'
    }, {
      style: 'color: #888888',
      name: 'bottomLine'
    }, {
      style: 'clear: both;'
    }]
  }],
  create: function () {
    this.inherited(arguments);
    this.$.topLine.setContent(this.model.get('documentNo') + ' - ' + this.model.get('businessPartnerName'));
    this.$.bottomLine.setContent(this.model.get('totalamount'));
    this.render();
  }
});

enyo.kind({
  name: 'CUSTSHA.UI.DGHelpQuotation',
  classes: 'row-fluid',
  handlers: {
    onSearchAction: 'searchAction',
    onClearAction: 'clearAction'
  },
  events: {
    onChangeFilterSelector: '',
    onShowPopup: '',
    onChangeCurrentOrder: ''
  },
  components: [{
    classes: 'span12',
    components: [{
      style: 'border-bottom: 1px solid #cccccc;',
      classes: 'row-fluid',
      components: [{
        classes: 'span12',
        components: [{
          name: 'dgHelpTable',
          kind: 'OB.UI.ScrollableTable',
          classes: 'bp-scroller',
          scrollAreaMaxHeight: '400px',
          renderHeader: 'CUSTSHA.UI.DGHelpQuotationScrollableHeader',
          renderLine: 'CUSTSHA.UI.DGHelpQuotationLine',
          renderEmpty: 'OB.UI.RenderEmpty'
        }, {
          name: 'renderLoading',
          style: 'border-bottom: 1px solid #cccccc; padding: 20px; text-align: center; font-weight: bold; font-size: 30px; color: #cccccc',
          showing: false,
          initComponents: function () {
            this.setContent(OB.I18N.getLabel('OBPOS_LblLoading'));
          }
        }]
      }]
    }]
  }],
  clearAction: function (inSender, inEvent) {
    this.itemsList.reset();
    return true;
  },
  searchAction: function (inSender, inEvent) {
    var me = this,
        ordersLoaded = [],
        process = new OB.DS.Process('com.openbravo.sharaf.retail.customdevelopments.sapws.dghelpquo.processes.DGHelpQuotation'),
        i;
    me.filters = inEvent.filters;
    this.clearAction();
    this.$.dgHelpTable.$.tempty.hide();
    this.$.dgHelpTable.$.tbody.hide();
    this.$.dgHelpTable.$.tlimit.hide();
    this.$.renderLoading.show();
    var limit;

    process.exec({
      filters: me.filters
    }, function (data) {
      if (data && data[0]) {
        ordersLoaded = [];
		var order = new OB.Model.Order();
		order.set("businessPartnerName",data[0].businessPartnerName);
		order.set("bp",data[0].bp);
		order.set("product",data[0].product);
		order.set("totalNetAmount", data[0].totalNetAmount);
		order.set("totalamount",data[0].totalamount);
		order.set("documentNo",data[0].custshaWebsiterefno);
        ordersLoaded.push(order);
		me.attributes.data = data;
		me.itemsList.reset(ordersLoaded);
        me.$.renderLoading.hide();
        if (data && data.length > 0) {
          me.$.dgHelpTable.$.tbody.show();
        } else {
          me.$.dgHelpTable.$.tempty.show();
        }
        me.$.dgHelpTable.getScrollArea().scrollToTop();
      } else {
	OB.UTIL.showError(data.exception.status.message);
       // OB.UTIL.showError(OB.I18N.getLabel('OBPOS_MsgErrorDropDep'));
      }
    }, function (error) {
      if (error) {
        OB.UTIL.showError(OB.I18N.getLabel('OBPOS_OfflineWindowRequiresOnline'));
      }
    });
    return true;
  },
  itemsList: null,
  init: function (model) {
    var me = this;
    this.model = model;
    this.itemsList = new Backbone.Collection();
    this.$.dgHelpTable.setCollection(this.itemsList);

    this.itemsList.on('click', function (model) {
	  if(OB.MobileApp.model.orderList.current && OB.MobileApp.model.orderList.current.get('lines').length !== 0){
		OB.MobileApp.model.orderList.saveCurrent();
		OB.MobileApp.model.orderList.current =OB.MobileApp.model.orderList.newOrder();
		OB.MobileApp.model.orderList.unshift(OB.MobileApp.model.orderList.current);
		OB.MobileApp.model.orderList.loadCurrent(true);
	  }
	  OB.Dal.get(OB.Model.BusinessPartner, model.get('bp'), function (bp) {
		var docType = OB.CUSTSDT.Utils.getDocumentTypeBySearchKey('PI');
		OB.CUSTSDT.Utils.changeDocumentType(OB.MobileApp.model.receipt, new Backbone.Model(docType));
                OB.CUSTSHA.updateBPForDGHelp(OB.MobileApp.model, model.get('bp'), function() {
                            });
              /* OB.MobileApp.model.receipt.setBPandBPLoc(bp, false, true); */
	        OB.Dal.get(OB.Model.Product,model.get('product'), function(p){
			p.set('standardPrice',parseInt(model.get('totalNetAmount').replace(",","")));
			OB.MobileApp.model.receipt.createLine(p, 1,  null, '');
			OB.MobileApp.model.receipt.set('custshaWebsiterefno',model.get('documentNo'));
		});
      });  
      return true;
    }, this);
  }
});

/* Modal definition */
enyo.kind({
  kind: 'OB.UI.Modal',
  name: 'CUSTSHA.UI.ModalDGHelpQuotationFinder',
  topPosition: '125px',
  i18nHeader: 'CUSTSHA_LblDGHelpQuotationFinderCaption',
  published: {
	    params: null
  },
  body: {
    kind: 'CUSTSHA.UI.DGHelpQuotation'
  },
  executeOnShow: function () {
/*    if (!this.initialized) {
      this.inherited(arguments);
      this.getFilterSelectorTableHeader().searchAction();
    }*/
	this.$.body.$.dGHelpQuotation.$.dgHelpTable.$.theader.$.dGHelpQuotationScrollableHeader.initSearchAction();
  },
  getFilterSelectorTableHeader: function () {
    //return this.$.body.$.documentTypeList.$.docTypeTable.$.theader.$.documentTypeScrollableHeader.$.filterSelector;
  },
  init: function (model) {
    //this.inherited(arguments);
	var me = this;
	this.model = model;
  }
});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'CUSTSHA.UI.ModalDGHelpQuotationFinder',
  name: 'CUSTSHA_ModalDGHelpQuotationFinder'
});
