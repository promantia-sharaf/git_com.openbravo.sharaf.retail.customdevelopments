OB.OBPOSPointOfSale.UI.Scan.extend({

  components: [{
    style: 'position:relative; background-color: #7da7d9; background-size: cover; color: white; height: 200px; margin: 5px; padding: 5px',
    components: [{
      kind: 'OB.UI.Clock',
      classes: 'pos-clock'
    }, {
      components: [{
        name: 'msgwelcome',
        showing: false,
        style: 'padding: 10px;',
        components: [{
          style: 'float:right;',
          name: 'msgwelcomeLbl'
        }]
      }, {
        name: 'msguser',
        showing: true,
        style: 'padding: 10px;',
        components: [{
          style: 'float:right;',
          name: 'msgUser'
        }]
      }, {
        name: 'msgrole',
        showing: true,
        style: 'padding: 10px;',
        components: [{
          style: 'float:right;',
          name: 'msgRole'
        }]
      }, {
        name: 'msgaction',
        showing: false,
        components: [{
          name: 'txtaction',
          style: 'overflow-x:hidden; overflow-y:auto; max-height:134px; padding: 10px; float: left; width: 400px; line-height: 23px',
          classes: 'enyo-scroller span7'
        }, {
          style: 'float: right;',
          components: [{
            name: 'undobutton',
            kind: 'OB.UI.SmallButton',
            i18nContent: 'OBMOBC_LblUndo',
            classes: 'btnlink-white btnlink-fontblue',
            tap: function () {
              var me = this,
                  undoaction = this.undoaction;
              this.setDisabled(true);
              OB.UTIL.HookManager.executeHooks('OBPOS_PreUndo_' + undoaction, {
                undoBtn: me,
                order: OB.MobileApp.model.receipt,
                selectedLines: OB.MobileApp.model.receipt.get('undo').lines
              }, function (args) {
                if (!args.cancellation && me.undoclick) {
                  me.undoclick();
                } else {
                  me.setDisabled(false);
                }
                OB.UTIL.HookManager.executeHooks('OBPOS_PostUndo_' + undoaction, {
                  undoBtn: me,
                  order: OB.MobileApp.model.receipt,
                  selectedLines: args.selectedLines
                });
              });
            },
            init: function (model) {
              this.model = model;
            }
          }]
        }]
      }, {
        kind: 'OB.OBPOSPointOfSale.UI.InDevHeader',
        style: 'height: 35px;',
        name: 'divInDevHeader'
      }]
    }]
  }],

  initComponents: function () {
    var user = 'User: ' + OB.MobileApp.model.get('context').user.name;
    var role = 'Role: ' + OB.MobileApp.model.get('context').role.name;
    this.inherited(arguments);
    this.$.msgwelcomeLbl.setContent(OB.I18N.getLabel('OBPOS_WelcomeMessage'));
    this.$.msgUser.setContent(user);
    this.$.msgRole.setContent(role);
  }
});