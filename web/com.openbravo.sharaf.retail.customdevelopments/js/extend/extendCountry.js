/**
 * 
 */
//To add country name, IDD code and mobile no length. 

OB.Model.Country.addProperties([{
	    name: 'country',
	    column: 'name',
	    type: 'TEXT'
	  }, {
		name: 'countryIDDCode',
		column: 'custshaCountryIddcode',
		type: 'TEXT'
	  }, {
		name: 'mobileNoLength',
		column: 'custshaMobilenoLength',
		type: 'TEXT'
}]);

