OB.UI.ModalAdvancedFilterBP.extend({
    executeOnShow: function() {
        var me = this;
        if (this.args.callback) {
            this.callback = this.args.callback;
            if (!this.presetLoaded) {
                this.presetLoaded = true;
                this.$.body.$.filters.clearAll();
            }
            this.filtersToApply = null;
        }
        // Find first component to focus
        var firstFilter = _.find(this.$.body.$.filters.filters, function(filter) {
            return filter.getShowing() && !filter.children[1].children[0].disabled;
        });
        if (firstFilter) {
            // Focus component after the focus set on "Apply Filters" button
            setTimeout(function() {
                firstFilter.children[1].children[0].focus();
            }, 200);
        }
        if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('BPDetailsFromBarcodeScan'))) {
            var barCodeScanDetails = localStorage.getItem('BPDetailsFromBarcodeScan');
            if (barCodeScanDetails) {
                let customerData = JSON.parse(barCodeScanDetails);
                let wp_id = customerData.wp_id;
                let email = customerData.email;
                let mobile = customerData.mobile;
                let first_name = customerData.first_name;
                let last_name = customerData.last_name;
                let full_name = first_name + " " + last_name;
                if (!OB.UTIL.isNullOrUndefined(me.$.body) && !OB.UTIL.isNullOrUndefined(me.$.body.$.filters)) {
                    me.$.body.$.filters.$.inputbpName.setValue(full_name);
                    me.$.body.$.filters.$.inputemail.setValue(email);
                    me.$.body.$.filters.$.inputphone.setValue(mobile);
                }
            }
        }
        return true;
    }
});