/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_PreDeleteLine', function (args, callbacks) {
    enyo.$.scrim.show();
    if (args.cancelOperation) {
      enyo.$.scrim.hide();
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
    var error = false,
        payments = _.find(args.order.get('payments').models, function (payment) {
        return payment.get('kind') === 'CPSEFT_payment.card_Mashreq' || payment.get('kind') === 'CUSTSPM_payment.card_MasterCard' || payment.get('kind') === 'CUSTSPM_payment.giftcard' || payment.get('kind') === 'CUSTSPM_payment.card_VISA' || payment.get('kind') === 'CUSTSPM_payment.creditcard' || payment.get('kind') === 'CUSTSPM_payment.card_AMEX' || payment === 'CUSTSPM_payment.card_UnionPay' || payment.get('kind') === 'CUSTSPM_payment.yougotagift' || payment.get('kind') === 'GCNV_payment.creditnote';
      });
      var spotiiRelatedLine = false;
      _.each(args.selectedLines, function(selLine) {
          if (selLine.get("product").get("linkedServiceArticle")) {
              spotiiRelatedLine = true;
          }
      });
    if (payments && !spotiiRelatedLine) {
      args.cancelOperation = true;
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTSHA_CannotDeleteItem'));
      enyo.$.scrim.hide();
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      return;
    } 

    _.each(args.selectedLines, function (selLine) {
      var chargeLines = _.filter(args.order.get('lines').models, function (ol) {
        return ol.get('custshaChargeLines') && _.find(ol.get('custshaChargeLines'), function (l) {
          return l === selLine.get('id');
        });
      });
      if (chargeLines.length > 0 && !error) {
        error = true;
        args.cancelOperation = true;
        enyo.$.scrim.hide();
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        var info = [];
        info.push(OB.I18N.getLabel('CUSTSHA_ErrDeleteChargeLine', [selLine.get('product').get('_identifier')]));
        info.push(OB.I18N.getLabel('CUSTSHA_ErrDeleteChargeLineRelated'));
        _.each(chargeLines, function (l) {
          info.push('- ' + l.get('product').get('_identifier'));
        });
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), info);
      }
    });

    if (!error) {
      enyo.$.scrim.hide();
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  });

}());