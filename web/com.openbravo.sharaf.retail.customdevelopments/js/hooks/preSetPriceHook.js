/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, _ */

OB.UTIL.HookManager.registerHook('OBPOS_PreSetPrice', function (args, callbacks) {
  if (args.cancellation) {
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    return;
  }

    var docSearchKey = args.context.attributes.custsdtDocumenttypeSearchKey;
    var allowModifyPrice = false;
    for(var i=0; i < OB.MobileApp.model.attributes.sharafDocumentType.length; i++) {
        if(OB.MobileApp.model.attributes.sharafDocumentType[i].searchKey === docSearchKey) {
           if(OB.MobileApp.model.attributes.sharafDocumentType[i].custshaAllowPriceModify) {
            allowModifyPrice = true;
           } 
       }
    }
    if((!allowModifyPrice && args.line.get('product').get('productType') !== 'S') || !OB.MobileApp.model.attributes.permissions["OBPOS_order.changePrice"]) {
        args.cancellation = true;
     if (args.line.get('product').get('productType') !== 'S') {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTSHA_ProductNotPriceEditable'));
     }
   } else {
        args.cancellation = false;
   }
  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
});