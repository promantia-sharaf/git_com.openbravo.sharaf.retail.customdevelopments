/*global OB, enyo, _ */
(function() {
	OB.UTIL.HookManager.registerHook('OBPOS_AfterCashUpSent', function(args, callbacks) {
		localStorage.setItem('OrderCount', 0);
		if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('transactionCount')) ) {
			localStorage.setItem('transactionCount', 0);
			OB.UTIL.HookManager.callbackExecutor(args, callbacks);
			var query = "insert into CUSTSHA_transactioncount(id, posterminal, count) values"
				+ "('" + OB.UTIL.get_UUID() + "',"
				+ "'" + OB.MobileApp.model.get('terminal').id + "',"
				+ "'" + localStorage.getItem('transactionCount') + "'"
				+ " );"
			OB.Dal.queryUsingCache(OB.Model.CUSTSHA_transactioncount, query, [], function(success) {

			});

		} else {
			OB.UTIL.HookManager.callbackExecutor(args, callbacks);
		}

	});
}());