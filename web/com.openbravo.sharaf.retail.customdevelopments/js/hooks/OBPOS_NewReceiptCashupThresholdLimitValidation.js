/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, _ */
   
  
   (function() {                    

    OB.UTIL.HookManager.registerHook('OBPOS_NewReceipt', function(args, callbacks) {
        var currentcash =0.0;
        var percentage = 0;
        var enabledCashupPaymentData = new Array();
                             
        // if only system lvl preference present, then don't proceed to check DGMember
       if (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.get('permissions').CUSTSHA_CashupThresholdLimit) 
            && !OB.UTIL.isNullOrUndefined(OB.MobileApp.model.get('permissions').CUSTSHA_CashupThresholdLimit) 
            && OB.MobileApp.model.get('permissions').CUSTSHA_CashupThresholdLimit !== '0') {
       
            var thresholdlimit =  Number(OB.MobileApp.model.attributes.permissions.CUSTSHA_CashupThresholdLimit);
            if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('enabledCashupPaymentData')) && !OB.UTIL.isNullOrUndefined(localStorage.getItem('enabledCashupPaymentData')).enabledCashupPaymentData) {
               enabledCashupPaymentData = JSON.parse(localStorage.getItem('enabledCashupPaymentData')).enabledCashupPaymentData;
            }
            OB.MobileApp.model.get('payments').forEach(function (payment) {
            if (!OB.UTIL.isNullOrUndefined(enabledCashupPaymentData[payment.paymentMethod.searchKey]) && enabledCashupPaymentData[payment.paymentMethod.searchKey]) {
                currentcash = currentcash + payment.currentCash;
             }
            }); 
            percentage = (currentcash * 100)/thresholdlimit 
          
             console.log('currentcash = '+currentcash);
             console.log(percentage);
             
             if(thresholdlimit > 0 && currentcash >0 && currentcash >= thresholdlimit ) {
                   OB.MobileApp.view.$.containerWindow.showPopup(
                    'CUSTSHA_UI_ThresholdForceCashUpPopUp',
                     {
                        args: args,
                        callbacks: callbacks,
                    }
                  );
                 OB.UTIL.HookManager.callbackExecutor(args, callbacks);
             } else if (percentage >= 90){
                 OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTSHA_CashupEnableThresholdAlertMessage', [percentage]));
                 OB.UTIL.HookManager.callbackExecutor(args, callbacks);
             } else{
                 OB.UTIL.HookManager.callbackExecutor(args, callbacks);
             }          
       } else {
           OB.UTIL.HookManager.callbackExecutor(args, callbacks);
       }
   
   });  
    
}());
