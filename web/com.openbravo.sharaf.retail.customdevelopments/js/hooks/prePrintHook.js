/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPRINT_PrePrint', function (args, callbacks) {
    if (args.forcePrint && args.order.get('isEditable')) {
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), OB.I18N.getLabel('CUSTSHA_ErrPrintOpenDocument'));
      args.cancelOperation = true;
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    } else {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  });

}());