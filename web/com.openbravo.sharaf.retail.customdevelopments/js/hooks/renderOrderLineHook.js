/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, _ */

OB.UTIL.HookManager.registerHook('OBPOS_RenderOrderLine', function (args, callbacks) {
  var cUSTSHAStockType = args.orderline.model.get('cUSTSHAStockType');
  if (cUSTSHAStockType) {
    var stockType = '',
        i = 0;
    for (i; i < OB.MobileApp.model.get('sharafStockType').length; i++) {
      if (OB.MobileApp.model.get('sharafStockType')[i].searchKey === cUSTSHAStockType) {
        stockType = OB.MobileApp.model.get('sharafStockType')[i].name;
        break;
      }
    }
    args.orderline.createComponent({
      style: 'float: left; width: 80%; display: block;',
      components: [{
        content: '-- ' + OB.I18N.getLabel('CUSTSHA_StockType') + ': ' + stockType,
        attributes: {
          style: 'clear: left;'
        }
      }]
    });
  }

  var custshaChargeLines = args.orderline.model.get('custshaChargeLines');
  if (custshaChargeLines) {
    var orderlines = OB.MobileApp.model.receipt.get('lines').models;
    _.each(custshaChargeLines, function (cl) {
      var orderline = _.find(orderlines, function (l) {
        return l.get('id') === cl;
      });
      if (orderline) {
        args.orderline.createComponent({
          style: 'float: left; width: 80%; display: block;',
          components: [{
            content: '-- ' + orderline.get('product').get('_identifier'),
            attributes: {
              style: 'clear: left;'
            }
          }]
        });
      }
    });
  }
  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
});