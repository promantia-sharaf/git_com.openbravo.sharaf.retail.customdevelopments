/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, moment, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_PrePaymentHook', function (args, callbacks) {
    var receipt = args.context.get('order');
    var lines = [];
    lines = receipt.get('lines').models;
    
    var endCustomer = false;
    var dtKey = receipt.get('custsdtDocumenttypeSearchKey');
    var isReturn = false;
      
    var sharafDocArr = OB.MobileApp.model.get('sharafDocumentType');
    for (i = 0; i < sharafDocArr.length; i++) {
      if (sharafDocArr[i].searchKey === dtKey && sharafDocArr[i].shfptEndCustomer) {
        endCustomer = true;
      }
      
      if (sharafDocArr[i].searchKey === dtKey) {
	     if (sharafDocArr[i].returnSequence) {
		    isReturn = true;
	     }
      }      
    }
        
    // Check for multiple sales representatives
    if (receipt.get('custsdtDocumenttype') && OB.COSRD.getSearchKeyFromDocumentType(receipt.get('custsdtDocumenttype'))
         && OB.COSRD.getSearchKeyFromDocumentType(receipt.get('custsdtDocumenttype')).ticketSequence && receipt.get('lines').length > 1) {
        var i, j;
        for (i = 0; i < lines.length; i++) {
            for (j = i + 1; j < lines.length; j++) {
                if (lines[i].get('product').get('productType') !== 'S' && lines[j].get('product').get('productType') !== 'S'
                   && !lines[i].get('custdisAddByPromo') && !lines[j].get('custdisAddByPromo')) {
                    if (lines[i].get('product').get('id') == lines[j].get('product').get('id')
                        && lines[i].get('shasrSalesrep') != lines[j].get('shasrSalesrep')) {
                         if (OB.UTIL.isNullOrUndefined(lines[i].get('custdisOrderline')) && OB.UTIL.isNullOrUndefined(lines[j].get('custdisOrderline'))) {
                             args.cancellation = true;
                             OB.UTIL.showConfirmation.display('Same product cannot be Sold with Different Sales Rep ID. Different Sales Rep ID Captured for Product ' + 
                               lines[i].get('product').get('searchkey') + ' ' + lines[i].get('product').get('_identifier') + ' Please correct');
                         }  
                    }
                }
            }
        }
    }
    if (!receipt.get('isVerifiedReturn') && !isReturn && !receipt.get('isQuotation') ) {
	    for (i = 0; i < lines.length; i++) {
	        for (j = i; j < lines.length; j++) {
	           if (OB.MobileApp.model.get('permissions').CUSTSHA_Alloweddeliverytype_googlemap && 
	                 OB.MobileApp.model.get('permissions').CUSTSHA_Alloweddeliverytype_googlemap.includes(lines[i].get('cUSTDELDeliveryCondition'))) {
		           if (OB.UTIL.isNullOrUndefined(receipt.get('custshaLatitude')) || OB.UTIL.isNullOrUndefined(receipt.get('custshaLongitude'))) {
			              if (!OB.UTIL.isNullOrUndefined(receipt.get('bp').get('locationModel')) && !OB.UTIL.isNullOrUndefined(receipt.get('bp').get('locationModel').get('custshaLongitude'))
			                    && !OB.UTIL.isNullOrUndefined(receipt.get('bp').get('locationModel').get('custshaLatitude')) && !endCustomer) {
			            	  receipt.set('custshaLatitude', +receipt.get('bp').get('locationModel').get('custshaLatitude'));
			            	  receipt.set('custshaLongitude', +receipt.get('bp').get('locationModel').get('custshaLongitude'));
			              } else if(!OB.UTIL.isNullOrUndefined(receipt.get('bp').get('locationBillModel')) && !OB.UTIL.isNullOrUndefined(receipt.get('bp').get('locationBillModel').custshaLongitude)
			                    && !OB.UTIL.isNullOrUndefined(receipt.get('bp').get('locationBillModel').custshaLatitude) && !endCustomer) {
			            	  receipt.set('custshaLatitude', +receipt.get('bp').get('locationBillModel').custshaLatitude);
			            	  receipt.set('custshaLongitude', +receipt.get('bp').get('locationBillModel').custshaLongitude);
			              } else if (!OB.UTIL.isNullOrUndefined(receipt.get('bp').get('custshaLatitude')) && !OB.UTIL.isNullOrUndefined(receipt.get('bp').get('custshaLongitude')) && !endCustomer) {
			            	  receipt.set('custshaLatitude', +receipt.get('bp').get('custshaLatitude'));
			            	  receipt.set('custshaLongitude', +receipt.get('bp').get('custshaLongitude'));
			              }else {
			                  args.cancellation = true;
			                  if (OB.MobileApp.model.get('sharafDeliveryConditions') && 
			                      OB.MobileApp.model.get('sharafDeliveryConditions').find(o => o.searchKey == lines[i].get('cUSTDELDeliveryCondition'))) {
				                  OB.UTIL.showConfirmation.display('Mandatory to capture the latitude and longitude coordinates for ' +
	                                OB.MobileApp.model.get('sharafDeliveryConditions').find(o => o.searchKey == lines[i].get('cUSTDELDeliveryCondition')).name
	                                  + ' items. Please select these coordinates in the customer information section.');
			                  } else {
				                    OB.UTIL.showConfirmation.display('Mandatory to capture the latitude and longitude coordinates.'                           
	                                  + ' Please select these coordinates in the customer information section.');
			                  }
	                      }
		           }
	           }
	        }
	      }
      }
   OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });

}());
