/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {
	OB.CUSTSHA = OB.CUSTSHA || {};
    OB.CUSTSHA.showpromo = OB.CUSTSHA.showpromo || {};
    OB.UTIL.HookManager.registerHook('OBMOBC_PrePaymentSelected', function (args, callbacks) {
        if (!OB.UTIL.isNullOrUndefined(args.paymentSelected)) {
        OB.CUSTSHA.showpromo = true;
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      }
    });
   // ------------------------------------------------------------
    OB.UTIL.HookManager.registerHook('OBPOS_PaymentSelected', function(args, callbacks) {
    	if(!OB.UTIL.isNullOrUndefined(args.paymentSelected) && !OB.UTIL.isNullOrUndefined(args.paymentSelected.paymentMethod) && !OB.UTIL.isNullOrUndefined(OB.CUSTSHA.showpromo) && OB.CUSTSHA.showpromo){
    	    var promoList = [];
    		var paymentMethodId = args.paymentSelected.paymentMethod.paymentMethod;
    		var query = "SELECT * FROM custsha_prombasis where fINPaymentmethod = '" + paymentMethodId + "'";
    	    OB.Dal.queryUsingCache(OB.Model.PromotionalBasis, query, [], function(promoBasis) {
    	    	if (promoBasis.length > 0) {
    	    		for(var i=0; i<promoBasis.models.length;i++){
    	    			var promotionBasis = promoBasis.models[i].get('PromotionalBasis');
    	    			promoList.push(promotionBasis);
    	    		}
    	    		args.order.set('PromoLists',promoList);
    	            OB.MobileApp.view.$.containerWindow.showPopup(
    	                    'CUSTSHA_UI_PromotionalBasisInfo',
    	                  {
    	                    args: args,
    	                    callbacks: callbacks,
    	                    order: args.order,
    	                  }
    	              );
    	    	}else{
    	    		   OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    	    	}
    	    });
    	}else{
    		   OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    	}	
    	});
    //------------------------------------------------------------------
    OB.UTIL.HookManager.registerHook('OBPOS_PreOrderSave', function(args, callbacks) {
    	OB.CUSTSHA.showpromo = false;
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    });
    
	
}());

