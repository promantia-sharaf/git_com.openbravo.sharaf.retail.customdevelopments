/*
Promantia Development
*/

OB.UTIL.HookManager.registerHook('OBPOS_preAddPayment', function(
    args,
    callbacks
  ) {
    var salesDocTypesArr = [];
    for (
      var i = 0;
      i < OB.MobileApp.model.attributes.sharafDocumentType.length;
      i++
    ) {
      var docs = OB.MobileApp.model.attributes.sharafDocumentType[i];
      if (docs.ticketSequence && !docs.returnSequence) {
        salesDocTypesArr.push(docs.searchKey);
      }
    }
    if (
      salesDocTypesArr.includes(
        OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey')
      )
    ) {
      var count = 0;
      var configuredPaymentMethodCount = 0;
      var configuredPaymentMethods;
      var requiredPaymentsAdded;
      var paymentMethodsAdded = [];
      var searchkey, productname;
      let uniquePaymentMethods = [];
      var totalPaymentAmountFlag = false;
      var ConfiguredPaymentsAdded;
      var productMsg = [];
      var paymentTobeadded;
      var configuredPaymentFlag;
      var configuredPaymentAddedOrNot = false;
      var linecount = 0;
      if (!OB.UTIL.isNullOrUndefined(args.paymentToAdd.get('kind'))) {
        paymentTobeadded =
          OB.MobileApp.model.paymentnames[args.paymentToAdd.get('kind')]
            .paymentMethod.paymentMethod$_identifier;
      }
      if (
        !OB.UTIL.isNullOrUndefined(args.receipt.get('payment')) &&
        !OB.UTIL.isNullOrUndefined(args.paymentToAdd.get('amount'))
      ) {
        var totalPaymentAmount;
        var convertedAmount;
        if (!OB.UTIL.isNullOrUndefined(args.paymentToAdd.get('rate'))) {
          convertedAmount = Math.round(
            args.receipt.attributes.gross / args.paymentToAdd.attributes.rate
          );
          totalPaymentAmount = Math.round(
            args.receipt.get('payment') / args.paymentToAdd.get('rate') +
              args.paymentToAdd.get('amount')
          );
          if (
            totalPaymentAmount === convertedAmount ||
            totalPaymentAmount >= convertedAmount
          ) {
            totalPaymentAmountFlag = true;
          }
        } else {
          totalPaymentAmount =
            args.receipt.get('payment') + args.paymentToAdd.get('amount');
          if (
            totalPaymentAmount === args.receipt.get('gross') ||
            totalPaymentAmount >= args.receipt.get('gross')
          ) {
            totalPaymentAmountFlag = true;
          }
        }
      }
      if (!OB.UTIL.isNullOrUndefined(args.receipt.get('payments'))) {
        args.receipt.get('payments').models.forEach(function(model) {
          paymentMethodsKind = model.get('kind');
          paymentMethodsAdded.push(
            OB.MobileApp.model.paymentnames[paymentMethodsKind].paymentMethod
              .paymentMethod$_identifier
          );
        });
      }
      if (!OB.UTIL.isNullOrUndefined(args.receipt.get('lines'))) {
        args.receipt.get('lines').models.forEach(function(model) {
          if (
            !OB.UTIL.isNullOrUndefined(
              model.get('product').get('configuredPaymentMethod')
            ) &&
            model.get('product').get('configuredPaymentMethod').length > 0
          ) {
            ConfiguredPaymentsAdded = false;
            configuredPaymentFlag = false;
            requiredPaymentsAdded = false;
            configuredPaymentMethods = [];
            searchkey = model.get('product').get('searchkey');
            productname = model.get('product').get('description');
            if (
              !OB.UTIL.isNullOrUndefined(
                model.get('product').get('configuredPaymentMethod')
              )
            ) {
              model
                .get('product')
                .get('configuredPaymentMethod')
                .forEach(function(configuredPaymentMethod) {
                  configuredPaymentMethodCount = configuredPaymentMethodCount + 1;
                  if (
                    !OB.UTIL.isNullOrUndefined(configuredPaymentMethod) &&
                    !configuredPaymentMethods.includes(configuredPaymentMethod)
                  ) {
                    configuredPaymentMethods.push(configuredPaymentMethod);
                  }
                });
            }
            if (
              !OB.UTIL.isNullOrUndefined(configuredPaymentMethods) &&
              configuredPaymentMethods.length > 0
            ) {
              configuredPaymentAddedOrNot = true;
              configuredPaymentMethods.forEach(function(configuredPaymentMethod) {
                count = count + 1;
                if (
                  !OB.UTIL.isNullOrUndefined(paymentMethodsAdded) &&
                  paymentMethodsAdded.length > 0 &&
                  paymentMethodsAdded.includes(configuredPaymentMethod)
                ) {
                  requiredPaymentsAdded = true;
                }
              });
            }
            if (!OB.UTIL.isNullOrUndefined(configuredPaymentMethods)) {
              paymentMethodsAdded.forEach(function(payment) {
                if (configuredPaymentMethods.includes(payment)) {
                  ConfiguredPaymentsAdded = true;
                }
              });
              if (
                !OB.UTIL.isNullOrUndefined(paymentTobeadded) &&
                configuredPaymentMethods.includes(paymentTobeadded)
              ) {
                configuredPaymentFlag = true;
              }
              if (
                !requiredPaymentsAdded &&
                uniquePaymentMethods.length == 0 &&
                !configuredPaymentFlag &&
                !OB.UTIL.isNullOrUndefined(
                  model.get('product').get('configuredPaymentMethod')
                )
              ) {
                uniquePaymentMethods = [
                  ...new Set(model.get('product').get('configuredPaymentMethod'))
                ];
              }
              if (
                !requiredPaymentsAdded &&
                productMsg.length == 0 &&
                !configuredPaymentFlag
              ) {
                productMsg.push(
                  'Product' +
                    ' ' +
                    searchkey +
                    ' ' +
                    productname +
                    ' ' +
                    'is allowed to buy only with below payment methods. Please ensure to add this payment method in the invoice'
                );
              }
              if (!ConfiguredPaymentsAdded && totalPaymentAmountFlag) {
                linecount = linecount + 1;
                checkAndApply();
              }
            }
          } else {
            linecount = linecount + 1;
          }
        });
      }
      function checkAndApply() {
        if (
          configuredPaymentAddedOrNot &&
          totalPaymentAmountFlag &&
          !ConfiguredPaymentsAdded &&
          !configuredPaymentFlag
        ) {
          OB.UTIL.showConfirmation.display([productMsg], uniquePaymentMethods);
          args.cancellation = true;
          if (
            !OB.UTIL.isNullOrUndefined(args.paymentToAdd.get('paymentData')) &&
            !OB.UTIL.isNullOrUndefined(
              args.paymentToAdd.get('paymentData').QwikCilverTransaction
            ) &&
            args.paymentToAdd.get('paymentData').QwikCilverTransaction === 'DONE'
          ) {
            var process = new OB.DS.Process(
              'com.openbravo.sharaf.integration.retail.qwikcilver.process.WSCancelRedeem'
            );
            process.exec(
              {
                cardNumber: args.paymentToAdd.get('paymentData').CardNumber,
                cardPin: args.paymentToAdd.get('paymentData').CardPin,
                amount: args.paymentToAdd.get('paymentData').RedeemAmount,
                cardCurrencySymbol: args.paymentToAdd.get('paymentData')
                  .CardCurrencySymbol,
                currencyConversionRate: args.paymentToAdd.get('paymentData')
                  .CurrencyConversionRate,
                invoiceNumber: args.paymentToAdd.get('paymentData').InvoiceNumber,
                referenceId: args.paymentToAdd.get('paymentData').ReferenceId,
                batchNumber: args.paymentToAdd.get('paymentData').BatchId,
                approvalCode: args.paymentToAdd.get('paymentData').ApprovalCode
              },
              function(response) {}
            );
          }
          if ('GCNV_payment.creditnote' === args.paymentToAdd.get('kind')) {
            OB.UI.GiftCardUtils.service(
              'org.openbravo.retail.giftcards.RevertGiftCardAmount',
              {
                _executeInOneServer: true,
                _tryCentralFromStore: true,
                transaction: args.paymentToAdd.get('transaction')
              },
              function(result) {},
              function(error) {}
            );
          }
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        } else if (
          configuredPaymentAddedOrNot &&
          totalPaymentAmountFlag &&
          (ConfiguredPaymentsAdded || configuredPaymentFlag)
        ) {
          args.cancellation = false;
        } else if (configuredPaymentAddedOrNot && !totalPaymentAmountFlag) {
          if (
            !OB.UTIL.isNullOrUndefined(configuredPaymentMethods) &&
            configuredPaymentMethods.length > 0
          ) {
            if (count === configuredPaymentMethodCount && requiredPaymentsAdded) {
              args.cancellation = false;
            } else {
              if (
                !OB.UTIL.isNullOrUndefined(args.paymentToAdd) &&
                !OB.UTIL.isNullOrUndefined(args.paymentToAdd.get('kind')) &&
                configuredPaymentMethods.includes(
                  OB.MobileApp.model.paymentnames[args.paymentToAdd.get('kind')]
                    .paymentMethod.paymentMethod$_identifier
                )
              ) {
                args.cancellation = false;
              } else {
                OB.UTIL.showConfirmation.display(
                  [productMsg],
                  uniquePaymentMethods
                );
                args.cancellation = true;
                if (
                  !OB.UTIL.isNullOrUndefined(
                    args.paymentToAdd.get('paymentData').QwikCilverTransaction
                  ) &&
                  args.paymentToAdd.get('paymentData').QwikCilverTransaction ===
                    'DONE'
                ) {
                  var process = new OB.DS.Process(
                    'com.openbravo.sharaf.integration.retail.qwikcilver.process.WSCancelRedeem'
                  );
                  process.exec(
                    {
                      cardNumber: args.paymentToAdd.get('paymentData').CardNumber,
                      cardPin: args.paymentToAdd.get('paymentData').CardPin,
                      amount: args.paymentToAdd.get('paymentData').RedeemAmount,
                      cardCurrencySymbol: args.paymentToAdd.get('paymentData')
                        .CardCurrencySymbol,
                      currencyConversionRate: args.paymentToAdd.get('paymentData')
                        .CurrencyConversionRate,
                      invoiceNumber: args.paymentToAdd.get('paymentData')
                        .InvoiceNumber,
                      referenceId: args.paymentToAdd.get('paymentData')
                        .ReferenceId,
                      batchNumber: args.paymentToAdd.get('paymentData').BatchId,
                      approvalCode: args.paymentToAdd.get('paymentData')
                        .ApprovalCode
                    },
                    function(response) {}
                  );
                }
                if ('GCNV_payment.creditnote' === args.paymentToAdd.get('kind')) {
                  OB.UI.GiftCardUtils.service(
                    'org.openbravo.retail.giftcards.RevertGiftCardAmount',
                    {
                      _executeInOneServer: true,
                      _tryCentralFromStore: true,
                      transaction: args.paymentToAdd.get('transaction')
                    },
                    function(result) {},
                    function(error) {}
                  );
                }
                OB.UTIL.HookManager.callbackExecutor(args, callbacks);
              }
            }
          }
        }
        if (linecount === args.receipt.get('lines').length)
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      }
    }
    var negativeQty = false;
    args.receipt.get('lines').models.forEach(function(line) {
      if (line.get('qty') < 0) {
        negativeQty = true;
      }
    });
  
    if (negativeQty) {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    } else if (args.receipt.get('isQuotation')) {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    } else {
      if (
        salesDocTypesArr.includes(
          OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey')
        )
      ) {
        var name = args.paymentToAdd.get('kind');
        for (var i = 0; i < OB.MobileApp.model.attributes.payments.length; i++) {
          var payment = OB.MobileApp.model.attributes.payments[i];
          if (name === payment.payment.searchKey) {
            if (
              payment.paymentMethod.paymentMethodCategory$_identifier ===
              'Non-cash'
            ) {
              var id = payment.paymentMethod.paymentMethod;
              OB.info('OBPOS_preAddPayment - trying to get Account Code');
              if (
                !OB.UTIL.isNullOrUndefined(
                  localStorage.getItem('accountCodeForPayment')
                )
              ) {
                var accountCodeObj = JSON.parse(
                  localStorage.getItem('accountCodeForPayment')
                ).AccountCode;
                OB.info(
                  'OBPOS_preAddPayment - Account Code obtained: ' + accountCodeObj
                );
                var bpAccountCodeObj = accountCodeObj[id];
                if (bpAccountCodeObj.length > 2) {
                  bpAccountCodeObj = bpAccountCodeObj.split('=');
                  var bp = bpAccountCodeObj[1].split('}');
                  bp = bp[0];
                  if (OB.UTIL.isNullOrUndefined(bp) || bp.length === 0) {
                    OB.UTIL.showError(OB.I18N.getLabel('CUSTSHA_ErrorGettingBP'));
                  } else {
                    var process = new OB.DS.Process(
                      'org.openbravo.retail.posterminal.CheckBusinessPartnerCredit'
                    );
                    process.exec(
                      {
                        businessPartnerId: bp,
                        totalPending: 0
                      },
                      function(data) {
                        if (data && data.actualCredit) {
                          args.paymentToAdd.set(
                            'availablePaymentLimit',
                            data.actualCredit
                          );
                          if (
                            args.paymentToAdd.get('availablePaymentLimit') > 0 &&
                            args.paymentToAdd.get('amount') <=
                              args.paymentToAdd.get('availablePaymentLimit')
                          ) {
                            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                          } else {
                            OB.UTIL.showConfirmation.display(
                              OB.I18N.getLabel('CUSTSHA_PaymentLimitReached', [
                                data.actualCredit
                              ]),
                              [
                                {
                                  label: OB.I18N.getLabel('OBMOBC_LblOk')
                                }
                              ]
                            );
                            args.cancelOperation = true;
                          }
                        } else {
                          OB.UTIL.showError(
                            OB.I18N.getLabel('CUSTSHA_Unabletocheckcredit')
                          );
                        }
                      },
                      function() {
                        OB.UTIL.showError(
                          OB.I18N.getLabel('CUSTSHA_Unabletocheckcredit')
                        );
                      }
                    );
                  }
                  break;
                } else {
                  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                }
              } else {
                OB.UTIL.HookManager.callbackExecutor(args, callbacks);
              }
            } else {
              OB.UTIL.HookManager.callbackExecutor(args, callbacks);
            }
          } else {
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
          }
        }
      } else {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      }
    }
  });
  
