/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, _ */


(function() {
	OB.UTIL.HookManager.registerHook('OBPOS_TerminalLoadedFromBackend', function(args, callbacks) {
		var processervice = new OB.DS.Process('com.openbravo.sharaf.retail.customdevelopments.service.GetcountOftxn');
		processervice.exec({
			posterminal: OB.MobileApp.model.get('terminal').id

		}, function(data) {
			if (data && !data.exception) {
				if (data.length !== 0) {
					if (!OB.UTIL.isNullOrUndefined(data[0][0])) {
						var countfrombackoffice = data[0][0];
						if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('transactionCount'))) {
							OB.UTIL.HookManager.callbackExecutor(args, callbacks);
						} else {
							localStorage.setItem('transactionCount', countfrombackoffice);
							OB.UTIL.HookManager.callbackExecutor(args, callbacks);
						}
					}
				}
			} else {
				OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), data.exception.message);
				OB.UTIL.HookManager.callbackExecutor(args, callbacks);

			}

		});

	});
}());