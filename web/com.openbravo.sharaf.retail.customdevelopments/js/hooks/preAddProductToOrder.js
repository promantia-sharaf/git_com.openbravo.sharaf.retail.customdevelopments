/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, _ */

OB.UTIL.HookManager.registerHook('OBPOS_PreAddProductToOrder', function(args, callbacks) {
    var cUSTSHAPaymentType = args.receipt.get('payments');
    var i = 0,
        len = cUSTSHAPaymentType.length;

    if (args.cancelOperation) {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        return;
    }
    for (i; i < len; i++) {
        var paymentMethod = cUSTSHAPaymentType.models[i].get('kind');
        if (paymentMethod === 'CUSTSPM_payment.card_MasterCard' || paymentMethod === 'CUSTSPM_payment.giftcard' || paymentMethod === 'CUSTSPM_payment.card_VISA' || paymentMethod === 'CUSTSPM_payment.creditcard' || paymentMethod === 'CUSTSPM_payment.card_AMEX' || paymentMethod === 'CUSTSPM_payment.card_UnionPay' || paymentMethod === 'CUSTSPM_payment.yougotagift' || paymentMethod === 'GCNV_payment.creditnote') {
            args.cancelOperation = true;
            OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTSHA_Validation'));
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
            return;
        }
    }

    // Validate if make a line charge
    if (args.productToAdd.get('productType') === 'S' && !args.productToAdd.get('isLinkedToProduct') && args.receipt.get('orderType') === OB.DEC.Zero && (!args.options || (args.options && !args.options.isVerifiedReturn))) {
        var orderLines = args.receipt.get('lines').models,
            negativeLines = _.filter(orderLines, function(line) {
                var validLine = line.get('qty') < 0 && line.get('product').get('productType') === 'I';
                if (validLine && line.get('custshaChargeLines')) {
                    _.each(line.get('custshaChargeLines'), function(lineId) {
                        var serviceLine = _.find(orderLines, function(l) {
                            return l.get('id') === lineId;
                        });
                        if (serviceLine && serviceLine.get('product').id === args.productToAdd.id) {
                            validLine = false;
                        }
                    });
                }
                return validLine;
            });
        if (negativeLines.length > 0) {
            OB.MobileApp.view.waterfall('onShowPopup', {
                popup: 'CUSTSHA_UI_ReturnLinesPopup',
                args: {
                    negativeLines: negativeLines,
                    callback: function(selectedLines) {
                        args.attrs = args.attrs || {};
                        if (selectedLines.length > 0) {
                            args.attrs.custshaChargeLines = selectedLines;
                        }
                        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                    }
                }
            });
        } else {
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
    } else {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
});