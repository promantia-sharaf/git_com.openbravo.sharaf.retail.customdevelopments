/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, moment */

(function () {

  OB.UTIL.HookManager.registerHook('POSS_PreSetBusinessDate', function (args, callbacks) {
    var currentDate = new Date(new Date().setHours(0, 0, 0, 0));
    if (moment(args.businessdate).format('DDMMYY') !== moment(currentDate).format('DDMMYY')) {
      OB.UTIL.showError(OB.I18N.getLabel('CUSTSHA_BusinessDateCurrentDate'));
      args.cancellation = true;
    }
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });

}());