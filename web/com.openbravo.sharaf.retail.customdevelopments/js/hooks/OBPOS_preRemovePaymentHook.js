(function() {
    OB.UTIL.HookManager.registerHook('OBPOS_preRemovePayment', function(args, callbacks) {
        var nonMandatoryPaymentAvailable = false;
        if (!OB.UTIL.isNullOrUndefined(args.payments)) {
            args.payments.models.forEach(function(model) {
                if (!model.get('isMandatoryPaymentMethod')) {
                    nonMandatoryPaymentAvailable = true;
                }
            });
        }
        if (!OB.UTIL.isNullOrUndefined(args.paymentToRem.get('isMandatoryPaymentMethod')) && args.paymentToRem.get('isMandatoryPaymentMethod') && nonMandatoryPaymentAvailable) {
            OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'),'Payment cannot be deleted because non mandatory payment methods are added  in the receipt');
            args.cancellation = true;
        }
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    });
}());