(function() {
	OB.UTIL.HookManager.registerHook('OBMOBC_PreWindowNavigate', function(args, callbacks) {
        
        if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('depositValidated')) && (localStorage.getItem('depositValidated') === 'true' || localStorage.getItem('depositValidated') === true) && args.window.permission == 'OBPOS_retail.cashup') {
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        } else if (args.window.permission == 'OBPOS_retail.cashup') {
			var finaccountid = null;
			var withid = null;
			var glfloatid = null;
			var depositAmount = null;
			var withdrawAmount = null;
			var gLItemForDeposits = null;
			
			if (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.attributes.payments)) {
				_.each(OB.MobileApp.model.attributes.payments, function(payment) {
					if (payment.paymentMethod.searchKey === 'CUSTSPM_payment.cashAED') {
						finaccountid = payment.payment.financialAccount;
						withid = payment.paymentMethod.gLItemForDrops;
						glfloatid = payment.paymentMethod.possGlitemTilldiff;
                        gLItemForDeposits = payment.paymentMethod.gLItemForDeposits;
					}
				});
			}
			var initialFloatDeposit = 0,
                initialDeposit = 0, 
                lastwithdrawAmt = 0,
                totalwithdrawAmt = 0;
                
            OB.CUSTSHA.getCashupFloatDepositAmount(OB.MobileApp.model.attributes.terminal.cashUpId,finaccountid,glfloatid,initialFloatDeposit,function(res){
                initialFloatDeposit = res;
                if(initialFloatDeposit >0) {
                    OB.CUSTSHA.getCashupWithdrawAmount(OB.MobileApp.model.attributes.terminal.cashUpId,finaccountid,withid,totalwithdrawAmt,function(res){
                        totalwithdrawAmt = res;
                        if (initialFloatDeposit > totalwithdrawAmt) {
                                OB.UTIL.showConfirmation.display(
                                OB.I18N.getLabel('OBMOBC_LblError'),
                                OB.I18N.getLabel('CUSTSHA_CashUpMessege'),
                                [
                                    {
                                        label: OB.I18N.getLabel('OBMOBC_LblOk'),
                                        isConfirmButton: true,
                                        action: function() {
                                            OB.POS.navigate('retail.cashmanagement');
                                        }
                                    }
                                ],
                                {
                                    autoDismiss: false,
                                    onHideFunction: function() {
                                        OB.POS.navigate('retail.cashmanagement');
                                    }
                                }
                            );
                        } else {
                          OB.CUSTSHA.getCashupFloatDepositAmount(OB.MobileApp.model.attributes.terminal.cashUpId,finaccountid,gLItemForDeposits,initialDeposit,function(res) {
                            initialDeposit = res;
                         if(OB.UTIL.isNullOrUndefined(localStorage.getItem('depositValidated')) || localStorage.getItem('depositValidated') === 'false' || localStorage.getItem('depositValidated') === false) {
                             if(initialDeposit > 0 && (initialFloatDeposit+initialDeposit)-totalwithdrawAmt > 0)  {
                               OB.UTIL.showConfirmation.display(
                                OB.I18N.getLabel('OBMOBC_LblError'),
                                OB.I18N.getLabel('CUSTSHA_CashUpMessageWithdrawDeposit'),
                                [
                                    {
                                        label: OB.I18N.getLabel('OBMOBC_LblOk'),
                                        isConfirmButton: true,
                                        action: function() {
                                            localStorage.setItem('depositValidated',true);
                                            OB.POS.navigate('retail.cashmanagement');
                                        }
                                    }
                                ],
                                {
                                    autoDismiss: false,
                                    onHideFunction: function() {
                                        localStorage.setItem('depositValidated',false);
                                        OB.POS.navigate('retail.cashmanagement');
                                    }
                                }
                            );  
                        } else {
                            localStorage.setItem('depositValidated',false);
                            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                        }
                       } else {
                          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                       }   
                        });
                        }
                    });
                } else {
                    OB.CUSTSHA.getCashupFloatDepositAmount(OB.MobileApp.model.attributes.terminal.cashUpId,finaccountid,gLItemForDeposits,initialDeposit,function(res) {
                        initialDeposit = res;
                        if(initialDeposit   > 0) {
                            OB.CUSTSHA.getCashupWithdrawAmount(OB.MobileApp.model.attributes.terminal.cashUpId,finaccountid,withid,totalwithdrawAmt,function(res){
                                totalwithdrawAmt = res;
                        if(OB.UTIL.isNullOrUndefined(localStorage.getItem('depositValidated')) || localStorage.getItem('depositValidated') === 'false' || localStorage.getItem('depositValidated') === false) {
                           if(initialDeposit > 0 && (initialFloatDeposit+initialDeposit)-totalwithdrawAmt > 0){
                                OB.UTIL.showConfirmation.display(
                                OB.I18N.getLabel('OBMOBC_LblError'),
                                OB.I18N.getLabel('CUSTSHA_CashUpMessageWithdrawDeposit'),
                                [
                                    {
                                        label: OB.I18N.getLabel('OBMOBC_LblOk'),
                                        isConfirmButton: true,
                                        action: function() {
                                            localStorage.setItem('depositValidated',true);
                                            OB.POS.navigate('retail.cashmanagement');
                                        }
                                    }
                                ],
                                {
                                    autoDismiss: false,
                                    onHideFunction: function() {
                                        localStorage.setItem('depositValidated',false);
                                        OB.POS.navigate('retail.cashmanagement');
                                    }
                                }
                            ); 
                            } else {
                                localStorage.setItem('depositValidated',false);
                                OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                            }  
                        } else {
                          OB.UTIL.HookManager.callbackExecutor(args, callbacks);  
                        }            
                        });  
                        } else {
                            OB.UTIL.HookManager.callbackExecutor(args, callbacks); 
                        }
                    }) ;
                }
                
            }) ;
		} else {
			OB.UTIL.HookManager.callbackExecutor(args, callbacks);
		}
	});

}());