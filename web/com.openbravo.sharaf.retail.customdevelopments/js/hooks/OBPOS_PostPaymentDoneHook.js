/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, _ */


(function() {
	OB.UTIL.HookManager.registerHook('OBPOS_PostPaymentDone', function(args, callbacks) {
		var count = 0;
		var transcount = 0;
		var orderCount = 0;
		var salesDocTypesArr = [];
		for (var i = 0; i < OB.MobileApp.model.attributes.sharafDocumentType.length; i++) {
			var docs = OB.MobileApp.model.attributes.sharafDocumentType[i];
			if ((docs.ticketSequence || docs.returnSequence) && (!docs.quotationSequence)) {
				salesDocTypesArr.push(docs.id);
			}
		}
		OB.Dal.find(
			OB.Model.Order,
			null,
			function(data) {
				if (data.length != 0) {
					if (!data.exception) {
						data.models.forEach(function(orders) {
							var jsondata = JSON.parse(orders.attributes.json);
							if (salesDocTypesArr.includes(jsondata.custsdtDocumenttype)) {
								count++;
							}
						});
						if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('transactionCount'))) {
							var txcount = localStorage.getItem('transactionCount');
							var txncount = parseInt(txcount);
							localStorage.setItem('transactionCount', txncount + 1);

						} else {
							localStorage.setItem('transactionCount', 1);
						}
						if (count > 0) {
							localStorage.setItem('OrderCount', count - 1);
						}
						else {
							localStorage.setItem('OrderCount', 0);
						}
						var query = "insert into CUSTSHA_transactioncount(id, posterminal, count) values"
							+ "('" + OB.UTIL.get_UUID() + "',"
							+ "'" + OB.MobileApp.model.get('terminal').id + "',"
							+ "'" + localStorage.getItem('transactionCount') + "'"
							+ " );"
						OB.Dal.queryUsingCache(OB.Model.CUSTSHA_transactioncount, query, [], function(success) {

						});
						
						if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('transactionCount'))) {
							transcount = localStorage.getItem('transactionCount');
						}
						if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('OrderCount'))) {
							orderCount = localStorage.getItem('OrderCount');
						}
						var actualcount = OB.DEC.add(transcount, orderCount);
						if (actualcount >= Number(OB.MobileApp.model.attributes.permissions.CUSTSHA_RecordLimit)) {
							OB.MobileApp.view.$.containerWindow.showPopup(
								'CUSTSHA_UI_ModalForceCashUpPopUp',
								{
									args: args,
									callbacks: callbacks,
								}
							);
							OB.UTIL.HookManager.callbackExecutor(args, callbacks);
						} else {
							OB.UTIL.HookManager.callbackExecutor(args, callbacks);
						}

					} else {
						OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), data.exception.message);
					}

				} else {
					OB.UTIL.HookManager.callbackExecutor(args, callbacks);
				}
			},
		);

	});
}());