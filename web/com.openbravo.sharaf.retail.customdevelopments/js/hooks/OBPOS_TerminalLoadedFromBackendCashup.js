/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, _ */


(function() {
    OB.UTIL.HookManager.registerHook('OBPOS_TerminalLoadedFromBackend', function(args, callbacks) {
        // OBDEV-274
        localStorage.setItem('depositValidated',false);
        // OBDEV-274
        var processervice = new OB.DS.Process('com.openbravo.sharaf.retail.customdevelopments.process.FetchCashupEnabledPaymentMethods');
        processervice.exec({
            terminalType: OB.MobileApp.model.get('terminal').terminalType.id
        }, function(data) {
        if (data && data.exception) {
            if (!OB.UTIL.isNullOrUndefined(data.exception.message)) {
                console.log(data.exception.message);
            }
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        } else {
            localStorage.setItem('enabledCashupPaymentData', JSON.stringify(data));
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
    }, function(error) {
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      }
    );
    });
}());