/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, moment, _ */
 
  OB.UTIL.HookManager.registerHook('OBPOS_PrePaymentHook', function (args, callbacks) {
    var receipt = args.context.get('order');  
    if(!receipt.get('isVerifiedReturn') && OB.MobileApp.model.receipt.get('bp').get('custshaCaptureoriginvoice') === true)  {
          var lineQueue = [],
              order = args.context.get('order');
            _.each(order.get('lines').models, function (line) {
              if (!line.get('captureoriginalinvoice')) {
                  line.set('captureoriginalinvoice',false);
                    lineQueue.push({
                      line: line,
                      captureoriginalinvoice: false
                    });
                } else {
                  line.set('captureoriginalinvoice', true);
                }
              });
    if (lineQueue.length > 0) {  
         OB.CUSTSHA.checkOriginalInvoiceLines(order, lineQueue, function () {
          OB.UTIL.HookManager.callbackExecutor(args, callbacks);
       });   
    } else {
       OB.UTIL.HookManager.callbackExecutor(args, callbacks); 
    }      
   } else {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  });

 