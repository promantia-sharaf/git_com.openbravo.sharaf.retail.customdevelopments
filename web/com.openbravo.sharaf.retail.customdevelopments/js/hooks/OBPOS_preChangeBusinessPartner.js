/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, _ */
(function() {

	OB.UTIL.HookManager.registerHook('OBPOS_preChangeBusinessPartner', function(args, callbacks) {
		OB.MobileApp.model.receipt.set('custshaLatitude', null);
		OB.MobileApp.model.receipt.set('custshaLongitude', null);
		OB.UTIL.HookManager.callbackExecutor(args, callbacks);
	});

}());