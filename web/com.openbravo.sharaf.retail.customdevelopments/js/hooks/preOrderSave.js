/*
 ************************************************************************************
 * Copyright (C) 2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_PreOrderSave', function (args, callbacks) {
    if (OB.MobileApp.model.get('POSS_ForceCashupWithTickets')) {
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTSHA_CannotCompleteTicket'));
      args.cancellation = true;
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    } else {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  });

}());