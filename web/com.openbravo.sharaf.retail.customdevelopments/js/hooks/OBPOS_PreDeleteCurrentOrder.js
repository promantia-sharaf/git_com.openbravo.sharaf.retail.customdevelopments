/*global OB, enyo, _ */
(function() {
	OB.UTIL.HookManager.registerHook('OBPOS_PreDeleteCurrentOrder', function(args, callbacks) {
		var count = 0;
		if (OB.MobileApp.model.attributes.permissions.OBPOS_remove_ticket == true) {
			count++;
			if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('transactionCount'))) {
				var txcount = localStorage.getItem('transactionCount');
				var txncount = parseInt(txcount);
				localStorage.setItem('transactionCount', count + txncount);

			} else {
				localStorage.setItem('transactionCount', count);
			}
		} 
		OB.UTIL.HookManager.callbackExecutor(args, callbacks);

	});
}());