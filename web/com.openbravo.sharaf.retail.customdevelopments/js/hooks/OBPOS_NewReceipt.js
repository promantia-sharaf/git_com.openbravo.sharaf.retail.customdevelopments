/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, _ */
(function() {

	OB.UTIL.HookManager.registerHook('OBPOS_NewReceipt', function(args, callbacks) {
		var txncount = 0;
		var orderCount = 0;
		
		if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('transactionCount'))) {
			 txncount = localStorage.getItem('transactionCount');
		}
		if (!OB.UTIL.isNullOrUndefined(localStorage.getItem('OrderCount'))) {
			 orderCount = localStorage.getItem('OrderCount');
		}
		var actualcount = OB.DEC.add(txncount, orderCount);
		if (actualcount >= Number(OB.MobileApp.model.attributes.permissions.CUSTSHA_RecordLimit)) {
			OB.MobileApp.view.$.containerWindow.showPopup(
				'CUSTSHA_UI_ModalForceCashUpPopUp',
				{
					args: args,
					callbacks: callbacks,
				}
			);
			OB.UTIL.HookManager.callbackExecutor(args, callbacks);
		} else {
			OB.UTIL.HookManager.callbackExecutor(args, callbacks);
		}

	});

}());