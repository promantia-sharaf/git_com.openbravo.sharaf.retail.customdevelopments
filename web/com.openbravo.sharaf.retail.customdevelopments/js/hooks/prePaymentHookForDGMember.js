(function() {
    OB.UTIL.CUSTSHAUtils = OB.UTIL.CUSTSHAUtils || {}
    OB.UTIL.HookManager.registerHook('OBPOS_PrePaymentHook', function(args, callbacks) {
        var negativeQty = false;
        args.context.get('order').get('lines').models.forEach(function(line) {
        	//check receipt has negative qty
            if (line.get('qty') < 0) {
                negativeQty = true;
            }
        });

        if (negativeQty) { //For return flow don't validate DG Member fair utilization
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        } else if (args.context.get('order').get('isQuotation')) { //For quotation don't validate DG Member fair utilization
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        } else {
            // if only system lvl preference present, then don't proceed to check DGMember
            if (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.get('permissions').CUSTSHA_DGMember_InvoiceCount) && !OB.UTIL.isNullOrUndefined(OB.MobileApp.model.get('permissions').CUSTSHA_DGMember_ItemQuantityCount) && OB.MobileApp.model.get('permissions').CUSTSHA_DGMember_InvoiceCount !== '0' && OB.MobileApp.model.get('permissions').CUSTSHA_DGMember_ItemQuantityCount !== '0') {

                OB.MobileApp.model.attributes.permissions['OBPOS_remote.customer'] = false;
                args.context.get('order').set('isInvoiceCountError', false);
                args.context.get('order').set('isItemQtyCountError', false);

                OB.UTIL.CUSTSHAUtils.checkDGMember(args, function() {
                    if (args.context.get('order').get('isInvoiceCountError')) {
                        OB.MobileApp.model.attributes.permissions['OBPOS_remote.customer'] = true;
                        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTSHA_DGMemberPolicy'));
                    } else if (args.context.get('order').get('isItemQtyCountError')) {
                        OB.MobileApp.model.attributes.permissions['OBPOS_remote.customer'] = true;
                        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTSHA_DGMemberPolicy'));
                    } else {
                        OB.MobileApp.model.attributes.permissions['OBPOS_remote.customer'] = true;
                        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                    }
                });
            } else {
                OB.UTIL.HookManager.callbackExecutor(args, callbacks);
            }
        }
    });
}());

OB.UTIL.CUSTSHAUtils.checkDGMember = function(args, callback) {
    var totalDGItemCount = 0,
        loop = 0,
        count = 0,
        countLoop = 0;
    for (var i = 0; i < args.context.get('order').get('lines').models.length; i++) {
        var line = args.context.get('order').get('lines').models[i],
            loop = i;
        //for price adj promo type 
        if (!OB.UTIL.isNullOrUndefined(line.get('custdisOffer')) && OB.UTIL.isNullOrUndefined(line.get('custdisOrderline'))) {
            var query = "select distinct OBPG.* from m_offer_bp_group OBPG " //
            + " where OBPG.m_offer_id = '" + line.get('custdisOffer') + "'"; //
            OB.Dal.query(OB.Model.DiscountFilterBusinessPartnerGroup, query, [], function(offers) { // to check if its a DG Member promo
                if (offers.length > 0) {
                    //success local
                    function success(model) {
                        //count++;
                    	var countQty = 0;
                    	for (var i = 0; i < args.context.get('order').get('lines').models.length; i++) {
                    		var orderLine = args.context.get('order').get('lines').models[i];
                    		if (!OB.UTIL.isNullOrUndefined(orderLine.get('custdisOffer')) && OB.UTIL.isNullOrUndefined(orderLine.get('custdisOrderline'))) {
                    			countQty = countQty + Number(orderLine.get('qty'));
                    		}
                    	}
                        //countQty = countQty + Number(args.context.get('order').get('lines').models[loop].get('qty'));
                       var totalDGItemCount = countQty + Number(model.get('cUSTSHADGItemQtyCount'));
						var cUSTSHADGItemQtyCount = Number(model.get('cUSTSHADGItemQtyCount'));
						var permission = Number(OB.MobileApp.model.get('permissions').CUSTSHA_DGMember_ItemQuantityCount);
						var cUSTSHADGInvoiceCount=Number(model.get('cUSTSHADGInvoiceCount'));
						var invoicepermission=Number(OB.MobileApp.model.get('permissions').CUSTSHA_DGMember_InvoiceCount);
						if (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.get('permissions').CUSTSHA_DGMember_InvoiceCount)) {
							if (cUSTSHADGInvoiceCount >= invoicepermission) {
								args.context.get('order').set('isInvoiceCountError', true);
								callback();
							}
						}
						if (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.get('permissions').CUSTSHA_DGMember_ItemQuantityCount)) {
							if ((cUSTSHADGItemQtyCount >= permission) || (totalDGItemCount > permission )) {
								count++;
								args.context.get('order').set('isItemQtyCountError', true);
								callback();
							} else {
								count++;
							}
						}

                        if (count === args.context.get('order').get('lines').models.length) {
                            callback();
                        }
                    }
                    //error local
                    function error(tx) {
                        window.console.error('error while fetching data from local storage- BP tale' + tx);
                        callback();
                    }
                    OB.Dal.get(OB.Model.BusinessPartner, args.context.get('order').get('bp').id, success, error);
                } else {
                    count++;
                    if (count === args.context.get('order').get('lines').models.length) {
                        callback();
                    }
                }
            });
        } else {
            count++
            if (count === args.context.get('order').get('lines').models.length) {
                callback();
            }
        }
    }
};