/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, moment, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_PrePaymentHook', function (args, callbacks) {
   var receipt = args.context.get('order');
   var cityName = OB.MobileApp.model.receipt.attributes.bp.attributes.cityName;
   var countryId = OB.MobileApp.model.receipt.attributes.bp.attributes.locationModel.attributes.countryId;
   
   if (!receipt.get('isVerifiedReturn') && !receipt.get('isQuotation') ) {
       if(OB.UTIL.isNullOrUndefined(cityName) || cityName == '' ) {
        args.cancellation = true;
        OB.UTIL.showConfirmation.display('City Name is Mandatory in Customer Address, Please Update Customer Address');
       } else {
                var query = "select p.id from custsha_city p where p.name = '" + cityName + "'"
                      +" and p.country = '" + countryId + "'" ;
                OB.Dal.queryUsingCache(OB.Model.City, query, [], function(res) {
                   if(res.models.length > 0){
                       OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                    } else {
                      args.cancellation = true;
                      OB.UTIL.showConfirmation.display('City Name is Invalid in Customer Address, Please Update Customer Address');  
                    }                                             
                 });
      }
    } else {
       OB.UTIL.HookManager.callbackExecutor(args, callbacks);  
    }
   
  });

}());
