/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */



OB.UTIL.HookManager.registerHook('OBPOS_BeforeCustomerSave', function (args, c) {
  args.customer.set('searchKey', OB.UTIL.get_UUID());
  OB.UTIL.HookManager.callbackExecutor(args, c);
});