/*
Promantia Development
*/

OB.UTIL.HookManager.registerHook('OBPOS_TerminalLoadedFromBackend', function (args, callbacks) {

  new OB.DS.Process('com.openbravo.sharaf.retail.customdevelopments.service.AccountCodeForPaymentMethods').exec({}, function (data) {
    if (data && data.exception) {
      if (!OB.UTIL.isNullOrUndefined(data.exception.message)) {
        console.log(data.exception.message);
      }
    } else {
      localStorage.setItem('accountCodeForPayment', JSON.stringify(data));
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  });

  new OB.DS.Process('com.openbravo.sharaf.retail.deliveryconditions.service.OrgWarehouseLocation').exec({}, function (data) {
    if (data && data.exception) {
      if (!OB.UTIL.isNullOrUndefined(data.exception.message)) {
        console.log(data.exception.message);
      }
    } else {
      localStorage.setItem('orgWarehouseLocation', JSON.stringify(data));
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  });
});
