/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_preApplyDiscountsHook', function (args, callbacks) {
    if (args.cancelOperation || (args.context.$.discountsContainer.requiresQty && !args.context.$.discountsContainer.amt)) {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      return;
    }
    OB.MobileApp.view.waterfall('onShowPopup', {
      popup: 'CUSTSHA_UI_DiscountReasonPopup',
      args: {
        selectedModels: args.context.checkedLines
      }
    });
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });

}());