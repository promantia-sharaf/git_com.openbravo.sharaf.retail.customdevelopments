OB.UTIL.HookManager.registerHook('OBRETUR_ReturnReceiptApply', function (args, callbacks) {
        
    var receiptlineslen = OB.MobileApp.model.receipt.attributes.lines.length;
    if(OB.MobileApp.model.attributes.permissions.CUSTSHA_Restrict_Multiple_Order_To_Add_In_Verified_Return === true) {
    for (i = 0; i < receiptlineslen ; i++) {
        if (args.order.documentNo != OB.MobileApp.model.receipt.attributes.lines.models[i].attributes.originalDocumentNo) {
          args.cancellation = true;   
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), OB.I18N.getLabel('CUSTSHA_Restrict_Multiple_Order'));
        }
      }
    }
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
});