/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, Backbone, _ */

(function () { 
  OB.UTIL.HookManager.registerHook('OBMOBC_PreWindowNavigate', function(args, callbacks) {
	  if(args.window.permission == 'OBPOS_retail.pointofsale'){
	      OB.SYSTEMDATE.VALIDATIONCHECK(args,callbacks);
	  }else{
	      OB.UTIL.HookManager.callbackExecutor(args, callbacks);  

	  }
  });
}());

