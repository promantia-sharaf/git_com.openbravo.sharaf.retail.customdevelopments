/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo */
OB.UTIL.HookManager.registerHook('OBPOS_BarcodeScan', function(args, callbacks) {
    if (!OB.UTIL.isNullOrUndefined(args) && !OB.UTIL.isNullOrUndefined(args.code)) {
        localStorage.setItem('BPDetailsFromBarcodeScan', JSON.stringify(args.code));
    }
    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
});