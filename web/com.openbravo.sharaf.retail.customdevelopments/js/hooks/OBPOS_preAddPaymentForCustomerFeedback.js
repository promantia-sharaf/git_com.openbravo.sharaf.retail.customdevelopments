/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, enyo, Backbone, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_preAddPayment', function (args, callbacks) {
	  var customerFeedbackEnabled = (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.attributes.permissions.CUSTSHA_CustomerFeedBackAllow)) 
      ? OB.MobileApp.model.attributes.permissions.CUSTSHA_CustomerFeedBackAllow === true 
    	      : false;
	if(customerFeedbackEnabled){
		if (!OB.UTIL.isNullOrUndefined(args.paymentToAdd)) {
	    	// Hardcoded customer feedback reasons
	    	var customerFeedbackReasons = [
	    		" ",
	    	    "Direct",
	    	    "Email",
	    	    "Radio Ad",
	    	    "WhatsApp",
	    	    "Website",
	    	    "Social Media (FB, Insta, TikTok, Snap)",
	    	    "Others"
	    	];

	    	// Setting the hardcoded values to a new key
	    	args.receipt.set('customerFeedbackReasons', customerFeedbackReasons);
	      OB.MobileApp.view.$.containerWindow.showPopup(
	            'CUSTSHA_UI_CustomerFeedBackInfo',
		      {
		      	args: args,
		        callbacks: callbacks
		      }
	       );
	    }
	}
	OB.UTIL.HookManager.callbackExecutor(args, callbacks);
  });
}());