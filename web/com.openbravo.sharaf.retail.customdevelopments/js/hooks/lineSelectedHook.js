/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, $ */

OB.UTIL.HookManager.registerHook('OBPOS_LineSelected', function (args, callbacks) {
  $("#terminal_containerWindow_pointOfSale_multiColumn_rightPanel_toolbarpane_edit_editTabContent_control").hide();
  $("#terminal_containerWindow_pointOfSale_multiColumn_rightPanel_toolbarpane_edit_editTabContent_actionButtonsContainer").css("max-height", "none");
  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
});