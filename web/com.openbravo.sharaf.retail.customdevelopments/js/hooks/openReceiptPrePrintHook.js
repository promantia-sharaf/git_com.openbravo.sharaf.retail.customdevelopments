
OB.Model.OrderFilter.prototype.source='com.openbravo.sharaf.retail.customdevelopments.CrossStoreOpenReceipts'

OB.UTIL.HookManager.registerHook('OBPRINT_PrePrint', function (args, callbacks) {
    if (OB.MobileApp.model.get('terminal').organization !== args.order.get('organization')) {
    	OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), OB.I18N.getLabel('CUSTSHA_RestrictPrintCrossStoreReceipt'));
    	args.cancelOperation = true;
    } else {
    	OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  });