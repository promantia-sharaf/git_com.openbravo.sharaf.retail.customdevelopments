/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, _*/

OB.UTIL.HookManager.registerHook('OBPOS_PostAddProductToOrder', function(args, callbacks) {
   
   // OBDEV-205 changes
   if (!OB.UTIL.isNullOrUndefined(args.attrs) && !OB.UTIL.isNullOrUndefined(args.attrs.shaquoOriginalDocumentNo)) {
     args.receipt.set('custshaLatitude', null);
     args.receipt.set('custshaLongitude', null);	
   }
   
   
    // OBDEV-145 changes
    var salesDocTypesArr = [];
    for (var i = 0; i < OB.MobileApp.model.attributes.sharafDocumentType.length; i++) {
        var docs = OB.MobileApp.model.attributes.sharafDocumentType[i];
        if ((docs.ticketSequence || docs.quotationSequence) && (!docs.returnSequence)) {
            salesDocTypesArr.push(docs.searchKey);
        }
    }
    if (salesDocTypesArr.includes(OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey'))) {
        if (!OB.UTIL.isNullOrUndefined(args.productToAdd.get('id'))) {
            var len = args.receipt.get('lines').models.length - 1;
            args.receipt.get('lines').models[len].get('product').attributes.configuredPaymentMethod = [];
            var paymentMethod = new Array();
            OB.MobileApp.model.receipt.attributes.lines.configuredPaymentMethod = paymentMethod;
            var productid = args.productToAdd.get('id');
            var searchkey = args.productToAdd.get('searchkey');
            var productname = args.productToAdd.get('description');
            var paymentNames = new Array();
            var productMsg = [];
            var count = 0;
            if (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.attributes.payments)) {
                var paymentslength = OB.MobileApp.model.attributes.payments.length;
            }
            var query = "select * from CUSTSHA_PaymentMethod cpm  where cpm.productId='" + productid + "'";
            OB.Dal.queryUsingCache(OB.Model.RestrictPaymentMethodConfig, query, [], function(data) {
                if (data.models.length !== 0) {
                    _.each(data.models, function(model) {
                        count = count + 1;
                        var paymentscount = 0;
                        var paymentName = null;
                        if (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.attributes.payments)) {
                            _.each(OB.MobileApp.model.attributes.payments, function(payment) {
                                paymentscount = paymentscount + 1;
                                if (payment.paymentMethod.paymentMethod === model.get('paymtId')) {
                                    paymentName = payment.paymentMethod.paymentMethod$_identifier;
                                }
                            });
                        }
                        if (paymentslength === paymentscount) {
                            paymentNames.push(paymentName);
                            if (productMsg.length == 0) {
                                productMsg.push('Product' + ' ' + searchkey + ' ' + productname + ' ' + 'is allowed to buy only with below payment methods. Please ensure to add this payment method in the invoice');
                            }
                            if (data.models.length == 1 && (count == data.models.length)) {
                                OB.UTIL.showConfirmation.display([productMsg], paymentNames);
                                if (!OB.UTIL.isNullOrUndefined(paymentNames)) {
                                    args.receipt.get('lines').models[len].get('product').attributes.configuredPaymentMethod = paymentNames;
                                    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                                }
                            }
                            if (data.models.length > 1 && (count == data.models.length)) {
                                let uniquePaymentMethods = [...new Set(paymentNames)];
                                OB.UTIL.showConfirmation.display([productMsg], uniquePaymentMethods);
                                if (!OB.UTIL.isNullOrUndefined(paymentNames)) {
                                    args.receipt.get('lines').models[len].get('product').attributes.configuredPaymentMethod = paymentNames;
                                    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                                }
                            }
                        }
                    });
                } else {
                    OB.UTIL.HookManager.callbackExecutor(args, callbacks);
                }
            });
        }
    } else {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
});
