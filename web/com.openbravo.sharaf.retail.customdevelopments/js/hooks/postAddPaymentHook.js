/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, _ */

(function() {
    OB.UTIL.HookManager.registerHook('OBPOS_postAddPayment', function(args, callbacks) {
        // OBDEV-145 changes
        var salesDocTypesArr = [];
        for (var i = 0; i < OB.MobileApp.model.attributes.sharafDocumentType.length; i++) {
            var docs = OB.MobileApp.model.attributes.sharafDocumentType[i];
            if (docs.ticketSequence && (!docs.returnSequence)) {
                salesDocTypesArr.push(docs.searchKey);
            }
        }
        if (salesDocTypesArr.includes(OB.MobileApp.model.receipt.get('custsdtDocumenttypeSearchKey'))) {
            var configuredPaymentMethods = [];
            if (!OB.UTIL.isNullOrUndefined(args.receipt.get('lines'))) {
                args.receipt.get('lines').models.forEach(function(model) {
                    if (!OB.UTIL.isNullOrUndefined(model.get('product').get('configuredPaymentMethod')) && (model.get('product').get('configuredPaymentMethod').length > 0)) {
                        model.get('product').get('configuredPaymentMethod').forEach(function(configuredPaymentMethod) {
                            if (!OB.UTIL.isNullOrUndefined(configuredPaymentMethod) && !configuredPaymentMethods.includes(configuredPaymentMethod)) {
                                configuredPaymentMethods.push(configuredPaymentMethod);
                            }
                        });
                    }
                });
            }
            if (!OB.UTIL.isNullOrUndefined(configuredPaymentMethods) && (configuredPaymentMethods.length > 0)
                && !OB.UTIL.isNullOrUndefined(args.paymentAdded.get('kind'))) {
                if (configuredPaymentMethods.includes(OB.MobileApp.model.paymentnames[args.paymentAdded.get('kind')].paymentMethod.paymentMethod$_identifier)) {
                    args.paymentAdded.set("isMandatoryPaymentMethod", true);
                } else {
                    args.paymentAdded.set("isMandatoryPaymentMethod", false);
                }
            }
            OB.UTIL.HookManager.callbackExecutor(args, callbacks);
        }
    });
}());