/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, moment, _ */

(function () {

  OB.UTIL.HookManager.registerHook('OBPOS_PrePaymentHook', function (args, callbacks) {
    var receipt = args.context.get('order'),
        negativeLines, errors = [];
   if(receipt.get('isEditable')) {
    negativeLines = _.filter(receipt.get('lines').models, function (line) {
      return line.get('qty') < 0;
    });

    var finalCheckReturnProperties = _.after(negativeLines.length, function () {
      if (errors.length > 0) {
    	args.cancellation = true;
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTSHA_MissingReturnProperties'), errors.join());
      } else {
        OB.UTIL.HookManager.callbackExecutor(args, callbacks);
      }
    });
    if (negativeLines.length > 0) {
      _.each(negativeLines, function (line) {
        if (OB.UTIL.isNullOrUndefined(line.get('returnReason')) || OB.UTIL.isNullOrUndefined(line.get('cUSTSHACustomerRequest')) || OB.UTIL.isNullOrUndefined(line.get('cUSTSHAProductCondition'))) {
          errors.push(line.get('product').get(OB.Constants.IDENTIFIER));
          finalCheckReturnProperties();
        } else {
          finalCheckReturnProperties();
        }
      });
    }
    
    } else {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  });

}());