OB.SYSTEMDATE = {

	VALIDATIONCHECK : function validationCheck(args, callbacks) {

		var businessDate = OB.I18N.formatDateISO(OB.I18N
				.parseServerDate(OB.UTIL.localStorage.getItem('businessdate')));
		var systemDate = OB.I18N.formatDateISO(new Date(new Date().setHours(0,
				0, 0, 0)));
		var closingtime = 0;
		var systemMinutes = new Date().getMinutes();
		var systemHours = new Date().getHours();
		var systemTime = +(systemHours + "." + systemMinutes);
		var diff = (new Date() - (OB.I18N.parseServerDate(OB.UTIL.localStorage
				.getItem('businessdate'))));
		var differenceDays = Math.ceil((diff)
				/ (1000 * 60 * 60 * 24));

		if (!OB.UTIL
				.isNullOrUndefined(OB.MobileApp.model.get('permissions').CUSTSHA_StoreClosingTime)
				&& !OB.UTIL.isNullOrUndefined(OB.MobileApp.model
						.get('permissions').CUSTSHA_StoreClosingTime)
				&& OB.MobileApp.model.get('permissions').CUSTSHA_StoreClosingTime !== '0') {
			closingtime = Number(OB.MobileApp.model.attributes.permissions.CUSTSHA_StoreClosingTime);
		}
		var processervice = new OB.DS.Process(
				'com.openbravo.sharaf.retail.customdevelopments.service.GetOpenStoreCount');
		processervice.exec({

		}, function(data) {
			if (data && !data.exception) {
				if (data[0][0] > 0) {
					if (!OB.UTIL.isNullOrUndefined(businessDate)
							&& businessDate < systemDate && differenceDays > 1 && closingtime > 0
							&& systemTime > closingtime) {
						args.cancellation = true;
						OB.UTIL.showConfirmation.display(OB.I18N
								.getLabel('OBMOBC_LblError'), OB.I18N
								.getLabel('CUSTSHA_SystemDateCheckMessage'),
								[ {
									label : OB.I18N.getLabel('OBMOBC_LblOk'),
									isConfirmButton : true,
									action : function() {
										OB.MobileApp.model.logout();
									}
								} ], {
									autoDismiss : false,
									onHideFunction : function() {
										OB.MobileApp.model.logout();
									}
								});
					}else {
						OB.UTIL.HookManager.callbackExecutor(args, callbacks);
					}

				} else {
					OB.UTIL.HookManager.callbackExecutor(args, callbacks);
				}

			} else {
				OB.UTIL.showConfirmation.display(OB.I18N
						.getLabel('OBMOBC_LblError'), data.exception.message);
				OB.UTIL.HookManager.callbackExecutor(args, callbacks);

			}

		});

		OB.UTIL.HookManager.callbackExecutor(args, callbacks);

	},
}