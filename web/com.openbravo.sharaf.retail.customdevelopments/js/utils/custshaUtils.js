/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global, _, Promise*/

OB = OB || {};

OB.CUSTSHA = OB.CUSTSHA || {};

OB.CUSTSHA.updateBPForDGHelp = function (order, bpId, callback) {
  if (order.receipt.get('bp')) {
    OB.Dal.get(OB.Model.BusinessPartner, bpId, function (bPartner) {
      bPartner.loadBPLocations(null, null, function (shipping, billing, locations) {
        bPartner.set('name', order.receipt.get('businessPartnerName'));
        bPartner.set('locId', billing.get('id'));
        bPartner.set('locName', billing.get('name'));
        bPartner.set('postalCode', billing.get('postalCode'));
        bPartner.set('cityName', billing.get('cityName'));
        bPartner.set('countryName', billing.get('countryName'));
        if (shipping) {
          bPartner.set('shipLocId', shipping.get('id'));
          bPartner.set('shipLocName', shipping.get('name'));
          bPartner.set('shipPostalCode', shipping.get('postalCode'));
          bPartner.set('shipCityName', shipping.get('cityName'));
          bPartner.set('shipCountryName', shipping.get('countryName'));
          bPartner.set('shipRegionId', shipping.get('regionId'));
          bPartner.set('shipCountryId', shipping.get('countryId'));
        }
        order.receipt.set('bp', bPartner);
        OB.MobileApp.model.receipt.save(function () {
          callback();
        });
      });
    });
  } else {
    callback();
  }
};

OB.CUSTSHA.getDGPStatus = function (statusSearchKey) {
  var dgpStatus = '',
      i = 0;
  for (i; i < OB.MobileApp.model.get('sharafDGPStatus').length; i++) {
    if (OB.MobileApp.model.get('sharafDGPStatus')[i].searchKey === statusSearchKey) {
      dgpStatus = OB.MobileApp.model.get('sharafDGPStatus')[i].name;
      break;
    }
  }
  return dgpStatus;
};

OB.CUSTSHA.findCategory = function (data) {
  return new Promise(function (resolve, reject) {
    OB.Dal.find(OB.Model.ProductCategory, {
      id: data.categoryId
    }, function (categories) {
      if (categories.length === 1) {
        data.category = categories.at(0);
        resolve(data);
      } else {
        reject();
      }
    });
  });
};

OB.CUSTSHA.findRootId = function (data) {
  return Promise.resolve(data) //
  .then(OB.CUSTSHA.findCategory).then(function (data) {
    if (data.parentId === '0' || data.category.get('custshaCategoryType') === 'Department') {
      return data;
    } else {
      if (data.category.get('custshaCategoryType') === 'Group') {
        data.Group = data.category.get('searchKey');
      } else if (data.category.get('custshaCategoryType') === 'Subgroup') {
        data.Subgroup = data.category.get('searchKey');
      }
      data.categoryId = data.parentId;
      return Promise.resolve(data) //
      .then(OB.CUSTSHA.findParentId) //
      .then(OB.CUSTSHA.findRootId);
    }
  });
};

OB.CUSTSHA.findParentId = function (data) {
  return new Promise(function (resolve, reject) {
    OB.Dal.find(OB.Model.ProductCategoryTree, {
      categoryId: data.categoryId
    }, function (categories) {
      if (categories.length === 1) {
        data.parentId = categories.at(0).get('parentId');
        resolve(data);
      } else {
        reject();
      }
    });
  });
};

OB.CUSTSHA.checkOriginalInvoiceLines = function (order, lines, callback) {
       
      lines = !OB.UTIL.isNullOrUndefined(lines.models) ? lines.models : lines;    
      for (var i = 0; i < lines.length; i++) {
        var flag =  !OB.UTIL.isNullOrUndefined(lines[i].line) ? lines[i].line.get('captureoriginalinvoice') : lines[i].get('captureoriginalinvoice');
       if ( flag === false) {
           var ln = lines[i];
           OB.MobileApp.view.waterfallDown('onShowPopup', {
            popup: 'CUSTSHA.UI.ModalPaidReceiptsCaptureOriginalInvoice',
            args: {
               lineQueue: ln,
               order : order 
            }
          });
          break;
        }
      }
    };
    
 OB.CUSTSHA.getCashupFloatDepositAmount = function (cashupid,financcountid,glitemid,depositAmount,callbacks) {
//  var depositAmount = 0;
  var depositService = new OB.DS.Process('com.openbravo.sharaf.retail.customdevelopments.service.InitialFloatAmountDeposit');
          depositService.exec({
            cashupid: cashupid,
            financcountid: financcountid,
            glitemid: glitemid,
        },
         function(depositdata) {
            if (depositdata && !depositdata.exception) {
                if (depositdata.length !== 0) {
                  _.each(depositdata, function (data) {
                    depositAmount += !OB.UTIL.isNullOrUndefined(data[0]) ? data[0]: 0;
                  });
                callbacks(depositAmount);
                } else {
                   callbacks(depositAmount); 
                }
            } else {
              callbacks(depositAmount);  
            }
        });        
     
  
};
OB.CUSTSHA.getCashupWithdrawAmount = function (cashupid,financcountid,glwithdrawid,totalWithdrawAmount,callbacks) {
//var  withdrawAmount=0,
  //  totalWithdrawAmount =0,
    processWithdrawService = new OB.DS.Process('com.openbravo.sharaf.retail.customdevelopments.service.InitialFloatAmountDrop');
    processWithdrawService.exec({
        cashupid: cashupid,
        financcountid: financcountid,
        glwithdrawid: glwithdrawid
    },
        function(withdrawData) {
            if (withdrawData.length !== 0) {
              _.each(withdrawData, function (data) {
                totalWithdrawAmount += !OB.UTIL.isNullOrUndefined(data[0]) ? data[0]: 0;
              });
              callbacks(totalWithdrawAmount);
            } else {
              callbacks(totalWithdrawAmount);
            }
         });
  
};

OB.CUSTSHA.getCashuplastWithdrawAmount = function (cashupid,financcountid,glwithdrawid,withdrawAmount,callbacks) {
//var  withdrawAmount=0,
    processWithdrawService = new OB.DS.Process('com.openbravo.sharaf.retail.customdevelopments.service.InitialFloatAmountDrop');
    processWithdrawService.exec({
        cashupid: cashupid,
        financcountid: financcountid,
        glwithdrawid: glwithdrawid
    },
        function(withdrawData) {
            if (withdrawData.length !== 0) {
                if (!OB.UTIL.isNullOrUndefined(withdrawData[0][0])) {
                    withdrawAmount = withdrawData[0][0];
                    callbacks(withdrawAmount);
                }
            } else {
              callbacks(withdrawAmount);  
            }
         });
 
};
   
