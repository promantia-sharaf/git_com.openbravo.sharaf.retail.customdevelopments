/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, _, $, CUSTSHA */

enyo.kind({
  name: 'CUSTSHA_ChequePaymentProvider',
  components: [{
    kind: 'Scroller',
    maxHeight: '245px',
    style: 'background-color: #ffffff;',
    thumb: true,
    horizontal: 'hidden',
    components: [{
      name: 'attributes'
    }]
  }, {
    style: 'padding: 10px 0px;',
    components: [{
      kind: 'CUSTSHA.UI.ProviderOK'
    }, {
      kind: 'OB.UI.CancelDialogButton'
    }]
  }],
  newAttributes: [{
    kind: 'CUSTSHA.UI.modularRenderTextProperty',
    name: 'custshaCustomerName',
    modelProperty: 'custshaCustomerName',
    i18nLabel: 'CUSTSHA_CustomerName',
    maxLength: 60,
    readOnly: false
  }, {
    kind: 'CUSTSHA.UI.modularRenderTextProperty',
    name: 'custshaChequeNumber',
    modelProperty: 'custshaChequeNumber',
    i18nLabel: 'CUSTSHA_ChequeNumber',
    maxLength: 60,
    readOnly: false
  }],
  initComponents: function () {
    this.inherited(arguments);
    this.mainPopup.$.header.setContent(OB.I18N.getLabel('CUSTSHA_Cheque'));
    this.propertycomponents = {};

    enyo.forEach(this.newAttributes, function (field) {
      var editline = this.$.attributes.createComponent({
        kind: 'OB.UI.PropertyEditLine',
        name: 'line_' + field.name,
        newAttribute: field
      });
      this.propertycomponents[field.name] = editline.propertycomponent;
      this.propertycomponents[field.name].propertiesDialog = this;
    }, this);
  }
});
enyo.kind({
  name: 'CUSTSHA.UI.ProviderOK',
  kind: 'OB.UI.ModalDialogButton',
  isDefaultAction: true,
  events: {
    onShowPopup: '',
    onHideThisPopup: ''
  },
  tap: function () {
    var payment = OB.MobileApp.model.paymentnames[this.owner.owner.owner.args.key];
    this.owner.receipt.addPayment(new OB.Model.PaymentLine({
      kind: payment.payment.searchKey,
      name: payment.payment[OB.Constants.IDENTIFIER],
      paymentMethod: this.owner.owner.owner.args.paymentMethod,
      amount: this.owner.paymentAmount,
      rate: payment.rate,
      mulrate: payment.mulrate,
      isocode: payment.isocode,
      isCash: payment.paymentMethod.iscash,
      allowOpenDrawer: payment.paymentMethod.allowopendrawer,
      openDrawer: payment.paymentMethod.openDrawer,
      printtwice: payment.paymentMethod.printtwice,
      custsharCustomerName: this.owner.propertycomponents.custshaCustomerName.getValue(),
      custsharChequeNumber: this.owner.propertycomponents.custshaChequeNumber.getValue(),
      paymentData: {
        custsharCustomerName: this.owner.propertycomponents.custshaCustomerName.getValue(),
        custsharChequeNumber: this.owner.propertycomponents.custshaChequeNumber.getValue()
      }
    }));
    this.doHideThisPopup();
  },
  initComponents: function () {
    this.inherited(arguments);
    this.setContent(OB.I18N.getLabel('OBMOBC_LblOk'));
  }
});
enyo.kind({
  name: 'CUSTSHA.UI.modularRenderTextProperty',
  kind: 'OB.UI.renderTextProperty',
  handlers: {
    onRecoverValues: 'recoverValues',
    onResetValue: 'resetValue'
  },
  initComponents: function () {
    this.inherited(arguments);
  },
  loadValue: function (inSender, inEvent) {
    if (!OB.UTIL.isNullOrUndefined(inEvent[this.modelProperty])) {
      this.setValue(inEvent[this.modelProperty]);
    }
  },
  resetValue: function (inSender, inEvent) {
    this.setValue('');
  },
  recoverValues: function (inSender, inEvent) {
    inEvent.values[this.modelProperty] = this.getValue();
  }
});