/*
 ************************************************************************************
 * Copyright (C) 2023 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, _, Google Maps */

// As google does not allow offline storing of JS libraries. Attaching a script DOM

// Create the script tag, set the appropriate attributes
var script = document.createElement('script');
script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyA5TqOJK2Hg2Ep6m5jFfUMI1pmA1djf-jk&libraries=places&v=weekly';
//script.async = true;

// Append the 'script' element to 'head'
document.head.appendChild(script);