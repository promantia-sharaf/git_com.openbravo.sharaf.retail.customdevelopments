/*
 ************************************************************************************
 * Copyright (C) 2023 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global */

enyo.kind({
  name: 'OB.UI.ModalGoogleMaps',
  kind: 'OB.UI.ModalAction',
  hideCloseButton: true,
  autoDismiss: false,
  closeOnEscKey: false,
  latitude: null,
  longitude: null,
  position: null,
  map: null,
  infoWindow: null,
  isSelected: false,
  isCancel: false,
  isSearched: false, 
  i18nHeader: '',
  searchBox: null,
  mapsComponent: null,
  marker: [],
  handlers: {
    
  },
  bodyContent: {
    kind: 'Scroller',
    maxHeight: '300px',
    thumb: true,
    horizontal: 'hidden',
    components: [{
	 kind: 'custshaGoogleMap'	 
   }]
  },
  bodyButtons: {
	style: 'padding:10px;',
    components: [{
      kind: 'OB.UI.ModalSelectLatLng'
    }, {
      kind: 'OB.UI.ModalCancelLatLng'
    }]
  },
  setLatLng: function(lat, lng) {
	this.latitude = +lat.toFixed(6);
	this.longitude = +lng.toFixed(6);
  },
  hasMap: function () {
	if (this.map) {
		return true;
	} 
	return false;
  },
  getLatLng: function() {
    var me = this;
    this.map.addListener("click", (mapsMouseEvent) => {
    	if(me.isSearched){
	   me.setLatLng(mapsMouseEvent.latLng.toJSON().lat, mapsMouseEvent.latLng.toJSON().lng);
	   me.position = mapsMouseEvent.latLng;
	   me.addInfoWindow();
	   me.isSelected = true;
    	}else{
           OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "Serach and then Click on the map to get Lat/Lng");
           return;
    	}
     });	
  },
  createInfoWindow: function() {
	this.infoWindow = new google.maps.InfoWindow({
       content: '<p style="color:black;">Serach and then Click on the map to get Lat/Lng!</p>',
       position: this.position
    });
    
    this.infoWindow.open(this.map);
  }, 
  addInfoWindow: function() {
	if (this.infoWindow) {
		this.infoWindow.close();
	} 
	this.infoWindow.setPosition(this.position);
    this.infoWindow.setContent(
     '<p style="color:black">' + 'Lat: ' + this.latitude + ' Lng:' + this.longitude + '</p>'
    );
    this.infoWindow.open(this.map);
  },
  addSearchBox: function () {
	var me = this;
	if (OB.UTIL.isNullOrUndefined(this.input)) {
	  this.input = document.createElement('input');	
	  this.input.type = 'text';
	  this.mapsComponent.appendChild(this.input);
	} else {
	  this.input.value = '';	
	}
	this.input.placeholder = 'Enter/Search Location';
	this.input.style.width = '50%';
	this.input.style.height = '10%';
	
	const searchBox = new google.maps.places.SearchBox(this.input);
	this.map.controls[google.maps.ControlPosition.TOP_LEFT].push(this.input);
  	// Bias the SearchBox results towards current map's viewport.
  	this.map.addListener('bounds_changed', () => {
          searchBox.setBounds(this.map.getBounds());
    });
    
    this.markers = [];
    searchBox.addListener("places_changed", () => {
        const places = searchBox.getPlaces();
	    if (places.length == 0) {
	      return;
	    }
	
	    // Clear out the old markers.
	    me.markers.forEach((marker) => {
	      marker.setMap(null);
	    });
	    me.markers = [];
	
	    // For each place, get the icon, name and location.
	    const bounds = new google.maps.LatLngBounds();
	
	    places.forEach((place) => {
	      if (!place.geometry || !place.geometry.location) {
	        console.log("Returned place contains no geometry");
	        return;
	      }
	
	      // Create a marker for each place.
	      me.markers.push(
	        new google.maps.Marker({
	          map: me.map,
	          title: place.name,
	          position: place.geometry.location,
	        })
	      );
	      if (place.geometry.viewport) {
	        // Only geocodes have viewport.
	        bounds.union(place.geometry.viewport);
	      } else {
	        bounds.extend(place.geometry.location);
	      }
	    });
	    this.isSearched = true;
	    me.map.fitBounds(bounds);
    });    
  },
  loadMap: function() {
	var id = this.$.bodyContent.$.custshaGoogleMap.node.id;
	if (document.querySelectorAll("[id=' "+ id + " ']").length == 0) {
		this.render();
	}
	var j = 0;
	for (j = 0; j <= document.querySelectorAll("[id='" + id + "']").length; j++) {
	  if (document.querySelectorAll("[id='" + id + "']")[j] && document.querySelectorAll("[id='" + id + "']")[j].parentElement && 
		  (this.$.bodyContent.$.custshaGoogleMap.parent.parent.id === document.querySelectorAll("[id='" + id + "']")[j].parentElement.id)) {
		     this.map = new google.maps.Map(document.querySelectorAll("[id='" + id + "']")[j], {
               center: new google.maps.LatLng(this.latitude, this.longitude),
               zoom: 8,
               mapTypeControlOptions: { mapTypeIds: [] },
               streetViewControl: false,
               fullscreenControl: false
             });
           this.mapsComponent = document.querySelectorAll("[id='" + id + "']")[j];  
           this.getLatLng();
           this.createInfoWindow();  
           this.addSearchBox();
           if (this.reselect) {
	         this.marker.push(new google.maps.Marker({
	           map: this.map,
	           position: new google.maps.LatLng(this.latitude, this.longitude),
	         }));
           }
           break;           
		}
	  }
  },
  destroyMap: function() {
	this.map = null;
	this.infoWindow = null;
	this.latitude = null;
	this.longitude = null;
	this.position = null;
	document.getElementsByTagName('html')[0].style.zoom = this.zoom;
	this.zoom = null;
	this.isSelected = false;
	this.isSearched = false;
	this.isCancel = false;
  },
  executeOnHide: function() {
    if (!this.isSelected) {
	  this.latitude = " - ";
	  this.longitude = " - ";
    }
    if (this.args.args !== null && this.args.callbacks !== null) {
	  OB.UTIL.HookManager.callbackExecutor(this.args.args, this.args.callbacks);
	  this.destroyMap();
    } else {
	  if (!this.isCancel) {
	    this.args.setLatLng(this.latitude, this.longitude);
	  }else{
		  this.args.setLatLng(' - ', ' - ');
	  }
	  this.destroyMap();
    }       
  },
  executeOnShow: function () {
	if (!OB.UTIL.isNullOrUndefined(this.args.args) && this.args.args !== '' && !this.args.args.includes('-')) {
	  this.latitude = +this.args.args.split('/')[0].replace('Lat:', '').trim();
	  this.longitude = +this.args.args.split('/')[1].replace('Lng:', '').trim();
	  this.reselect = true;
	} else {
	  this.latitude = 23.372905;
	  this.longitude = 54.333455;
	  this.reselect = false;		
	}
	this.zoom = +document.getElementsByTagName('html')[0].style.zoom;
	document.getElementsByTagName('html')[0].style.zoom = 1
	
	this.$.header.setContent('Select a delivery location');
    if (!this.hasMap()) {
	  this.loadMap();
    }    
  },
  applyChanges: function (inSender, inEvent) {
    
  },
  initComponents: function () {
    this.inherited(arguments);
    this.attributeContainer = this.$.bodyContent.$.attributes;
    if (!this.$.bodyContent.$.custshaGoogleMap.node) {
	  this.$.bodyContent.$.custshaGoogleMap.renderNode();
      this.$.bodyContent.$.custshaGoogleMap.renderDom();
    }
  }
});

enyo.kind({
  name: 'custshaGoogleMap',
  style: 'height:275px;width:100%;transform: scale(1)',
  initComponents: function () {
    this.inherited(arguments);
    this.render();
  }
});


enyo.kind({
  kind: 'OB.UI.ModalDialogButton',
  name: 'OB.UI.ModalSelectLatLng',
  tap: function() {
    var me = this;
    if(!me.owner.owner.isSelected){
  	  OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_LblError'), "Please select lat/lng then proceed");
  	   return;
    	 }
	  me.doHideThisPopup();
    },
  initComponents: function() {
    this.inherited(arguments);
    this.setContent("Select");
  }
});

enyo.kind({
  kind: 'OB.UI.ModalDialogButton',
  name: 'OB.UI.ModalCancelLatLng',
  tap: function() {
	this.owner.owner.isCancel = true;
	var me = this;
    me.doHideThisPopup();
    },
  initComponents: function() {
    this.inherited(arguments);
    this.setContent("Cancel");
  }
});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'OB.UI.ModalGoogleMaps',
  name: 'OB_UI_ModalGoogleMaps'
});