/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo */

enyo.kind({
  name: 'CUSTSHA.UI.OrderHeaderLabel',
  style: 'background-color: #e2e2e2; color: #000; border: none; display: inline-block; font-size: 16px; font-size: 16px; font-weight: 400; height: 18px; margin: 10px; padding: 5px 15px 7px; text-decoration: none;'
});

OB.UI.OrderHeader.prototype.newButtonComponents = [{
  kind: 'OB.UI.BusinessPartnerSelector',
  name: 'bpbutton'
}, {
  classes: 'customer-buttons-separator'
}, {
  name: 'creditAmount',
  style: 'text-overflow: ellipsis; white-space: nowrap; overflow: hidden; min-width: 7em',
  kind: 'CUSTSHA.UI.CreditAmount'
}, {
  classes: 'customer-buttons-separator'
}, {
  name: 'KSAEinvoiceType',
  kind: 'CUSTSHA.UI.KSAEinvoiceType'
},{
  name: 'dgpStatus',
  kind: 'CUSTSHA.UI.DGPStatus'
}];

enyo.kind({
  name: 'CUSTSHA.UI.CreditAmount',
  kind: 'CUSTSHA.UI.OrderHeaderLabel',
  permission: 'CUSTSHA_CreditAmount',
  disabled: true,
  tap: function () {
    if (this.disabled) {
      return;
    }
  },
  init: function (model) {
    var me = this,
        process = new OB.DS.Process('org.openbravo.retail.posterminal.CheckBusinessPartnerCredit');
    this.model = model;
    if (!OB.MobileApp.model.hasPermission(this.permission)) {
      this.hide();
    } else {
      OB.MobileApp.model.receipt.on('change:bp', function (model) {
        var businessPartner = model.get('bp');
        if (businessPartner.get('custshaBPCatManagescredit')) {
          if (businessPartner.get('creditLimit') === 999999999999999) {
            me.setContent(OB.I18N.getLabel('CUSTSHA_NoLimit'));
          } else {
            process.exec({
              businessPartnerId: businessPartner.get('id'),
              totalPending: 0
            }, function (data) {
              if (data && data.actualCredit) {
                businessPartner.set('creditLimit', data.actualCredit);
                businessPartner.set('creditUsed', 0);
                me.setContent(OB.I18N.getLabel('CUSTSHA_Limit', [data.actualCredit]));
              } else {
                me.setContent(OB.I18N.getLabel('CUSTSHA_Limit', []));
                OB.UTIL.showError(OB.I18N.getLabel('SAPCC_Unabletocheckcredit'));
              }
            }, function () {
              me.setContent(OB.I18N.getLabel('CUSTSHA_Limit', []));
              OB.UTIL.showError(OB.I18N.getLabel('SAPCC_Unabletocheckcredit'));
            });
          }
          if (OB.MobileApp.model.get('lastPaneShown') === 'payment') {
            OB.MobileApp.model.receipt.trigger('scan');
          }
          me.show();
        } else {
          businessPartner.set('creditLimit', 0);
          businessPartner.set('creditUsed', 0);
          me.hide();
        }
      }, this);
    }
  },
  initComponents: function () {
    this.inherited(arguments);
  }
});

enyo.kind({
  name: 'CUSTSHA.UI.DGPStatus',
  kind: 'CUSTSHA.UI.OrderHeaderLabel',
  permission: 'CUSTSHA_DGPStatus',
  disabled: true,
  tap: function () {
    if (this.disabled) {
      return;
    }
  },
  init: function (model) {
    var me = this;
    this.model = model;
    if (!OB.MobileApp.model.hasPermission(this.permission)) {
      this.hide();
    }
    OB.MobileApp.model.receipt.on('change:bp', function (model) {
      var custshaDgpstatus = OB.MobileApp.model.receipt.get('bp').get('custshaDgpstatus');
      if (OB.UTIL.isNullOrUndefined(custshaDgpstatus) || custshaDgpstatus === 'NONE' || custshaDgpstatus === '' || custshaDgpstatus === null) {
        custshaDgpstatus = OB.MobileApp.model.receipt.get('bp').get('businessPartnerCategory_name');
      }
      if (custshaDgpstatus && custshaDgpstatus !== 'NONE') {
        if (custshaDgpstatus === 'SILVER' || custshaDgpstatus === 'Silver') {
          me.setStyle('background-color: #D3D3D3 !important; color: #FFFFFF !important;');
        } else if (custshaDgpstatus === 'GOLD' || custshaDgpstatus === 'Gold') {
          me.setStyle('background-color: #FFDF00 !important; color: #000000 !important;');
        } else if (custshaDgpstatus === 'PLATINUM' || custshaDgpstatus === 'Platinum') {
          me.setStyle('background-color: #E5E4E2 !important; color: #000000 !important;');
        } else {
          me.setStyle('background-color: #000000 !important; color: #FFFFFF !important;');
        }
        me.setContent(custshaDgpstatus);
        me.show();
      } else {
        me.hide();
      }
    }, this);
  },
  initComponents: function () {
    this.inherited(arguments);
  }
});

// KSA Changes

enyo.kind({
   name: 'CUSTSHA.UI.KSAEinvoiceType',
   kind: 'OB.UI.SmallButton',
   classes: 'btnlink-gray',
   style: 'float: left; text-overflow:ellipsis; white-space: nowrap; overflow: hidden;',
   permission: 'CPSKSA_POSEInvoiceActivation',
  tap: function () {
        OB.MobileApp.view.$.containerWindow.showPopup(
            'CPSKSA.UI.KSAEInvoiceB2BConfirmation',
          {
           parm : this  
          }
      );
    this.inherited(arguments);
  },
  init: function (model) {
    var me = this;
    if (!OB.MobileApp.model.hasPermission(this.permission)) {
      this.hide();
    } else {
      OB.MobileApp.model.receipt.on('change:bp', function (model) {
        this.show();
        me.setContent(OB.I18N.getLabel('CPSKSA_EInvoiceB2C'));
            OB.MobileApp.model.receipt.unset('EnableKSAB2BEInvoice');
       }, this);
    }
     
  },
  initComponents: function () {
    this.inherited(arguments);
  }
});