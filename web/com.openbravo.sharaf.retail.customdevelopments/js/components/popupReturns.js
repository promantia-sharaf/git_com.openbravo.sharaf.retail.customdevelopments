/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, _ */

enyo.kind({
  name: 'CUSTSHA.UI.ReturnsPopup',
  kind: 'OB.UI.ModalAction',
  i18nHeader: 'CUSTSHA_ReturnProperties',
  handlers: {
    onApplyChanges: 'applyChanges',
    onSetValue: 'setValue'
  },
  bodyContent: {
    kind: 'Scroller',
    maxHeight: '225px',
    style: 'background-color: #ffffff;',
    thumb: true,
    horizontal: 'hidden',
    components: [{
      name: 'attributes'
    }]
  },
  bodyButtons: {
    components: [{
      kind: 'CUSTSHA.UI.ReturnsApply'
    }, {
      kind: 'OB.UI.CancelDialogButton'
    }]
  },
  executeOnShow: function () {
	  var me = this;
	  var productCondition= this.$.bodyContent.$.attributes.$.line_ProductConditionBox.$.newAttribute.$.ProductConditionBox.$.renderCombo.getValue();
	  var value , helpdeskFlag;
      _.each(OB.MobileApp.model.get('sharafProductCondition'), function (sharafProductCondition) {
    	    value = sharafProductCondition.id;
	        helpdeskFlag = sharafProductCondition.helpdeskticket;
	        if (helpdeskFlag && value === productCondition) {
	        	me.$.bodyContent.$.attributes.$.line_HelpDeskTicketBox.show();
	        }else{
	        	if((OB.UTIL.isNullOrUndefined(productCondition) || productCondition === 'undefined') && value != productCondition){
	        		me.$.bodyContent.$.attributes.$.line_HelpDeskTicketBox.hide();
	        		me.$.bodyContent.$.attributes.$.line_HelpDeskTicketBox.$.newAttribute.$.HelpDeskTicketBox.clear();
	        		}
	        	}
	        });
    this.waterfall('onLoadValue', {
      selectedModels: this.args.selectedModels
    });
  },
  applyChanges: function (inSender, inEvent) {
    this.waterfall('onApplyChange', {});
    return true;
  },
  setValue: function (inSender, inEvent) {
    var i = 0;
    if (!OB.UTIL.isNullOrUndefined(this.args) && !OB.UTIL.isNullOrUndefined(this.args.selectedModels) && this.args.selectedModels.length > 0) {
      for (i; i < this.args.selectedModels.length; i++) {
        this.args.selectedModels[i].set(inEvent.modelProperty, inEvent.value);
      }
    }
    return true;
  },
  initComponents: function () {
    this.inherited(arguments);
    this.attributeContainer = this.$.bodyContent.$.attributes;
    enyo.forEach(this.newAttributes, function (natt) {
      this.$.bodyContent.$.attributes.createComponent({
        kind: 'OB.UI.PropertyEditLine',
        name: 'line_' + natt.name,
        newAttribute: natt
      });
    }, this);
  }
});

enyo.kind({
  name: 'CUSTSHA.UI.ReturnsPopupImpl',
  kind: 'CUSTSHA.UI.ReturnsPopup',
  newAttributes: [{
    kind: 'CUSTSHA.UI.ProductCondition',
    name: 'ProductConditionBox',
    i18nLabel: 'CUSTSHA_ProductCondition'
  }, {
    kind: 'CUSTSHA.UI.CustomerRequest',
    name: 'CustomerRequestBox',
    i18nLabel: 'CUSTSHA_CustomerRequest'
  }, {
	kind: 'CUSTSHA.UI.HelpdeskTicket',
	name: 'HelpDeskTicketBox',
	i18nLabel: 'CUSTSHA_HelpDeskTicket',
	maxLength: 255
}]
});

enyo.kind({
  name: 'CUSTSHA.UI.ProductCondition',
  kind: 'OB.UI.renderComboProperty',
  onchange: 'applyChange',
  events: {
    onSetValue: ''
  },
  handlers: {
	onchange: 'selectChanged'
  },
  modelProperty: 'cUSTSHAProductCondition',
  retrievedPropertyForValue: 'id',
  retrievedPropertyForText: 'name',
  init: function (model) {
    this.model = model;
    this.collection = new Backbone.Collection();
    this.$.renderCombo.setCollection(this.collection);
    var i = 0;
    this.collection.add(new Backbone.Model());
    for (i; i < OB.MobileApp.model.get('sharafProductCondition').length; i++) {
      model = new Backbone.Model(OB.MobileApp.model.get('sharafProductCondition')[i]);
      this.collection.add(model);
    }
  },
  loadValue: function (inSender, inEvent) {
    var index = 0;
    if (inEvent.selectedModels && inEvent.selectedModels.length > 0 && inEvent.selectedModels[0].get('cUSTSHAProductCondition')) {
      _.each(this.collection.models, function (st, idx) {
        if (st.get('id') === inEvent.selectedModels[0].get('cUSTSHAProductCondition')) {
          index = idx;
        }
      });
    }
    this.$.renderCombo.setSelected(index);
  },
  applyChange: function (inSender, inEvent) {
    var selected = this.collection.at(this.$.renderCombo.getSelected());
    if (selected) {
      this.doSetValue({
        modelProperty: this.modelProperty,
        value: selected.get(this.retrievedPropertyForValue)
      });
    }
  },
  selectChanged : function (inSender, inEvent) {
	var me = this;
	var id = inSender.getValue();
	_.each(inSender.children, function (prodCond) {
		var value = prodCond.value;
		var content = prodCond.content;
		var isHelpdeskTicket = prodCond.model.get('helpdeskticket');
		if(value === id && isHelpdeskTicket){
			me.owner.owner.owner.owner.owner.attributeContainer.$.line_HelpDeskTicketBox.show();
		}else{
			me.owner.owner.owner.owner.owner.attributeContainer.$.line_HelpDeskTicketBox.hide();
		}
		});
	}
});

enyo.kind({
  name: 'CUSTSHA.UI.CustomerRequest',
  kind: 'OB.UI.renderComboProperty',
  events: {
    onSetValue: ''
  },
  modelProperty: 'cUSTSHACustomerRequest',
  retrievedPropertyForValue: 'id',
  retrievedPropertyForText: 'name',
  init: function (model) {
    this.model = model;
    this.collection = new Backbone.Collection();
    this.$.renderCombo.setCollection(this.collection);
    var i = 0;
    this.collection.add(new Backbone.Model());
    for (i; i < OB.MobileApp.model.get('sharafCustomerRequest').length; i++) {
      model = new Backbone.Model(OB.MobileApp.model.get('sharafCustomerRequest')[i]);
      this.collection.add(model);
    }
  },
  loadValue: function (inSender, inEvent) {
    var index = 0;
    if (inEvent.selectedModels && inEvent.selectedModels.length > 0 && inEvent.selectedModels[0].get('cUSTSHACustomerRequest')) {
      _.each(this.collection.models, function (st, idx) {
        if (st.get('id') === inEvent.selectedModels[0].get('cUSTSHACustomerRequest')) {
          index = idx;
        }
      });
    }
    this.$.renderCombo.setSelected(index);
  },
  applyChange: function (inSender, inEvent) {
    var selected = this.collection.at(this.$.renderCombo.getSelected());
    if (selected) {
      this.doSetValue({
        modelProperty: this.modelProperty,
        value: selected.get(this.retrievedPropertyForValue)
      });
    }
  }
});

enyo.kind({
	  name: 'CUSTSHA.UI.HelpdeskTicket',
	  kind: 'OB.UI.renderTextProperty',
	  events: {
	    onSetValue: ''
	  },
	  modelProperty: 'cUSTSHAHelpDeskTicket',
	  init: function (model) {
	    this.model = model;
	  },
	  loadValue: function (inSender, inEvent) {
		  var me = this;
		  var isProdCondValue = false; 
		  var ProdCondValue = inSender.$.bodyContent.$.attributes.$.line_ProductConditionBox.$.newAttribute.$.ProductConditionBox.$.renderCombo.getValue();
		  _.each(OB.MobileApp.model.get('sharafProductCondition'), function (sharafProductCondition) {
	    	    value = sharafProductCondition.id;
		        helpdeskFlag = sharafProductCondition.helpdeskticket;
		        if (helpdeskFlag && value === ProdCondValue) {
		        	isProdCondValue = true;
		        }else{
		        	isProdCondValue = false;
		        }
		        });
	    if (inEvent.selectedModels && inEvent.selectedModels.length > 0 && isProdCondValue) {
	    	this.owner.owner.owner.$.line_HelpDeskTicketBox.show();
	    	if(!OB.UTIL.isNullOrUndefined(inEvent.selectedModels[0].attributes.product.attributes.helpDeskTicket)){
	    		this.owner.$.HelpDeskTicketBox.setValue(inEvent.selectedModels[0].attributes.product.attributes.helpDeskTicket)
	    	}
	    	}
	    else{
	    	this.owner.$.HelpDeskTicketBox.clear();
	    	this.owner.owner.owner.$.line_HelpDeskTicketBox.hide();
	    	}
	  },
	  applyChange: function (inSender, inEvent) {	 
	  }
	});

enyo.kind({
  name: 'CUSTSHA.UI.ReturnsApply',
  kind: 'OB.UI.ModalDialogButton',
  events: {
    onHideThisPopup: '',
    onApplyChanges: ''
  },
  tap: function () {
	var helpdeskTicket;
	var me = this;
	var prodCondValue;
	var isHelpDeskFlag = false;
    this.doApplyChanges();
    if(!OB.UTIL.isNullOrUndefined(me.owner.owner.$.bodyContent.$.attributes.$.line_ProductConditionBox)){
    	prodCondValue = me.owner.owner.$.bodyContent.$.attributes.$.line_ProductConditionBox.$.newAttribute.$.ProductConditionBox.$.renderCombo.getValue();
    }
    _.each(OB.MobileApp.model.get('sharafProductCondition'), function (sharafProductCondition) {
	    value = sharafProductCondition.id;
        helpdeskFlag = sharafProductCondition.helpdeskticket;
        if (helpdeskFlag && value === prodCondValue) {
        	isHelpDeskFlag = true;
        }else{
        	isHelpDeskFlag = false;
        	}
        });
    _.each(me.owner.owner.args.selectedModels, function (lines) {
    if(!OB.UTIL.isNullOrUndefined(me.owner.owner.$.bodyContent.$.attributes.$.line_HelpDeskTicketBox) && !OB.UTIL.isNullOrUndefined(me.owner.owner.$.bodyContent.$.attributes.$.line_HelpDeskTicketBox.$.newAttribute.$.HelpDeskTicketBox.getValue()) && me.owner.owner.$.bodyContent.$.attributes.$.line_HelpDeskTicketBox.$.newAttribute.$.HelpDeskTicketBox.getValue() != '' && isHelpDeskFlag){
    	helpdeskTicket = me.owner.owner.$.bodyContent.$.attributes.$.line_HelpDeskTicketBox.$.newAttribute.$.HelpDeskTicketBox.getValue();
        	lines.get('product').set('helpDeskTicket',helpdeskTicket);
        	me.doHideThisPopup();
    }else{
    	if(isHelpDeskFlag){
            OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), 'Helpdesk Ticket Number cannot be blank.');
            return false;
    	}
    	lines.get('product').set('helpDeskTicket','');
    	 me.doHideThisPopup();
    }
    });
    this.model.get('order').save();
    this.model.get('orderList').saveCurrent();
  },
  initComponents: function () {
    this.inherited(arguments);
    this.setContent(OB.I18N.getLabel('OBMOBC_LblApply'));
  },
  init: function (model) {
    this.model = model;
  }
});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'CUSTSHA.UI.ReturnsPopupImpl',
  name: 'CUSTSHA_UI_ReturnsPopup'
});