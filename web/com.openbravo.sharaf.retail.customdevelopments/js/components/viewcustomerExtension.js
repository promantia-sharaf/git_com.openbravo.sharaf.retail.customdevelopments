OB.OBPOSPointOfSale.UI.customers.editcustomers_impl.prototype.newAttributes.shift({
  kind: 'OB.UI.CustomerTextProperty',
  name: 'customerName'
});

OB.OBPOSPointOfSale.UI.customers.editcustomers_impl.prototype.newAttributes.shift({
  kind: 'OB.UI.CustomerTextProperty',
  name: 'customerLastName'
});

OB.OBPOSPointOfSale.UI.customers.editcustomers_impl.prototype.newAttributes.shift({
  kind: 'OB.UI.CustomerTextProperty',
  name: 'customerBpCat'
});

OB.OBPOSPointOfSale.UI.customers.editcustomers_impl.prototype.newAttributes.unshift({
  kind: 'OB.UI.DGMemberCustomerProperty',
  name: 'bpDGMemberCustomer',
  modelProperty: 'dgMember',
  modelPropertyText: 'dgMember',
  i18nLabel: 'CUSTSHA_DGMember',

  displayLogic: function () {
    if ((this.$.invoiceCount.value.length > 0 && this.$.qtyCount.value.length > 0) && this.$.invoiceCount.value >= 0 && this.$.qtyCount.value >= 0) {
      this.show();
      this.owner.owner.owner.$.line_bpDGMemberCustomer.setShowing('true');
      return this;
    } else {
      return;
    }
  }
});

enyo.kind({
  name: 'OB.UI.DGMemberCustomerProperty',

  components: [{
    kind: 'OB.UI.MenuAction',
    name: 'invoiceCountLabel',
    i18nLabel: 'CUSTSHA_LblInvoiceCount',
    readOnly: true,
    style: 'font-style: italic; width: 29%; float: left; margin: 0;',
    loadValue: function (inSender, inEvent) {
      this.setValue(OB.I18N.getLabel(this.i18nLabel));
    }
  }, {
    kind: 'OB.UI.CustomerTextProperty',
    name: 'invoiceCount',
    readOnly: true,
    style: 'width: 17%; float: left;',
    loadValue: function (inSender, inEvent) {
      if (inEvent.customer !== undefined && (!OB.UTIL.isNullOrUndefined(inEvent.customer.get('cUSTSHADGInvoiceCount')))) {
        this.setValue(inEvent.customer.get('cUSTSHADGInvoiceCount'));
      } else {
        this.setValue('');
      }
    },

  }, {
    kind: 'OB.UI.MenuAction',
    name: 'qtyCountLabel',
    i18nLabel: 'CUSTSHA_LblItemQtyCount',
    style: 'font-style: italic; width: 30%; float: left;',
    readOnly: true,
    loadValue: function (inSender, inEvent) {
      this.setValue(OB.I18N.getLabel(this.i18nLabel));
    }
  }, {
    kind: 'OB.UI.CustomerTextProperty',
    name: 'qtyCount',
    readOnly: true,
    style: 'width: 17%; float: left;',

    loadValue: function (inSender, inEvent) {
      if (inEvent.customer !== undefined && (!OB.UTIL.isNullOrUndefined(inEvent.customer.get('cUSTSHADGItemQtyCount')))) {
        this.setValue(inEvent.customer.get('cUSTSHADGItemQtyCount'));
      } else {
        this.setValue('');
      }
    }
  }]
});

OB.OBPOSPointOfSale.UI.customers.editcustomers_impl.prototype.newAttributes.unshift({
  kind: 'OB.UI.CustomerTextProperty',
  name: 'customerBpCat',
  modelProperty: 'businessPartnerCategory_name',
  i18nLabel: 'OBPOS_BPCategory',
  readOnly: true,
  displayLogic: function () {
    return OB.MobileApp.model.get('terminal').bp_showcategoryselector;
  }
});

//

OB.OBPOSPointOfSale.UI.customers.editcustomers_impl.prototype.newAttributes.unshift({
  kind: 'OB.UI.CustomerTextProperty',
  name: 'custshaLoyaltyCard',
  modelProperty: 'custshaLoyaltyCard',
  isFirstFocus: true,
  readOnly: true,
  i18nLabel: 'CUSTSHA_LblLoyalityCard',
  maxlength: 12,
  
  loadValue: function (inSender, inEvent) {
      if (inEvent.customer !== undefined && (!OB.UTIL.isNullOrUndefined(inEvent.customer.get('custshaLoyaltyCard')))
         && inEvent.customer.get('custshaLoyaltyCard') !=0 ) {
        this.setValue(inEvent.customer.get('custshaLoyaltyCard'));
      } else {
        this.setValue('');
      }
    }
    
});

// ksa start---------------
OB.OBPOSPointOfSale.UI.customers.editcustomers_impl.prototype.newAttributes.unshift({
  kind: 'OB.UI.CustomerTextProperty',
  name: 'cpsksaCustomeridvalue',
  modelProperty: 'cpsksaCustomeridvalue',
  isFirstFocus: true,
  readOnly: true,
  i18nLabel: 'CPSKSA_PartyIdVal',
  displayLogic: function () {
    return OB.MobileApp.model.get('terminal').client === '63FE5171371A45D48E5322531C1EFEFC' ;
  },
  loadValue: function (inSender, inEvent) {
      if (inEvent.customer !== undefined && (!OB.UTIL.isNullOrUndefined(inEvent.customer.get('cpsksaCustomeridvalue')))
         && inEvent.customer.get('cpsksaCustomeridvalue') != '' ) {
        this.setValue(inEvent.customer.get('cpsksaCustomeridvalue'));
      } else {
        this.setValue('');
      }
    }
    
});
OB.OBPOSPointOfSale.UI.customers.editcustomers_impl.prototype.newAttributes.unshift({
  kind: 'OB.UI.CustomerTextProperty',
  name: 'cpsksaCustomerid',
  modelProperty: 'cpsksaCustomerid',
  isFirstFocus: true,
  readOnly: true,
  i18nLabel: 'CPSKSA_PartyId',
  displayLogic: function () {
    return OB.MobileApp.model.get('terminal').client === '63FE5171371A45D48E5322531C1EFEFC' ;
  },
  loadValue: function (inSender, inEvent) {
    var value = '';  
      if (inEvent.customer !== undefined && (!OB.UTIL.isNullOrUndefined(inEvent.customer.get('cpsksaCustomerid')))
         && inEvent.customer.get('cpsksaCustomerid') != '' ) {
     for (var i=0; i < OB.MobileApp.model.get('businessPartnerIdentityInfo').length; i++) {
           var model = OB.MobileApp.model.get('businessPartnerIdentityInfo')[i];
          if(model.id === inEvent.customer.get('cpsksaCustomerid')) {
             value = model.description;
             break;
          }  
      }   
    }
    this.setValue(value);
  }  
});

// ksa end ------------------------------------

OB.OBPOSPointOfSale.UI.customers.editcustomers_impl.prototype.newAttributes.unshift({
  kind: 'OB.UI.CustomerTextProperty',
  name: 'customerLastName',
  modelProperty: 'lastName',
  i18nLabel: 'OBPOS_LblLastName',
  readOnly: true
});
OB.OBPOSPointOfSale.UI.customers.editcustomers_impl.prototype.newAttributes.unshift({
  kind: 'OB.UI.CustomerTextProperty',
  name: 'customerName',
  modelProperty: 'firstName',
  i18nLabel: 'OBPOS_LblName',
  readOnly: true
});
