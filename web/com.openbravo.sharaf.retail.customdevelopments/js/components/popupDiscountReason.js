/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, _ */

enyo.kind({
  name: 'CUSTSHA.UI.DiscountReasonPopup',
  kind: 'OB.UI.ModalAction',
  i18nHeader: 'CUSTSHA_DiscountReason',
  closeOnAcceptButton: true,
  handlers: {
    onApplyChanges: 'applyChanges',
    onSetValue: 'setValue'
  },
  bodyContent: {
    kind: 'Scroller',
    maxHeight: '225px',
    style: 'background-color: #ffffff;',
    thumb: true,
    horizontal: 'hidden',
    components: [{
      name: 'attributes'
    }]
  },
  bodyButtons: {
    components: [{
      kind: 'OB.UI.AcceptDialogButton'
    }, {
      kind: 'OB.UI.CancelDialogButton'
    }]
  },
  executeOnShow: function () {
    this.waterfall('onLoadValue', {
      selectedModels: this.args.selectedModels
    });
  },
  acceptCallback: function () {
    this.waterfall('onApplyChange', {});
    return true;
  },
  setValue: function (inSender, inEvent) {
    var i = 0;
    if (!OB.UTIL.isNullOrUndefined(this.args) && !OB.UTIL.isNullOrUndefined(this.args.selectedModels) && this.args.selectedModels.length > 0) {
      for (i; i < this.args.selectedModels.length; i++) {
        this.args.selectedModels[i].set(inEvent.modelProperty, inEvent.value);
      }
    }
    return true;
  },
  initComponents: function () {
    this.inherited(arguments);
    this.$.bodyButtons.$.acceptDialogButton.dialogContainer = this;
    this.attributeContainer = this.$.bodyContent.$.attributes;
    enyo.forEach(this.newAttributes, function (natt) {
      this.$.bodyContent.$.attributes.createComponent({
        kind: 'OB.UI.PropertyEditLine',
        name: 'line_' + natt.name,
        newAttribute: natt
      });
    }, this);
  }
});

enyo.kind({
  name: 'CUSTSHA.UI.DiscountReasonPopupImpl',
  kind: 'CUSTSHA.UI.DiscountReasonPopup',
  newAttributes: [{
    kind: 'CUSTSHA.UI.DiscountReasonBox',
    name: 'DiscountReasonBox',
    i18nLabel: 'CUSTSHA_DiscountReason'
  }]
});

enyo.kind({
  name: 'CUSTSHA.UI.DiscountReasonBox',
  kind: 'OB.UI.renderComboProperty',
  events: {
    onSetValue: ''
  },
  modelProperty: 'custshaDiscountReason',
  retrievedPropertyForValue: 'id',
  retrievedPropertyForText: 'name',
  init: function (model) {
    this.model = model;
    this.collection = new Backbone.Collection();
    this.$.renderCombo.setCollection(this.collection);
    var i = 0;
    for (i; i < OB.MobileApp.model.get('sharafDiscountReason').length; i++) {
      model = new Backbone.Model(OB.MobileApp.model.get('sharafDiscountReason')[i]);
      this.collection.add(model);
    }
  },
  loadValue: function (inSender, inEvent) {
    var index = 0;
    if (inEvent.selectedModels && inEvent.selectedModels.length > 0 && inEvent.selectedModels[0].get('custshaDiscountReason')) {
      _.each(this.collection.models, function (st, idx) {
        if (st.get('id') === inEvent.selectedModels[0].get('custshaDiscountReason')) {
          index = idx;
        }
      });
    }
    this.$.renderCombo.setSelected(index);
  },
  applyChange: function (inSender, inEvent) {
    var selected = this.collection.at(this.$.renderCombo.getSelected());
    if (selected) {
      this.doSetValue({
        modelProperty: this.modelProperty,
        value: selected.get(this.retrievedPropertyForValue)
      });
    }
  }
});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'CUSTSHA.UI.DiscountReasonPopupImpl',
  name: 'CUSTSHA_UI_DiscountReasonPopup'
});
