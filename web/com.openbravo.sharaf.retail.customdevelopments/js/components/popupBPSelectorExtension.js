/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone */

OB.UI.ListBpsSelectorLine.prototype.kindComponents[0].components[0].components.push({
  style: 'float: right;',
  components: [{
    kind: 'CUSTSHA.UI.bpCategory',
    name: 'bpCategory'
  }]
});

enyo.kind({
  name: 'CUSTSHA.UI.bpCategory',
  kind: 'CUSTSHA.UI.OrderHeaderLabel',
  permission: 'CUSTSHA_DGPStatus',
  tap: function () {
    if (this.disabled) {
      return;
    }
  },
  initComponents: function () {
    this.inherited(arguments);
    var me = this;
    if (!OB.MobileApp.model.hasPermission(this.permission)) {
      this.hide();
    }
    var custshaDgpstatus = this.owner.model.get('custshaDgpstatus');
    if (OB.UTIL.isNullOrUndefined(custshaDgpstatus) || custshaDgpstatus === 'NONE' || custshaDgpstatus === '' || custshaDgpstatus === null) {
      custshaDgpstatus = this.owner.model.get('bpCategory') ? this.owner.model.get('bpCategory') : this.owner.model.get('businessPartnerCategory_name');
    }
    if (custshaDgpstatus && custshaDgpstatus !== 'NONE') {
      if (custshaDgpstatus === 'SILVER' || custshaDgpstatus === 'Silver') {
        me.setStyle('background-color: #D3D3D3 !important; color: #FFFFFF !important; margin: 0 !important');
      } else if (custshaDgpstatus === 'GOLD' || custshaDgpstatus === 'Gold') {
        me.setStyle('background-color: #FFDF00 !important; color: #000000 !important; margin: 0 !important');
      } else if (custshaDgpstatus === 'PLATINUM' || custshaDgpstatus === 'Platinum') {
        me.setStyle('background-color: #E5E4E2 !important; color: #000000 !important; margin: 0 !important');
      } else {
        me.setStyle('background-color: #000000 !important; color: #FFFFFF !important; margin: 0 !important');
      }
      me.setContent(custshaDgpstatus);
      me.show();
    } else {
      me.hide();
    }
  }
});