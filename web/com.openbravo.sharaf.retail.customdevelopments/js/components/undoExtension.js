/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

OB.OBPOSPointOfSale.UI.Scan.extend({
  manageUndo: function () {
    this.$.msgaction.hide();
    this.$.msgwelcome.show();
    this.$.undobutton.undoaction = null;
    delete this.$.undobutton.undoclick;
  }
});
