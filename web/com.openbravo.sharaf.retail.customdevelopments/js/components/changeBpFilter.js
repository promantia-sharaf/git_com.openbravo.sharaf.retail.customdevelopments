/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone */

OB.UI.FilterSelectorTableHeader.extend({
  getSelectedIndex: function () {
    if (this.owner.kind === 'OB.UI.ModalBpSelectorScrollableHeader') {
      var i, selected = 0,
          position = -3,
          properties = OB.Model.BPartnerFilter.getProperties();
      for (i = 0; i < properties.length; i++) {
        if (properties[i].filter === true) {
          position = position + 1;
          if (properties[i].name === 'phone') {
            selected = position;
          }
        }
      }
      return selected;
    } else {
      return 0;
    }
  }
});