/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, _ */

OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.newAttributes.forEach(function (field) {
  if (field.modelProperty === 'businessPartnerCategory') {
    field.displayLogic = function () {
      return false;
    };
  }
});