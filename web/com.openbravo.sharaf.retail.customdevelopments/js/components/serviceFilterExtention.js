OB.UI.SearchServicesFilter.extend({
sqlFilter: function() {
     var me = this,
      result = {},
      where = '',
      filters = [],
      auxProdFilters = [],
      auxCatFilters = [],
      auxProdStr = '(',
      auxCatStr = '(',
      appendProdComma = false,
      appendCatComma = false,
      existingServices,
      lineIdList;

    if(!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.receipt.attributes.custsdtDocumenttypeSearchKey) 
            && OB.MobileApp.model.receipt.attributes.custsdtDocumenttypeSearchKey === 'DP'){
    	if (this.productId){
    		 var serviceType=[];
    	       var k=0;
    	      // Only one product
    	      existingServices = OB.MobileApp.model.receipt
    	        .get('lines')
    	        .filter(function(l) {
    	          if (
    	            l.get('relatedLines') &&
    	            _.indexOf(
    	              _.pluck(l.get('relatedLines'), 'orderlineId'),
    	              me.orderline.get('id')
    	            ) !== -1
    	          ) {
    	            return true;
    	          }
    	          return false;
    	        })
    	        .map(function(line) {
    	          var product = line.get('product');  
    	          return product.get('forceFilterId') || product.get('id');
    	        });
    	       var product = this.orderline.get('product'),
    	        productId = product.get('forceFilterId') || product.get('id');
    	         
    	       where =
    	        " and product.productType = 'S' and product.isLinkedToProduct = 'true' and ";
    	          
    	       where +=
    	        "  product.proposalType='MP' and  product.custshaEwdpdoctypeEnabled = 'true' " ;
    	      where +=
    	        "and product.m_product_id not in ('" +
    	        (existingServices.length > 0 ? existingServices.join("','") : '-') +
    	        "')";
    	}
    }else{
    if (this.productList && this.productList.length > 0) {
      //product multiselection
      lineIdList = this.orderlineList.map(function(line) {
        return line.get('id');
      });
      existingServices = OB.MobileApp.model.receipt
        .get('lines')
        .filter(function(l) {
          if (
            l.get('relatedLines') &&
            _.intersection(
              lineIdList,
              _.pluck(l.get('relatedLines'), 'orderlineId')
            ).length > 0
          ) {
            return true;
          }
          return false;
        })
        .map(function(line) {
          var product = line.get('product');
          return product.get('forceFilterId') || product.get('id');
        });

      //build auxiliar string for the products filter and for the categories filter:
      this.orderlineList.forEach(function(l) {
        if (appendProdComma) {
          auxProdStr += ', ';
        } else {
          appendProdComma = true;
        }
        auxProdStr += '?';

        var product = l.get('product');
        auxProdFilters.push(product.get('forceFilterId') || product.get('id'));

        if (
          auxCatFilters.indexOf(l.get('product').get('productCategory')) < 0
        ) {
          if (appendCatComma) {
            auxCatStr += ', ';
          } else {
            appendCatComma = true;
          }
          auxCatStr += '?';

          auxCatFilters.push(l.get('product').get('productCategory'));
        }
      });

      auxProdStr += ')';
      auxCatStr += ')';

      where =
        " and product.productType = 'S' and (product.isLinkedToProduct = 'true' and ";

      if (this.productList.length > 1) {
        where += " product.availableForMultiline = 'true' and ";
      }

      //including/excluding products
      where +=
        "((product.includeProducts = 'Y' and not exists (select 1 from m_product_service sp where product.m_product_id = sp.m_product_id and sp.m_related_product_id in " +
        auxProdStr +
        ' )) ';
      where +=
        "or (product.includeProducts = 'N' and " +
        auxProdFilters.length +
        ' = (select count(*) from m_product_service sp where product.m_product_id = sp.m_product_id and sp.m_related_product_id in ' +
        auxProdStr +
        ' )) ';
      where += 'or product.includeProducts is null) ';

      //including/excluding product categories
      where +=
        "and ((product.includeProductCategories = 'Y' and not exists (select 1 from m_product_category_service spc where product.m_product_id = spc.m_product_id and spc.m_product_category_id in " +
        auxCatStr +
        ' )) ';
      where +=
        "or (product.includeProductCategories = 'N' and " +
        auxCatFilters.length +
        ' = (select count(*) from m_product_category_service spc where product.m_product_id = spc.m_product_id and spc.m_product_category_id in ' +
        auxCatStr +
        ' )) ';
      where += 'or product.includeProductCategories is null)) ';
      where +=
        "and product.m_product_id not in ('" +
        existingServices.join("','") +
        "')";

      filters = filters.concat(auxProdFilters);
      filters = filters.concat(auxProdFilters);
      filters = filters.concat(auxCatFilters);
      filters = filters.concat(auxCatFilters);
      
    } else if (this.productId) {
      
      if (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.get('permissions').CUSTSHA_EnableServiceArticleBilling) 
            && OB.MobileApp.model.get('permissions').CUSTSHA_EnableServiceArticleBilling) {
             var serviceType=[];
       var k=0;
      // Only one product
      existingServices = OB.MobileApp.model.receipt
        .get('lines')
        .filter(function(l) {
          if (
            l.get('relatedLines') &&
            _.indexOf(
              _.pluck(l.get('relatedLines'), 'orderlineId'),
              me.orderline.get('id')
            ) !== -1
          ) {
            return true;
          }
          return false;
        })
        .map(function(line) {
          var product = line.get('product');  
          return product.get('forceFilterId') || product.get('id');
        });
       var product = this.orderline.get('product'),
        productId = product.get('forceFilterId') || product.get('id');
        
        if(product.attributes.custshaExtdwtyEnabled){
          serviceType[k++] ='"EW"';
          console.log('--1-'+serviceType.toString());
         } 
        if(product.attributes.custshaDgspremiumEnabled){
           serviceType[k++] = '"DGSP"';
           console.log('--2-'+serviceType.toString());
        } 
        if(product.attributes.custshaDgshieldEnabled){
           serviceType[k++] = '"DGS"';
           console.log('--3-'+serviceType.toString());
        }
        if(product.attributes.custshaApplecareEnabled){
            serviceType[k++] = '"AC+"';
            console.log('--4-'+serviceType.toString());
         }
        if(product.attributes.custshaDgsplusEnabled){
            serviceType[k++] = '"DGS +"';
            console.log('--5-'+serviceType.toString());
         }
       
       where =
        " and product.productType = 'S' and product.isLinkedToProduct = 'true' and ";
          
       where +=
        "  product.proposalType='MP' and  product.custshaServiceType in (" + serviceType.toString()+ ")" ;
      where +=
        "and product.m_product_id not in ('" +
        (existingServices.length > 0 ? existingServices.join("','") : '-') +
        "')";
                
      } else {
          
           // Only one product
      existingServices = OB.MobileApp.model.receipt
        .get('lines')
        .filter(function(l) {
          if (
            l.get('relatedLines') &&
            _.indexOf(
              _.pluck(l.get('relatedLines'), 'orderlineId'),
              me.orderline.get('id')
            ) !== -1
          ) {
            return true;
          }
          return false;
        })
        .map(function(line) {
          var product = line.get('product');
          return product.get('forceFilterId') || product.get('id');
        });

      where =
        " and product.productType = 'S' and (product.isLinkedToProduct = 'true' and ";

      //including/excluding products
      where +=
        "((product.includeProducts = 'Y' and not exists (select 1 from m_product_service sp where product.m_product_id = sp.m_product_id and sp.m_related_product_id = ? )) ";
      where +=
        "or (product.includeProducts = 'N' and exists (select 1 from m_product_service sp where product.m_product_id = sp.m_product_id and sp.m_related_product_id = ? )) ";
      where += 'or product.includeProducts is null) ';

      //including/excluding product categories
      where +=
        "and ((product.includeProductCategories = 'Y' and not exists (select 1 from m_product_category_service spc where product.m_product_id = spc.m_product_id and spc.m_product_category_id =  ? )) ";
      where +=
        "or (product.includeProductCategories = 'N' and exists (select 1 from m_product_category_service spc where product.m_product_id = spc.m_product_id and spc.m_product_category_id  = ? )) ";
      where += 'or product.includeProductCategories is null)) ';
      where +=
        "and product.m_product_id not in ('" +
        (existingServices.length > 0 ? existingServices.join("','") : '-') +
        "')";

      var product = this.orderline.get('product'),
        productId = product.get('forceFilterId') || product.get('id');
      filters.push(productId);
      filters.push(productId);
      filters.push(this.orderline.get('product').get('productCategory'));
      filters.push(this.orderline.get('product').get('productCategory'));
      }        
    }
}
    result.where = where;
    result.filters = filters;
    return result;
  }

});