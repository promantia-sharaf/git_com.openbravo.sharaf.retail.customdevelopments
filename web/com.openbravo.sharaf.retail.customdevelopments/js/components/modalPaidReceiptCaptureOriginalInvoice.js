/*global OB, Backbone, _, moment, enyo */

/*header of scrollable table*/
enyo.kind({
    name: 'CUSTSHA.UI.ModalPRScrollableHeader',
    kind: 'OB.UI.ScrollableTableHeader',
    events: {
        onSearchAction: '',
        onClearAction: ''
    },
    handlers: {
        onFiltered: 'searchAction'
    },
    components: [{
        style: 'padding: 10px;',
        components: [{
            style: 'display: table;',
            components: [{
                style: 'display: table-cell; width: 100%;',
                components: [{
                    kind: 'OB.UI.SearchInputAutoFilter',
                    name: 'filterText',
                    style: 'width: 100%',
                    skipAutoFilterPref: 'OBPOS_remote.order'
                }]
            }, {
                style: 'display: table-cell;',
                components: [{
                    kind: 'OB.UI.SmallButton',
                    name: 'clearButton',
                    classes: 'btnlink-gray btn-icon-small btn-icon-clear',
                    style: 'width: 100px; margin: 0px 5px 8px 19px;',
                    ontap: 'clearAction'
                }]
            }, {
                style: 'display: table-cell;',
                components: [{
                    kind: 'OB.UI.SmallButton',
                    name: 'searchButton',
                    classes: 'btnlink-yellow btn-icon-small btn-icon-search',
                    style: 'width: 100px; margin: 0px 0px 8px 5px;',
                    ontap: 'searchAction'
                }]
            }]
        }, {
            style: 'display: table;',
            components: [{
                style: 'display: table-cell;',
                components: [{
                    tag: 'h4',
                    initComponents: function() {
                        this.setContent(OB.I18N.getLabel('OBPOS_LblStartDate'));
                    },
                    style: 'width: 200px;  margin: 0px 0px 2px 5px;'
                }]
            }, {
                style: 'display: table-cell;',
                components: [{
                    tag: 'h4',
                    initComponents: function() {
                        this.setContent(OB.I18N.getLabel('OBPOS_LblEndDate'));
                    },
                    style: 'width 200px; margin: 0px 0px 2px 65px;'
                }]
            }]
        }, {
            style: 'display: table;',
            components: [{
                style: 'display: table-cell;',
                components: [{
                    kind: 'enyo.Input',
                    name: 'startDate',
                    size: '10',
                    type: 'text',
                    style: 'width: 100px;  margin: 0px 0px 8px 5px;',
                    onchange: 'searchAction'
                }]
            }, {
                style: 'display: table-cell;',
                components: [{
                    tag: 'h4',
                    initComponents: function() {
                        this.setContent(OB.I18N.getDateFormatLabel());
                    },
                    style: 'width: 100px; color:gray;  margin: 0px 0px 8px 5px;'
                }]
            }, {
                kind: 'enyo.Input',
                name: 'endDate',
                size: '10',
                type: 'text',
                style: 'width: 100px;  margin: 0px 0px 8px 50px;',
                onchange: 'searchAction'
            }, {
                style: 'display: table-cell;',
                components: [{
                    tag: 'h4',
                    initComponents: function() {
                        this.setContent(OB.I18N.getDateFormatLabel());
                    },
                    style: 'width: 100px; color:gray;  margin: 0px 0px 8px 5px;'
                }]
            }]
        }]
    }],
    showValidationErrors: function(stDate, endDate) {
        var me = this;
        if (stDate === false) {
            this.$.startDate.addClass('error');
            setTimeout(function() {
                me.$.startDate.removeClass('error');
            }, 5000);
        }
        if (endDate === false) {
            this.$.endDate.addClass('error');
            setTimeout(function() {
                me.$.endDate.removeClass('error');
            }, 5000);
        }
    },
    disableFilterText: function(value) {
        this.$.filterText.setDisabled(value);
    },
    disableFilterButtons: function(value) {
        this.$.searchButton.setDisabled(value);
        this.$.clearButton.setDisabled(value);
    },
    clearAction: function() {
        if (!this.$.filterText.disabled) {
            this.$.filterText.setValue('');
        }
        this.$.startDate.setValue('');
        this.$.endDate.setValue('');
        this.doClearAction();
    },

    getDateFilters: function() {
        var startDate, endDate, startDateValidated = true,
            endDateValidated = true,
            formattedStartDate = '',
            formattedEndDate = '';
        startDate = this.$.startDate.getValue();
        endDate = this.$.endDate.getValue();

        if (startDate !== '') {
            startDateValidated = OB.Utilities.Date.OBToJS(startDate, OB.Format.date);
            if (startDateValidated) {
                formattedStartDate = OB.Utilities.Date.JSToOB(
                    startDateValidated, 'yyyy-MM-dd');
            }
        }

        if (endDate !== '') {
            endDateValidated = OB.Utilities.Date.OBToJS(endDate, OB.Format.date);
            if (endDateValidated) {
                formattedEndDate = OB.Utilities.Date.JSToOB(
                    endDateValidated, 'yyyy-MM-dd');
            }
        }




         if (
      startDate !== '' &&
      startDateValidated &&
      endDate !== '' &&
      endDateValidated
    ) {
      if (moment(endDateValidated).diff(moment(startDateValidated)) < 0) {
        endDateValidated = null;
        startDateValidated = null;
      }
    }
   if (startDateValidated === null || endDateValidated === null) {
      this.showValidationErrors(
        startDateValidated !== null,
        endDateValidated !== null
      );
      return false;
    }
    this.$.startDate.removeClass('error');
    this.$.endDate.removeClass('error');

    this.filters = _.extend(this.filters, {
      startDate: formattedStartDate,
      endDate: formattedEndDate
    });

    return true;
  },

  searchAction: function() {

        this.filters = {
            documentType: [OB.MobileApp.model.get('terminal').terminalType.documentType],
            filterText: this.$.filterText.getValue(),
            pos: OB.MobileApp.model.get('terminal').id,
            client: OB.MobileApp.model.get('terminal').client,
            organization: OB.MobileApp.model.get('terminal').organization
        };

        if (!this.getDateFilters()) {
            return true;
        }

        this.doSearchAction({
            filters: this.filters
        });
        return true;
    }
});

/*items of collection*/
enyo.kind({
    name: 'CUSTSHA.UI.ListPRsLine',
    kind: 'OB.UI.listItemButton',
    allowHtml: true,
    events: {
        onHideThisPopup: ''
    },
    tap: function() {
        this.inherited(arguments);
        this.doHideThisPopup();
    },
    components: [
        {
            name: 'line',
            style: 'line-height: 23px;',
            components: [
                {
                    name: 'topLine'
                },
                {
                    style: 'color: #888888',
                    name: 'bottonLine'
                },
                {
                    style: 'clear: both;'
                }
            ]
        }
    ],
    create: function() {
        var me = this;
        var returnLabel = '';
        this.inherited(arguments);
        if (
            this.model.get('documentTypeId') ===
            OB.MobileApp.model.get('terminal').terminalType.documentTypeForReturns
        ) {
            this.model.set(
                'totalamount',
                OB.DEC.mul(this.model.get('totalamount'), -1)
            );
            returnLabel = ' (' + OB.I18N.getLabel('OBPOS_ToReturn') + ')';
        }
        this.$.topLine.setContent(
            this.model.get('documentNo') +
            ' - ' +
            this.model.get('businessPartner') +
            returnLabel
        );
        this.$.bottonLine.setContent(
            this.model.get('totalamount') +
            ' (' +
            this.model.get('orderDate').substring(0, 10) +
            ') '
        );

        OB.UTIL.HookManager.executeHooks(
            'OBPOS_RenderSelectorLine',
            {
                selectorLine: this
            },
            function(args) {
                me.render();
            }
        );
    }
});
/*scrollable table (body of modal)*/
enyo.kind({
    name: 'CUSTSHA.UI.ListPRs',
    classes: 'row-fluid',
    handlers: {
        onSearchAction: 'searchAction',
        onClearAction: 'clearAction'
    },
    events: {
        onChangePaidReceipt: '',
        onShowPopup: '',
        onAddProduct: '',
        onHideThisPopup: '',
        onChangeCurrentOrder: ''
    },
    components: [{
        classes: 'span12',
        components: [{
            style: 'border-bottom: 1px solid #cccccc;',
            classes: 'row-fluid',
            components: [{
                classes: 'span12',
                components: [{
                    name: 'prslistitemprinter',
                    kind: 'OB.UI.ScrollableTable',
                    scrollAreaMaxHeight: '350px',
                    renderHeader: 'CUSTSHA.UI.ModalPRScrollableHeader',
                    renderLine: 'CUSTSHA.UI.ListPRsLine',
                    renderEmpty: 'OB.UI.RenderEmpty'
                }, {
                    name: 'renderLoading',
                    style: 'border-bottom: 1px solid #cccccc; padding: 20px; text-align: center; font-weight: bold; font-size: 30px; color: #cccccc',
                    showing: false,
                    initComponents: function() {
                        this.setContent(OB.I18N.getLabel('OBPOS_LblLoading'));
                    }
                }]
            }]
        }]
    }],
    clearAction: function() {
        this.prsList.reset();
        return true;
    },
    searchAction: function(inSender, inEvent) {
        var me = this,
            ordersLoaded = [],
            existingOrders = [],
            process = new OB.DS.Process(
                'com.openbravo.sharaf.retail.customdevelopments.service.CaptureOriginalInvoiceDocSearch'
            );
        me.filters = inEvent.filters;
        this.clearAction();
        this.$.prslistitemprinter.$.tempty.hide();
        this.$.prslistitemprinter.$.tbody.hide();
        this.$.prslistitemprinter.$.tlimit.hide();
        this.$.renderLoading.show();
        this.$.prslistitemprinter.$.theader.$.modalPRScrollableHeader.disableFilterButtons(true);
        var limit;      
        if (!OB.MobileApp.model.hasPermission('OBPOS_remote.order', true)) {
            limit = OB.Model.Order.prototype.dataLimit;
        } else {
            limit = OB.Model.Order.prototype.remoteDataLimit ? OB.Model.Order.prototype.remoteDataLimit : OB.Model.Order.prototype.dataLimit;
        }
        for (i = 0; i < this.model.get('orderList').length; i++) {
            // When an canceller order is ready, the cancelled order cannot be opened
            if (this.model.get('orderList').models[i].get('replacedorder')) {
                existingOrders.push(
                    this.model.get('orderList').models[i].get('replacedorder'));
            }
        }
        if (OB.MobileApp.model.hasPermission('OBPOS_orderLimit', true)) {
            limit = OB.DEC.abs(
                OB.MobileApp.model.hasPermission('OBPOS_orderLimit', true));
            OB.Model.Order.prototype.tempDataLimit = OB.DEC.abs(
                OB.MobileApp.model.hasPermission('OBPOS_orderLimit', true));
        }
        process.exec({
            filters: me.filters,
            _limit: limit + 1,
            _dateFormat: OB.Format.date,
        }, function(data) {
            if (data) {
                if (data.exception) {
                    me.$.prslistitemprinter.$.theader.$.modalPRScrollableHeader.disableFilterButtons(
                        false);
                    me.$.renderLoading.hide();
                    me.prsList.reset();
                    OB.UTIL.showConfirmation.display(
                        OB.I18N.getLabel('OBMOBC_Error'), data.exception.message ? data.exception.message : OB.I18N.getLabel('OBMOBC_OfflineWindowRequiresOnline'));
                    return;
                }
                ordersLoaded = [];
                _.each(data, function(iter) {
                    me.model.get('orderList').newDynamicOrder(iter, function(order) {
                        if (existingOrders.indexOf(order.id) === -1) {
                            // Only push the order if not exists in previous receipts
                            ordersLoaded.push(order);
                        }
                    });
                });
                me.prsList.reset(ordersLoaded);
                me.$.renderLoading.hide();
                if (data && data.length > 0) {
                    me.$.prslistitemprinter.$.tbody.show();
                } else {
                    me.$.prslistitemprinter.$.tempty.show();
                }
                me.$.prslistitemprinter.getScrollArea().scrollToTop();
            } else {
                OB.UTIL.showError(OB.I18N.getLabel('OBPOS_MsgErrorDropDep'));
            }
            me.$.prslistitemprinter.$.theader.$.modalPRScrollableHeader.disableFilterButtons(
                false);
        }, function(error) {
            me.$.prslistitemprinter.$.theader.$.modalPRScrollableHeader.disableFilterButtons(
                false);
            me.$.renderLoading.hide();
            me.prsList.reset();
            OB.UTIL.showConfirmation.display(
                OB.I18N.getLabel('OBMOBC_Error'), error && error.exception && error.exception.message ? error.exception.message : OB.I18N.getLabel('OBMOBC_OfflineWindowRequiresOnline'));
            return;
        }, true, 0);
        return true;
    },
    prsList: null,
  init: function(model) {
        var me = this;
        this.model = model;
        this.prsList = new Backbone.Collection();
        this.$.prslistitemprinter.setCollection(this.prsList);
        this.prsList.on(
      'click',
      function(model) {
              var process = new OB.DS.Process(
        'com.openbravo.sharaf.retail.customdevelopments.service.CaptureOriginalInvoiceDetails'
      );
      process.exec({
              orderid:model.get('id'),
      }, function(data) { 
          if (data && !data.exception) {
             var lineIdmap = new Map(),
                 availableLines = [],
                 count = 1;
             if(data.data.length > 0 ) {
              OB.MobileApp.view.waterfallDown('onShowPopup', {
                    popup: 'CUSTSHA.UI.ModalCaptureInvoiceDetails',
                    args: {
                         order: data.data,
                         line: data.data,
                         lineQueue : lineQueue,
                        callback: function(args) {
                          OB.CUSTSHA.checkOriginalInvoiceLines('',OB.MobileApp.model.receipt.get('lines').models,args);
                        }
                    }

                });
            } else {
              OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTSHA_OriginalInvoiceIssuedError'), [{
                  label: OB.I18N.getLabel('OBMOBC_LblOk'),
                  isConfirmButton: true,
                  action: function () {
                     me.doHideThisPopup();
                     OB.CUSTSHA.checkOriginalInvoiceLines('',OB.MobileApp.model.receipt.get('lines').models,me);
                      
                  }
                }]);
           }
         } else {
             OB.UTIL.showConfirmation.display('',OBMOBC_LblError);
          }
       });
      },
      this
    );
    }
});
/*Modal definiton*/
enyo.kind({
    name: 'CUSTSHA.UI.ModalPaidReceiptsCaptureOriginalInvoice',
    kind: 'OB.UI.Modal',
    topPosition: '125px',
    i18nHeader: 'OBPOS_LblPaidReceipts',
    published: {
        params: null
    },
    body: {
        kind: 'CUSTSHA.UI.ListPRs'
    },
    handlers: {
        onChangePaidReceipt: 'changePaidReceipt'
    },
    changePaidReceipt: function(inSender, inEvent) {
        this.model.get('orderList').addPaidReceipt(inEvent.newPaidReceipt);
        if (inEvent.newPaidReceipt.get('isLayaway')) {
            this.model.attributes.order.calculateReceipt();
        } else {
            this.model.attributes.order.calculateGrossAndSave(false);
        }
        return true;
    },
    executeOnShow: function() {
        this.$.header.setContent('Receipts');
        lineQueue = this.args;
        this.$.body.$.listPRs.$.prslistitemprinter.$.theader.$.modalPRScrollableHeader.disableFilterText(false);
        this.$.body.$.listPRs.$.prslistitemprinter.$.theader.$.modalPRScrollableHeader.clearAction();
    },
    executeOnHide: function() {
        
    },
    init: function(model) {
        this.model = model;
    }
});
OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
    kind: 'CUSTSHA.UI.ModalPaidReceiptsCaptureOriginalInvoice',
    name: 'CUSTSHA.UI.ModalPaidReceiptsCaptureOriginalInvoice'
});