/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, _ */

enyo.kind({
  name: 'CUSTSHA.UI.CheckboxButton',
  kind: 'OB.UI.CheckboxButton',
  classes: 'modal-dialog-btn-check span1',
  style: 'width: 8%',
  handlers: {
    onCheckAll: 'checkAll'
  },
  checkAll: function (inSender, inEvent) {
    if (inEvent.checked) {
      this.check();
    } else {
      this.unCheck();
    }
  }
});

enyo.kind({
  name: 'CUSTSHA.UI.EditOrderLine',
  style: 'border-bottom: 1px solid #cccccc; text-align: center; color: black; padding-top: 9px;',
  handlers: {
    onApplyChange: 'applyChange'
  },
  components: [{
    kind: 'CUSTSHA.UI.CheckboxButton',
    name: 'checkboxButton'
  }, {
    name: 'product',
    classes: 'span4',
    style: 'line-height: 35px; font-size: 17px; width: 325px; text-align: left; padding-left: 10px;'
  }, {
    name: 'quantity',
    classes: 'span2',
    style: 'line-height: 35px; font-size: 17px; width:55px; text-align: right;'
  }, {
    name: 'price',
    classes: 'span2',
    style: 'line-height: 35px; font-size: 17px; width: 110px; text-align: right; padding-right: 10px;'
  }, {
    style: 'clear: both;'
  }],
  initComponents: function () {
    this.inherited(arguments);
    this.$.product.setContent(this.newAttribute.get('product').get('_identifier'));
    this.$.quantity.setContent(this.newAttribute.get('qty'));
    this.$.price.setContent(OB.I18N.formatCurrency(this.newAttribute.get('gross')));
  }
});

enyo.kind({
  name: 'CUSTSHA.UI.ReturnLinesPopup',
  kind: 'OB.UI.ModalAction',
  autoDismiss: false,
  closeOnEscKey: false,
  handlers: {
    onApplyChanges: 'applyChanges',
    onCheckedAll: 'checkedAll'
  },
  bodyContent: {
    kind: 'Scroller',
    maxHeight: '225px',
    style: 'background-color: #ffffff;margin-top: -7px;',
    thumb: true,
    horizontal: 'hidden',
    components: [{
      name: 'attributes'
    }]
  },
  bodyButtons: {
    components: [{
      kind: 'CUSTSHA.UI.ReturnsApply'
    }, {
      kind: 'OB.UI.CancelDialogButton'
    }]
  },
  checkedAll: function (inSender, inEvent) {
    this.waterfall('onCheckAll', {
      checked: inEvent.checked
    });
    return true;
  },
  selectedLines: [],
  applyChanges: function (inSender, inEvent) {
    var i;
    for (i = 0; i < this.numberOfLines; i++) {
      if (this.$.bodyContent.$.attributes.$['line_' + i].$.checkboxButton.checked) {
        this.selectedLines.push(this.args.negativeLines[i].get('id'));
      }
    }
    return true;
  },
  executeOnHide: function () {
    this.args.callback(this.selectedLines);
  },
  executeOnShow: function () {
    this.selectedLines = [];
    this.$.bodyContent.$.attributes.destroyComponents();
    this.$.header.destroyComponents();
    this.$.header.createComponent({
      name: 'checkAllHeader',
      style: 'text-align: center; color: white;',
      components: [{
        classes: 'span12',
        style: 'line-height: 5px; font-size: 24px;',
        content: OB.I18N.getLabel('CUSTSHA_SelectLines')
      }, {
        style: 'clear: both;'
      }]
    });
    if (!this.$.header.$.checkboxButtonAll) {
      this.$.header.addStyles('padding-bottom: 0px; margin: 0px; height: 102px;');

      this.$.header.createComponent({
        name: 'CheckAllHeader',
        style: 'overflow: hidden; padding-top: 5px; border-bottom: 3px solid #cccccc; text-align: center; color: black; margin-top: 30px; padding-top: 7px; padding-bottom: 7px;  font-weight: bold; background-color: white; height:46px;',
        components: [{
          kind: 'OB.UI.CheckboxButtonAll',
          name: 'checkboxButtonAll'
        }, {
          content: OB.I18N.getLabel('OBRETUR_LblProductName'),
          name: 'productNameLbl',
          classes: 'span4',
          style: 'line-height: 25px; font-size: 17px;  width: 325px; padding-top: 10px; text-align: left; padding-left: 10px;'
        }, {
          content: OB.I18N.getLabel('OBRETUR_LblQty'),
          name: 'qtyLbl',
          classes: 'span3',
          style: 'line-height: 25px; font-size: 17px; width: 55px; padding-top: 10px; text-align: right;'
        }, {
          content: OB.I18N.getLabel('OBRETUR_LblPrice'),
          name: 'priceLbl',
          classes: 'span2',
          style: 'line-height: 25px; font-size: 17px; width: 110px; padding-top: 10px; text-align: right; padding-right: 10px;'
        }, {
          style: 'clear: both;'
        }]
      });
    } else {
      this.$.header.$.checkboxButtonAll.unCheck();
    }
    this.$.header.render();

    this.$.bodyContent.$.attributes.destroyComponents();
    this.numberOfLines = 0;
    enyo.forEach(this.args.negativeLines, function (line, indx) {
      var lineEnyoObject = this.$.bodyContent.$.attributes.createComponent({
        kind: 'CUSTSHA.UI.EditOrderLine',
        name: 'line_' + indx,
        newAttribute: line
      });
      this.numberOfLines += 1;
    }, this);

    this.$.bodyContent.$.attributes.render();
  },
  initComponents: function () {
    this.inherited(arguments);
  }
});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'CUSTSHA.UI.ReturnLinesPopup',
  name: 'CUSTSHA_UI_ReturnLinesPopup'
});