/**
 * 
 */
enyo.kind({
    kind: 'OB.UI.ModalAction',
    name: 'CUSTSHA.UI.CustomerFeedBackInfo',
    i18nLabel: 'CUSTSHA_LblCustomerFeedBack',
    hideCloseButton: false,
    autoDismiss: true,
    closeOnEscKey: true,

    handlers: {},

    bodyContent: {
        kind: 'Scroller',
        maxHeight: '225px',
        thumb: true,
        horizontal: 'hidden',
        components: [{
            name: 'attributes'
        }]
    },

    bodyButtons: {
        components: [{
            kind: 'OB.UI.customerfeedbackApply'
        },{
            kind: 'OB.UI.customerfeedbackClose'
        }],
    },

    executeOnHide: function() {

    },

    executeOnShow: function() {

    },

    applyChanges: function(inSender, inEvent) {

    },

    initComponents: function() {
        this.inherited(arguments);
        this.attributeContainer = this.$.bodyContent.$.attributes;
        enyo.forEach(this.newAttributes, function(natt) {
            this.$.bodyContent.$.attributes.createComponent({
                kind: 'OB.UI.PropertyEditLine',
                name: 'line_' + natt.name,
                newAttribute: natt
            });
        }, this);
        this.setHeader(OB.I18N.getLabel('CUSTSHA_LblCustomerFeedBack'));
    },
});

enyo.kind({
    name: 'CUSTSHA.UI.CustomerFeedBackInfo',
    kind: 'CUSTSHA.UI.CustomerFeedBackInfo',
    newAttributes: [{
        i18nLabel: 'CUSTSHA_ReasonsToVisit',
        kind: 'OB.UI.List',
        name: 'reasonsList',
        mandatory: true,
        tag: 'select',
        classes: 'combo',
        style: 'width: 101%; margin:0;',
        renderEmpty: enyo.Control,
        renderLine: enyo.kind({
            kind: 'enyo.Option',
            initComponents: function() {
                this.inherited(arguments);
                this.setValue(this.model.get('id'));
                this.setContent(this.model.get('_identifier'));

            }
        })

    }],
    executeOnHide: function() {

    },
    executeOnShow: function() {
        var reasonsList = new Backbone.Collection();
        var listOfReasons = this.args.args.receipt.get('customerFeedbackReasons');
        // Set the collection for the dropdown
        this.$.bodyContent.$.attributes.$.line_reasonsList.$.newAttribute.$.reasonsList.setCollection(reasonsList);

        // Reset the collection with an array of objects
        reasonsList.reset(listOfReasons.map(function(value, index) {
            return {
                id: index,
                _identifier: value
            };
        }));

    },

    applyChanges: function(inSender, inEvent) {

    },
});
enyo.kind({
    kind: 'OB.UI.ModalDialogButton',
    name: 'OB.UI.customerfeedbackApply',
    classes: 'obUiCustomerFeedbackApply',
    tap: function() {
    	var selectedIndex=this.owner.owner.$.bodyContent.$.attributes.$.line_reasonsList.$.newAttribute.$.reasonsList.getSelected();
    	var selecetdFeedback=this.owner.owner.$.bodyContent.$.attributes.$.line_reasonsList.$.newAttribute.$.reasonsList.collection.at(selectedIndex).get('_identifier');
    	this.owner.owner.args.args.receipt.set('custshaCustomerFeedback',selecetdFeedback);
        OB.MobileApp.model.receipt.save();
        this.doHideThisPopup();

    },
    initComponents: function() {
        this.inherited(arguments);
        this.setContent(OB.I18N.getLabel('OBMOBC_LblApply'));
    }

});

enyo.kind({
    kind: 'OB.UI.ModalDialogButton',
    name: 'OB.UI.customerfeedbackClose',
    classes: 'obUiCustomerFeedbackClose',
    tap: function() {
    	this.owner.owner.args.args.receipt.set('custshaCustomerFeedback',' ');
        OB.MobileApp.model.receipt.save();
        this.doHideThisPopup();
    },
    initComponents: function() {
        this.inherited(arguments);
        this.setContent(OB.I18N.getLabel('OBMOBC_LblCancel'));
    }

});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
    kind: 'CUSTSHA.UI.CustomerFeedBackInfo',
    name: 'CUSTSHA_UI_CustomerFeedBackInfo'
});