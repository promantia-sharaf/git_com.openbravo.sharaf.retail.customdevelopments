/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, _ */

enyo.kind({
  name: 'OB.OBPOSPointOfSale.UI.EditLine.SharafReturnsButton',
  kind: 'OB.UI.SmallButton',
  classes: 'btnlink-orange',
  handlers: {
    onSetMultiSelected: 'setMultiSelected'
  },
  tap: function () {
    var me = this;
    OB.MobileApp.view.waterfall('onShowPopup', {
      popup: 'CUSTSHA_UI_ReturnsPopup',
      args: {
        selectedModels: me.owner.owner.selectedModels
      }
    });
  },
  setMultiSelected: function (inSender, inEvent) {
    var me = this,
        nLines = 0,
        linesSelected = inEvent.models.length;
    _.each(inEvent.models, function (line) {
      if (line && line.get('qty') < 0) {
        nLines = nLines + 1;
      }
    });
    if (nLines === linesSelected) {
      me.setShowing(true);
    } else {
      me.setShowing(false);
    }
    return;
  },
  init: function (model) {
    this.model = model;
  },
  initComponents: function () {
    this.inherited(arguments);
    this.setContent(OB.I18N.getLabel('CUSTSHA_Returns'));
    if (OB.MobileApp.model.hasPermission('CUSTSHA_ReturnsButton', true)) {
      this.show();
    } else {
      this.hide();
    }
  }
});

OB.OBPOSPointOfSale.UI.EditLine.prototype.actionButtons.push({
  kind: 'OB.OBPOSPointOfSale.UI.EditLine.SharafReturnsButton',
  name: 'sharafReturnsButton'
});