/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global */

enyo.kind({
	name: 'CUSTSHA.UI.ThresholdForceCashUpPopUp',
	kind: 'OB.UI.ModalAction',
	hideCloseButton: true,
	autoDismiss: false,
	closeOnEscKey: false,
	i18nHeader: '',
	handlers: {

	},
	bodyContent: {
		kind: 'Scroller',
		maxHeight: '225px',
		thumb: true,
		horizontal: 'hidden',
		components: [{
			name: 'email'
		}, {
			kind: 'ThresholdCashupInformation',
		}]
	},
	bodyButtons: {
		components: [{
			kind: 'CUSTSHA.UI.ThresholdCashupInfoOk'
		}]
	},
	executeOnHide: function() {
		OB.POS.navigate('retail.cashup');
		return true;
	},
	executeOnShow: function() {
		this.$.bodyContent.$.email.setContent(OB.I18N.getLabel('CUSTSHA_CashupEnableThreshold',[
            OB.MobileApp.model.attributes.permissions.CUSTSHA_CashupThresholdLimit
          ])
          );
	},
	applyChanges: function(inSender, inEvent) {

	},
	initComponents: function() {
		this.inherited(arguments);
		this.attributeContainer = this.$.bodyContent.$.attributes;
	}
});

enyo.kind({
	name: 'ThresholdCashupInformation',
	components: [{

	}],

	init: function() {
		this.inherited(arguments);
	}
});


enyo.kind({
	kind: 'OB.UI.ModalDialogButton',
	name: 'CUSTSHA.UI.ThresholdCashupInfoOk',
	tap: function() {
		var me = this;
		me.doHideThisPopup();
	},
	initComponents: function() {
		this.inherited(arguments);
		this.setContent(OB.I18N.getLabel('OBPOS_LblOk'));
	}
});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
	kind: 'CUSTSHA.UI.ThresholdForceCashUpPopUp',
	name: 'CUSTSHA_UI_ThresholdForceCashUpPopUp'
});