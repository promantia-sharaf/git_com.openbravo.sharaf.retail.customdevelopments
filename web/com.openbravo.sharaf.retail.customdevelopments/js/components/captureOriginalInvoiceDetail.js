
/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

enyo.kind({
  kind: 'OB.UI.ModalAction',
  name: 'CUSTSHA.UI.ModalCaptureInvoiceDetails',
  topPosition: '50px',
  style: 'height:550px;',
  hideCloseButton: true,
  autoDismiss: false,
  closeOnEscKey: false,
  i18nHeader: 'CUSTSHA_OriginalInvoiceHeader',
  events: {
    onHideThisPopup: '',
    onShowPopup: ''
  },
  bodyContent: {
    components: [{
      style: 'border-bottom: 1px solid #cccccc; text-align:center; margin-bottom: 10px;',
      name: 'invoiceDetail'
    }, {
        kind: 'articleCode',
    },{
      kind: 'InvoNo',
    }, {
      kind: 'sNo',
    }, {
      kind: 'lineno',
    },{
      kind: 'Imei'
    }]
  },
  bodyButtons: {
    components: [{
      kind: 'OB.UI.ModalDialogButton',
      name: 'ok',
      isDefaultAction: true,
      i18nContent: 'OBMOBC_LblApply',
      tap: function () {
        this.owner.owner.actionApply();
      }
    }]
  },
   actionApply: function () { 
    var me = this; 
    var validate = function setProperties(temp) {
      var spaceCheck = false;
      temp = temp.replace(/^\s+|\s+$/g, '');
        var CharArray = temp.split(' ');
        if (CharArray.length > 2) {
            spaceCheck = true;
        }
     return spaceCheck;
    }
    
    var lineQueue = this.args.lineQueue,
         lineId,alreadyused = true,
          articlecode,orgline; 

     for (var i = 0; i < OB.MobileApp.model.receipt.get('lines').models.length; i++) {
      var line = OB.MobileApp.model.receipt.get('lines').models[i];
      var searchkey =  !OB.UTIL.isNullOrUndefined(lineQueue.lineQueue.line) ? 
                       lineQueue.lineQueue.line.id : lineQueue.lineQueue.id;
      if (line.get('id') === searchkey) {
        articlecode = line.get('product').get('searchkey');
        orgline = line;
          }   
     }
    var oldInvoiceValues = this.$.bodyContent.$.articleCode.$.productNameInputList.collection.models[this.$.bodyContent.$.articleCode.$.productNameInputList.getSelected()].attributes.id.split(','),
       invoiceno = this.$.bodyContent.$.invoNo.$.InvoNoInput.getValue().trim(),
       serialno = this.$.bodyContent.$.sNo.$.sNoInput.getValue().trim(),
       imeino = this.$.bodyContent.$.imei.$.ImeiNoInput.getValue().trim(),
       selectedproduct = this.$.bodyContent.$.articleCode.$.productNameInputList.getSelected(),
       lineno = this.$.bodyContent.$.lineno.$.lineNoInput.getValue();
    for (var i = 0; i < OB.MobileApp.model.receipt.get('lines').models.length; i++) {
        var line = OB.MobileApp.model.receipt.get('lines').models[i];
       if(!OB.UTIL.isNullOrUndefined(line.get('custshaOriginvoiceno')) 
                          && line.get('custshaOriginvoiceno') ===  invoiceno
                          && line.get('custshaOriginvoicelineno') ===  lineno) {
         
         alreadyused = false;
       } 
    }
          
  if(validate(invoiceno) || OB.UTIL.isNullOrUndefined(invoiceno) || invoiceno  === '' || invoiceno.length < 16  || invoiceno.length > 16) {
     OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTSHA_OriginalInvoiceError'));
     this.$.bodyContent.$.invoNo.$.InvoNoInput.setValue('');
     return false; 
  } else if(selectedproduct === 0) {
     OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTSHA_OriginalInvoiceArticlecodeError'));
     return false;
  } else if(validate(imeino) || OB.UTIL.isNullOrUndefined(imeino) || imeino  === '') {
     OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTSHA_OriginalInvoiceimeinoError'));
     this.$.bodyContent.$.imei.$.ImeiNoInput.setValue('');
     return false;
 }else if(!alreadyused) {
     if(this.$.bodyContent.$.articleCode.$.productNameInputList.collection.length > 2) {
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), 'Please check Article Code. Original Invoice already applied for this Article Code.', [{
          label: OB.I18N.getLabel('OBMOBC_LblOk'),
          isConfirmButton: true,
          action: function () {
            return false;
          }
        }]);

     } else {
          OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), 'Please check Article Code. Original Invoice already applied for this Article Code.', [{
          label: OB.I18N.getLabel('OBMOBC_LblOk'),
          isConfirmButton: true,
          action: function () {
             me.doHideThisPopup();
             if (me.args.callback) {
                    me.args.callback(me.args.order,OB.MobileApp.model.receipt.get('lines').models);
             }
          }
        }]);
     }
  }else if (orgline) {
        orgline.set('custshaOriginvoiceno', invoiceno);
        orgline.set('custshaOriginvoiceserialno', serialno);
        orgline.set('custshaOriginvoiceimeino', imeino);
        orgline.set('custshaOriginvarticlecode', articlecode);
        orgline.set('custshaOriginvoicelineno', lineno);
        orgline.set('captureoriginalinvoice', true);
        OB.MobileApp.model.receipt.save();
        me.doHideThisPopup();
       if (this.args.callback) {
              this.args.callback(me.args.order,OB.MobileApp.model.receipt.get('lines').models);
       }
   }
  
  },
 executeOnShow: function () {
    var productList = new Backbone.Collection();
    var product = !OB.UTIL.isNullOrUndefined (this.args.lineQueue.lineQueue.line) ? 
                   this.args.lineQueue.lineQueue.line.get('product').get('description') : this.args.lineQueue.lineQueue.get('product').get('description'); 
    var oriInvNo = this.args.line.length > 0 ? this.args.line[0].originalInvoiceNo : '';
    this.args.line.unshift('');
    this.$.bodyContent.$.articleCode.$.productNameInputList.setCollection(productList);
      productList.reset(this.args.line.map(function (value, index) {
            return {
                 id: index +',' + value.lineNo + ',' +  value.obposSerialNumber + ',' + value.lineId,
                _identifier: value.productname
            };
        }));
    this.$.bodyContent.$.invoiceDetail.setContent(product);  
    this.$.bodyContent.$.invoNo.$.InvoNoInput.setValue(oriInvNo);
    this.$.bodyContent.$.invoNo.$.InvoNoInput.setAttribute('readonly', 'readonly');
    this.$.bodyContent.$.sNo.$.sNoInput.setValue('');
    this.$.bodyContent.$.sNo.$.sNoInput.setAttribute('readonly', 'readonly');
    this.$.bodyContent.$.lineno.$.lineNoInput.setValue('');
    this.$.bodyContent.$.lineno.$.lineNoInput.setAttribute('readonly', 'readonly');
    this.$.bodyContent.$.imei.$.ImeiNoInput.setValue('');
  },
  init: function () {
    this.inherited(arguments);
  }
});

enyo.kind({
   name: 'InvoNo',
   style: 'text-align: center;height: 60px;margin-left:5%;margin-right:5%;padding-bottom: 8px;',
   components: [{
     name: 'Inv',
     style: 'color:white;font-size:20px;font-weight:bold;'
   }, {
     name: 'InvoNoInput',
     kind: 'enyo.Input',
     style: 'margin-top:10px;height: 25px;width: 80%;',
     readOnly : true
   }],
   focus: function () {
   },
   init: function () {
     this.inherited(arguments);
     this.$.Inv.setContent(OB.I18N.getLabel('CUSTSHA_OriginalInvoiceNumber'));
   }
});
/*scrollable table (body of modal)*/

enyo.kind({
  name: 'articleCode',
  style: 'text-align: center;height: 60px;margin-left:5%;margin-right:5%;padding-bottom: 8px;',
  components: [{
    name: 'articalCodePname',
    style: 'color:white;font-size:20px;font-weight:bold;'
  }, {
   kind: 'OB.UI.List',
   name: 'productNameInputList',
   classes : 'combo',
   handlers: {
    onchange: 'change'
   },
   style: 'margin-top:10px;height: 30px;width: 80%;',   
   renderEmpty : enyo.Control,
   change: function() {
       var value = this.getValue(),
         lineno = value.split(',')[1];
        if(value.split(',')[2] === 'undefined') {
          this.parent.parent.children[3].$.sNoInput.setValue(''); 
        }else {
         this.parent.parent.children[3].$.sNoInput.setValue(value.split(',')[2]);
        }
         if(parseInt(value.split(',')[0]) === 0) {
           this.parent.parent.children[4].$.lineNoInput.setValue('');
         } else {
           this.parent.parent.children[4].$.lineNoInput.setValue(lineno);
         }
           this.parent.parent.children[3].$.sNoInput.setAttribute('readonly', 'readonly');
           this.parent.parent.children[4].$.lineNoInput.setAttribute('readonly', 'readonly');
           this.parent.parent.children[5].$.ImeiNoInput.setValue('');
        },
           renderLine : enyo.kind({
                kind : 'enyo.Option',
                initComponents : function() {
                    this.inherited(arguments);
                    this.setValue(this.model.get('id'));
                    this.setContent(this.model.get('_identifier'));
                }
           })
     }
   ],
  
  init: function () {
    this.inherited(arguments);
    this.$.articalCodePname.setContent(OB.I18N.getLabel('CUSTSHA_ArticleCode'));
  }
});

enyo.kind({
  name: 'sNo',
  style: 'text-align: center;height: 60px;margin-left:5%;margin-right:5%;padding-bottom: 8px;',
  components: [{
    name: 'serial',
    style: 'color:white;font-size:20px;font-weight:bold;'
  }, {
    name: 'sNoInput',
    kind: 'enyo.Input',
    style: 'margin-top:10px;height: 25px;width: 80%;',
    readOnly : true
  }],
  init: function () {
    this.inherited(arguments);
    this.$.serial.setContent(OB.I18N.getLabel('CUSTSHA_OriginalInvoiceSerialNo'));
  }
});

enyo.kind({
  name: 'lineno',
  style: 'text-align: center;height: 60px;margin-left:5%;margin-right:5%;padding-bottom: 8px;',
  components: [{
    name: 'lineNo',
    style: 'color:white;font-size:20px;font-weight:bold;'
  }, {
    name: 'lineNoInput',
    kind: 'enyo.Input',
    style: 'margin-top:10px;height: 25px;width: 80%;',
    readOnly : true
  }],
  initComponents: function () {
    this.inherited(arguments);
    this.$.lineNo.setContent(OB.I18N.getLabel('CUSTSHA_OriginalInvoicelineNo'));
  }
});

enyo.kind({
  name: 'Imei',
  style: 'text-align: center;height: 60px;margin-left:5%;margin-right:5%;padding-bottom: 8px;',
  components: [{
    name: 'ImeiNo',
    style: 'color:white;font-size:20px;font-weight:bold;'
  }, {
    name: 'ImeiNoInput',
    kind: 'enyo.Input',
    style: 'margin-top:10px;height: 25px;width: 80%;',
    readOnly : false
  }],
  initComponents: function () {
    this.inherited(arguments);
    this.$.ImeiNo.setContent(OB.I18N.getLabel('CUSTSHA_OriginalInvoiceIMEI'));
  }
});
OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
  kind: 'CUSTSHA.UI.ModalCaptureInvoiceDetails',
  name: 'CUSTSHA.UI.ModalCaptureInvoiceDetails'
});