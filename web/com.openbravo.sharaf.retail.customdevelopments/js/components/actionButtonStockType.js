/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone */

enyo.kind({
  name: 'OB.OBPOSPointOfSale.UI.EditLine.SharafStockTypeButton',
  kind: 'OB.UI.SmallButton',
  classes: 'btnlink-orange',
  tap: function () {
    var me = this;
    OB.MobileApp.view.waterfall('onShowPopup', {
      popup: 'CUSTSHA_UI_StockTypePopup',
      args: {
        selectedModels: me.owner.owner.selectedModels
      }
    });
  },
  init: function (model) {
    this.model = model;
    this.model.get('order').on('change:isPaid change:isEditable', function (newValue) {
      if (newValue) {
        if (newValue.get('isPaid') === true || newValue.get('isEditable') === false) {
          this.setShowing(false);
          return;
        }
      }
      this.setShowing(true);
    }, this);
  },
  initComponents: function () {
    this.inherited(arguments);
    this.setContent(OB.I18N.getLabel('CUSTSHA_StockType'));
    if (OB.MobileApp.model.hasPermission('CUSTSHA_StockTypeButton', true)) {
      this.show();
    } else {
      this.hide();
    }
  }
});

OB.OBPOSPointOfSale.UI.EditLine.prototype.actionButtons.push({
  kind: 'OB.OBPOSPointOfSale.UI.EditLine.SharafStockTypeButton',
  name: 'sharafStockTypeButton'
});