/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */


(function () {
  OB.UI.ModalReceiptPropertiesImpl.prototype.newAttributes.unshift({
    kind: 'OB.UI.renderTextProperty',
    name: 'customerAddress',
    modelProperty: 'custshaCustomerAddress',
    i18nLabel: 'CUSTSHA_CustomerAddress',
    maxLength: 255
  });
  
  OB.UI.ModalReceiptPropertiesImpl.prototype.newAttributes.push({
	  kind: 'OB.UI.CustomerTextProperty',
	  name: 'customerRPLatLng',
	  modelPropertyLat: 'custshaLatitude',
	  modelPropertyLng: 'custshaLongitude',
	  placeholder: 'Click here to select location on the map',
	  i18nLabel: 'CUSTSHA_Location',
	  loadValue: function(inSender, inEvent) {
	    if (!OB.UTIL.isNullOrUndefined(inEvent.model) && !OB.UTIL.isNullOrUndefined(inEvent.model.get('custshaLongitude'))
             && !OB.UTIL.isNullOrUndefined(inEvent.model.get('custshaLatitude'))) {
           this.setValue('Lat: ' + inEvent.model.get('custshaLatitude') + ' / Lng:' + inEvent.model.get('custshaLongitude'));
        } else {
           this.setValue('');
        }
	  },
	  tap: function () {
		var me = this;
		var setLatLng = function (lat, lng) {			
			me.setValue("Lat: " + lat + " / Lng:" + lng);
			if (lat && lng && lat !== ' - ' && lng !== ' - ') {
			  me.modelPropertyLat = lat;
			  me.modelPropertyLng = lng;
			}
		}
		OB.MobileApp.view.waterfallDown('onShowPopup', {
	       popup: 'OB_UI_ModalGoogleMaps',
	          args:{
	               args: me.getValue(),
	               callbacks: null,
	               setLatLng: setLatLng
	          }
	    });
	  } 
  });
  
  OB.UI.ModalReceiptPropertiesImpl.prototype.newAttributes.unshift({
      kind: 'OB.UI.CustomerAddrComboPropertyNew',
      name: 'customerAddrCountry',
      modelProperty: 'mobileNoEntered',
      i18nLabel: 'CUSTSHA_CountryCode',
      maxLength: 60,
      mandatory: true,
      collectionName: 'CountryList',
      defaultValue: function () {
        //this.owner.owner.owner.$.line_customerAddrCountry.setStyle('float:left; width: 100%');
        return OB.MobileApp.model.get('terminal').defaultbp_bpcountry;
      },
      //Default value for new lines
      retrievedPropertyForValue: 'id',
      //property of the retrieved model to get the value of the combo item
      retrievedPropertyForText: '_identifier',
      fetchDataFunction: function (args) {
        var me = this,
            criteria;
        criteria = {
          _orderByClause: '_identifier asc'
        };
        OB.Dal.find(OB.Model.Country, criteria, function (data, args) {
          me.dataReadyFunction(data, args);
        }, function (error) {
          OB.UTIL.showError(OB.I18N.getLabel('OBPOS_ErrorGettingCountries'));
          me.dataReadyFunction(null, args);
        }, args);
      },
      displayLogic: function () {
        return OB.MobileApp.model.get('terminal').bp_showcategoryselector;
      }
  });

  OB.UI.ModalReceiptPropertiesImpl.prototype.newAttributes.unshift({
    kind: 'OB.UI.renderTextProperty',
    name: 'customerEmail',
    modelProperty: 'custshaCustomerEmail',
    i18nLabel: 'CUSTSHA_CustomerEmail',
    maxLength: 255
  });

  OB.UI.ModalReceiptPropertiesImpl.prototype.newAttributes.unshift({
    kind: 'OB.UI.renderTextProperty',
    name: 'customerName',
    modelProperty: 'custshaCustomerName',
    i18nLabel: 'CUSTSHA_CustomerName',
    maxLength: 60
  });

  OB.UI.ModalReceiptPropertiesImpl.prototype.newAttributes.unshift({
    kind: 'OB.UI.renderTextProperty',
    name: 'WebsiteOrderReference',
    modelProperty: 'custshaWebsiterefno',
    i18nLabel: 'CUSTSHA_WebsiteOrderReference',
    maxLength: 60
  });

  OB.UI.ModalReceiptPropertiesImpl.extend({
	  autoDismiss: false,
	  executeOnShown: function () {
      var dtKey = this.model.get('custsdtDocumenttypeSearchKey'),
      endCustomer = false;
      
      var sharafDocArr = OB.MobileApp.model.get('sharafDocumentType');
      for (i = 0; i < sharafDocArr.length; i++) {
        if (sharafDocArr[i].searchKey === dtKey && sharafDocArr[i].shfptEndCustomer) {
          endCustomer = true;
        }
      }
      
      if (endCustomer) {
        this.$.bodyContent.$.attributes.$.line_customerName.show();
        this.$.bodyContent.$.attributes.$.line_customerEmail.show();
        this.$.bodyContent.$.attributes.$.line_customerAddress.show();
        this.$.bodyContent.$.attributes.$.line_customerAddrCountry.show();
        this.$.bodyContent.$.attributes.$.line_customerRPLatLng.show();
        if (this.model && this.model.get('custshaLatitude') && this.model.get('custshaLongitude')) {
	       this.$.bodyContent.$.attributes.$.line_customerRPLatLng.$.newAttribute.$.customerRPLatLng.setValue('Lat: ' + 
	         this.model.get('custshaLatitude') + ' / Lng:' + this.model.get('custshaLongitude'));
        } else {
	       this.$.bodyContent.$.attributes.$.line_customerRPLatLng.$.newAttribute.$.customerRPLatLng.setValue('');
        }
        if (this.model.get('custshaCustomerPhone') && this.model.get('custshaCustomerMobileNumber') && this.model.get('custshaCustomerCountryCode')) {
          this.$.bodyContent.$.attributes.$.line_customerAddrCountry.$.newAttribute.$.customerAddrCountry.$.customerAddrCombo.setSelected(this.model.get('custshaCustomerCountryCode'));
          this.$.bodyContent.$.attributes.$.line_customerAddrCountry.$.newAttribute.$.customerAddrCountry.$.mobileNoLength.setValue(this.model.get('custshaCustomerMobileNumber'));
          }
         } else if (this.model.get('generatedFromQuotation')) {
        this.$.bodyContent.$.attributes.$.line_WebsiteOrderReference.$.newAttribute.$.WebsiteOrderReference.setValue(this.model.get('custshaWebsiterefno'));
        this.$.bodyContent.$.attributes.$.line_customerName.hide();
        this.$.bodyContent.$.attributes.$.line_customerName.$.newAttribute.$.customerName.setValue(this.model.get('custshaCustomerName'));
        this.$.bodyContent.$.attributes.$.line_customerAddrCountry.hide();
        this.$.bodyContent.$.attributes.$.line_customerEmail.hide();
        this.$.bodyContent.$.attributes.$.line_customerEmail.$.newAttribute.$.customerEmail.setValue(this.model.get('custshaCustomerEmail'));
        this.$.bodyContent.$.attributes.$.line_customerAddress.hide();
        this.$.bodyContent.$.attributes.$.line_customerAddress.$.newAttribute.$.customerAddress.setValue(this.model.get('custshaCustomerAddress'));
        this.$.bodyContent.$.attributes.$.line_customerRPLatLng.hide();
        if (this.model.get('custshaCustomerPhone') && this.model.get('custshaCustomerMobileNumber') && this.model.get('custshaCustomerCountryCode')) {
          this.$.bodyContent.$.attributes.$.line_customerAddrCountry.$.newAttribute.$.customerAddrCountry.$.customerAddrCombo.setSelected(this.model.get('custshaCustomerCountryCode'));
          this.$.bodyContent.$.attributes.$.line_customerAddrCountry.$.newAttribute.$.customerAddrCountry.$.mobileNoLength.setValue(this.model.get('custshaCustomerMobileNumber'));
        }
      } else {
        this.$.bodyContent.$.attributes.$.line_customerName.hide();
        this.$.bodyContent.$.attributes.$.line_customerAddrCountry.hide();
        this.$.bodyContent.$.attributes.$.line_customerEmail.hide();
        this.$.bodyContent.$.attributes.$.line_customerAddress.hide();
        this.$.bodyContent.$.attributes.$.line_customerRPLatLng.hide();
      }
    }
  });
  
  OB.UI.ReceiptPropertiesDialogApply.extend({
    tap: function() {
    	var latitude = 0.0;
    	var langitude = 0.0;
	        if (!OB.UTIL.isNullOrUndefined(this.owner.owner.$.bodyContent.$.attributes.$.line_customerRPLatLng) && this.owner.owner.$.bodyContent.$.attributes.$.line_customerRPLatLng.showing) {
		        if (this.owner.owner.$.bodyContent.$.attributes.$.line_customerRPLatLng.$.newAttribute.$.customerRPLatLng.modelPropertyLat && 
		            this.owner.owner.$.bodyContent.$.attributes.$.line_customerRPLatLng.$.newAttribute.$.customerRPLatLng.modelPropertyLng && 
		            this.owner.owner.$.bodyContent.$.attributes.$.line_customerRPLatLng.$.newAttribute.$.customerRPLatLng.modelPropertyLat !== 'custshaLatitude' && 
		            this.owner.owner.$.bodyContent.$.attributes.$.line_customerRPLatLng.$.newAttribute.$.customerRPLatLng.modelPropertyLng !== 'custshaLongitude' &&
		            this.owner.owner.$.bodyContent.$.attributes.$.line_customerRPLatLng.$.newAttribute.$.customerRPLatLng.getValue() !== '' && 
		            this.owner.owner.$.bodyContent.$.attributes.$.line_customerRPLatLng.$.newAttribute.$.customerRPLatLng.getValue() !== "Lat:  -  / Lng: - ") {			        
		        	latitude = this.owner.owner.$.bodyContent.$.attributes.$.line_customerRPLatLng.$.newAttribute.$.customerRPLatLng.modelPropertyLat;
		        	langitude = this.owner.owner.$.bodyContent.$.attributes.$.line_customerRPLatLng.$.newAttribute.$.customerRPLatLng.modelPropertyLng;
		        } 
	        }
            if (this.owner.owner.$.bodyContent.$.attributes.$.line_customerAddrCountry) {
            var countryCode = this.owner.owner.$.bodyContent.$.attributes.$.line_customerAddrCountry.$.newAttribute.$.customerAddrCountry.$.customerAddrCombo.getSelected();
            var countryMobileCode = this.owner.owner.$.bodyContent.$.attributes.$.line_customerAddrCountry.$.newAttribute.$.customerAddrCountry.$.customerAddrCombo.collection.at(countryCode).get('countryIDDCode');
            var mobileLength = this.owner.owner.$.bodyContent.$.attributes.$.line_customerAddrCountry.$.newAttribute.$.customerAddrCountry.$.customerAddrCombo.collection.at(countryCode).get('mobileNoLength');
            var mobile = this.owner.owner.$.bodyContent.$.attributes.$.line_customerAddrCountry.$.newAttribute.$.customerAddrCountry.$.mobileNoLength.getValue();
            var isPhoneNumberEnabled = this.owner.owner.$.bodyContent.$.attributes.$.line_customerAddrCountry.showing;
            var isEmailEnabled = this.owner.owner.$.bodyContent.$.attributes.$.line_customerEmail.showing;
            if (this.owner.owner.$.bodyContent.$.attributes.$.line_customerEmail && isEmailEnabled) {
              var email=this.owner.owner.$.bodyContent.$.attributes.$.line_customerEmail.$.newAttribute.$.customerEmail.getValue();
              if(!email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,60})+$/)){
	              OB.UTIL.showError('Please provide a valid Customer email'); 
	              return false;
              }
            }
            if ((!countryMobileCode || !mobile) && isPhoneNumberEnabled) {
              OB.UTIL.showError('Please provide a valid mobile number with country code'); 
              return false; 
            } else if (isPhoneNumberEnabled && (mobile.length !== Math.round(mobileLength) || !((countryMobileCode + '' + mobile).match(/^(\d)*$/)))) {
              OB.UTIL.showError('Please provide a valid mobile number with country code'); 
              return false;
            }  
             else {
                if(isPhoneNumberEnabled )  {
                    this.owner.owner.model.set('custshaCustomerPhone', countryMobileCode + '' + mobile);
                    this.owner.owner.model.set('custshaCustomerCountryCode', countryCode);
                    this.owner.owner.model.set('custshaCustomercountryMobileCode', countryMobileCode);
                    this.owner.owner.model.set('custshaCustomerMobileNumber', mobile);
                    this.owner.owner.model.set('custshaCustomerEmail', email);
                }
             }
         }
            
        if(latitude != 0.0 && langitude != 0.0){
            this.owner.owner.model.set('custshaLatitude',latitude);
            this.owner.owner.model.set('custshaLongitude',langitude);
         }
            
                if (this.doApplyChanges()) {
                this.doHideThisPopup();
        }
    }
  });
}());