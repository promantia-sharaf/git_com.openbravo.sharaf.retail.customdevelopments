/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone */

OB.OBPOSCashUp.UI.CashUp.extend({
  changeStep: function (inSender, inEvent) {
    var direction = inEvent.originator.stepCount;
    var me = this;

    if (direction > 0) {
      // Check with the step if can go next.
      if (me.model.get('step') === 4 && this.model.get('totalExpected') !== this.model.get('totalCounted')) {
        OB.UTIL.showConfirmation.display(OB.I18N.getLabel('OBMOBC_Error'), OB.I18N.getLabel('CUSTSHA_ErrCashUpUnbalancedAmount'));
        return;
      } else {
        this.model.verifyStep(this.$.cashupMultiColumn.$.leftPanel.$, function () {
          me.moveStep(direction);
          OB.info("Cashup step: " + me.model.get('step'));
        });
      }
    } else {
      this.moveStep(direction);
      OB.info("Cashup step: " + me.model.get('step'));
    }
  }
});