/**
 * 
 */
enyo.kind({
	kind : 'OB.UI.ModalAction',
	name : 'CUSTSHA.UI.PromotionalBasisInfo',
	i18nLabel : 'CUSTSHA_LblPromotionalBasisInfo',
	hideCloseButton : true,
	autoDismiss : false,
	closeOnEscKey : true,

	handlers : {},

	bodyContent : {
		kind : 'Scroller',
		maxHeight : '225px',
		thumb : true,
		horizontal : 'hidden',
		components : [ {
			name : 'attributes'
		} ]
	},

	bodyButtons : {
		components : [ {
			name : 'error',
			content : '',
			style : 'color: red;'
		}, {
			kind : 'OB.UI.promotinalApply'
		}],
	},

	executeOnHide : function() {

	},

	executeOnShow : function() {

	},

	applyChanges : function(inSender, inEvent) {

	},

	initComponents : function() {
		this.inherited(arguments);
		this.attributeContainer = this.$.bodyContent.$.attributes;
		enyo.forEach(this.newAttributes, function(natt) {
			this.$.bodyContent.$.attributes.createComponent({
				kind : 'OB.UI.PropertyEditLine',
				name : 'line_' + natt.name,
				newAttribute : natt
			});
		}, this);
		this.setHeader(OB.I18N.getLabel('CUSTSHA_LblPromotionalBasisInfo'));
	},
});

enyo.kind({
	name : 'CUSTSHA.UI.PromotionalBasisInfo',
	kind : 'CUSTSHA.UI.PromotionalBasisInfo',
	newAttributes : [ {
		i18nLabel : 'CUSTSHA_PromoBasis',
		kind : 'OB.UI.List',
		name : 'promoList',
		mandatory : true,
		tag : 'select',
		classes : 'combo',
		style : 'width: 101%; margin:0;',
		renderEmpty : enyo.Control,
		renderLine : enyo.kind({
			kind : 'enyo.Option',
			initComponents : function() {
				this.inherited(arguments);
				this.setValue(this.model.get('id'));
				this.setContent(this.model.get('_identifier'));

			}
		})

	}, {
		kind : 'OB.UI.renderTextProperty',
		name : 'remarks',
		i18nLabel : 'CUSTSHA_Remarks',
		mandatory : true,
		maxLength : 60
	} ],
	executeOnHide : function() {

	},
	executeOnShow: function() {
	    this.$.bodyButtons.$.error.setContent('');
	    var promoBasisList = new Backbone.Collection();

	    // Get the array from the order
	    var listOfPromoBasis = this.args.order.get('PromoLists');

	    // Add an empty option as a placeholder
	    listOfPromoBasis.unshift('');
	    
	    // Set the collection for the dropdown
	    this.$.bodyContent.$.attributes.$.line_promoList.$.newAttribute.$.promoList.setCollection(promoBasisList);
	    this.$.bodyContent.$.attributes.$.line_remarks.$.newAttribute.$.remarks.clear();

	    // Reset the collection with an array of objects
	    promoBasisList.reset(listOfPromoBasis.map(function (value, index) {
	        return {
	            id: index,
	            _identifier: value
	        };
	    }));
	    
	},

	applyChanges : function(inSender, inEvent) {

	},
});
enyo.kind({
	kind : 'OB.UI.ModalDialogButton',
	name : 'OB.UI.promotinalApply',
	classes : 'obUiPromoBasisDialogApply',
	tap : function() {
		var promoLinesArray;
		if(!OB.UTIL.isNullOrUndefined(this.owner.owner.args.order.get('promobasisLines'))){
			promoLinesArray = this.owner.owner.args.order.get('promobasisLines');
		}else{
			promoLinesArray = [];
		}	
	var promotionalTypeSelected = this.owner.owner.$.bodyContent.$.attributes.$.line_promoList.$.newAttribute.$.promoList.getSelected();
	var promotionalBasisType = this.owner.owner.$.bodyContent.$.attributes.$.line_promoList.$.newAttribute.$.promoList.collection.at(promotionalTypeSelected).get("_identifier");
	var Remarks = this.owner.owner.$.bodyContent.$.attributes.$.line_remarks.$.newAttribute.$.remarks.getValue();
	var paymentname = this.owner.owner.args.args.paymentSelected.paymentMethod.name;
		if (Remarks == '' || promotionalBasisType == '') {
			this.owner.$.error.setContent('Promotional Basis/Remarks are mandatory Fields, Please enter');
			return;
		} else {
		    var promoLines = {};
		    promoLines= {
		    		"paymentname" : paymentname, 
		            "PromoBasis": promotionalBasisType,
		            "remarks": Remarks
		        };
		    promoLinesArray.push(promoLines);
			this.owner.owner.args.order.set('promobasisLines',promoLinesArray);
			
			this.doHideThisPopup();
		 }
		},

	initComponents : function() {
		this.inherited(arguments);
		this.setContent(OB.I18N.getLabel('OBMOBC_LblApply'));
	}

});

OB.UI.WindowView.registerPopup('OB.OBPOSPointOfSale.UI.PointOfSale', {
	kind : 'CUSTSHA.UI.PromotionalBasisInfo',
	name : 'CUSTSHA_UI_PromotionalBasisInfo'
});
