/*
 ************************************************************************************
 * Copyright (C) 2019 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, _ */

(function () {
  OB.UTIL.HookManager.registerHook('OBPOS_preChangeBusinessPartner', function (args, callbacks) {
    if (OB.MobileApp.model.receipt.get('payments').length !== 0) {
      OB.UTIL.showConfirmation.display(OB.I18N.getLabel('CUSTSHA_RestictChangeBusinessPartnerHeader'), OB.I18N.getLabel('CUSTSHA_RestictChangeBusinessPartnerBody'),[{
        label: OB.I18N.getLabel('OBMOBC_LblOk')
      }]);
    } else {
      OB.UTIL.HookManager.callbackExecutor(args, callbacks);
    }
  });
}());