/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, _ */

enyo.kind({
  name: 'OB.OBPOSPointOfSale.UI.EditLine.SharafDiscountReasonButton',
  kind: 'OB.UI.SmallButton',
  classes: 'btnlink-orange',
  handlers: {
    onSetMultiSelected: 'setMultiSelected'
  },
  tap: function () {
    var me = this;
    OB.MobileApp.view.waterfall('onShowPopup', {
      popup: 'CUSTSHA_UI_DiscountReasonPopup',
      args: {
        selectedModels: me.owner.owner.selectedModels
      }
    });
  },
  setMultiSelected: function (inSender, inEvent) {
    var hideButton = inSender.model.get('order').get('isPaid') && !inSender.model.get('order').get('isEditable');
    if (!hideButton) {
      _.each(inEvent.models, function (line) {
        if (line.get('promotions')) {
          var promo = _.find(line.get('promotions'), function (p) {
            return p.manual && p.discountType !== 'CA5491E6000647BD889B8D7CDF680795' && p.discountType !== '6A3C2313136147A6B2D1B8D2F5F768E6';
          });
          if (!promo) {
            hideButton = true;
          }
        } else {
          hideButton = true;
        }
      });
    }
    if (hideButton) {
      this.setShowing(false);
    } else {
      this.setShowing(true);
    }
  },
  init: function (model) {
    this.model = model;
  },
  initComponents: function () {
    this.inherited(arguments);
    this.setContent(OB.I18N.getLabel('CUSTSHA_DiscountReason'));
  }
});

OB.OBPOSPointOfSale.UI.EditLine.prototype.actionButtons.push({
  kind: 'OB.OBPOSPointOfSale.UI.EditLine.SharafDiscountReasonButton',
  name: 'sharafDiscountReasonButton'
});