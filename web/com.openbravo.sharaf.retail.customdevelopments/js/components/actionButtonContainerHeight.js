/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global enyo, Backbone, _ */

OB.OBPOSPointOfSale.UI.EditLine.prototype.kindComponents[1].components[0].components[0].maxHeight = "50%";
OB.OBPOSPointOfSale.UI.EditLine.prototype.kindComponents[1].components[0].components[2].components[0].style = "display: none";