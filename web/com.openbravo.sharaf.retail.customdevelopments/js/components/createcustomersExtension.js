/*
 ************************************************************************************
 * Copyright (C) 2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */
/*global OB, enyo, _ */

OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.newAttributes.shift({
  kind: 'OB.UI.CustomerTextProperty',
  name: 'firstName'
});
OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.newAttributes.shift({
  kind: 'OB.UI.CustomerTextProperty',
  name: 'lastName'
});

OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.newAttributes.shift({
  kind: 'OB.UI.CustomerComboProperty',
  name: 'customerCategory'
});
OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.newAttributes.shift({
  kind: 'OB.UI.CustomerTextProperty',
  name: 'customerTaxId'
});

OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.newAttributes.push({
  kind: 'OB.UI.CustomerTextProperty',
  name: 'customerLatLng',
  i18nLabel: 'CUSTSHA_Location',
  placeholder: 'Click here to select location on the map',
  loadValue: function(inSender, inEvent) {
     this.setValue('');
  },
  tap: function () {
	var me = this;
	var setLatLng = function (lat, lng) {
		  me.setValue("Lat: " + lat + " / Lng:" + lng);
		
	}
	OB.MobileApp.view.waterfallDown('onShowPopup', {
       popup: 'OB_UI_ModalGoogleMaps',
          args:{
               args: me.getValue(),
               callbacks: null,
               setLatLng: setLatLng
          }
    });
  } 
});

OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.newAttributes.shift({
  kind: 'OB.UI.CustomerTextProperty',
  name: 'customerPhone'
});

OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.newAttributes.unshift({
  kind: 'OB.UI.CustomerAddrComboPropertyNew',
  name: 'customerAddrCountry',
  modelProperty: 'mobileNoEntered',
  modelPropertyText: 'countryCode',
  i18nLabel: 'CUSTSHA_CountryCode',
  maxLength: 60,
  mandatory: true,
  collectionName: 'CountryList',
  defaultValue: function () {
    this.owner.owner.owner.$.line_customerAddrCountry.setStyle('float:left; width: 100%');
    return OB.MobileApp.model.get('terminal').defaultbp_bpcountry;
  },
  //Default value for new lines
  retrievedPropertyForValue: 'id',
  //property of the retrieved model to get the value of the combo item
  retrievedPropertyForText: '_identifier',
  fetchDataFunction: function (args) {
    var me = this,
        criteria;
    criteria = {
      _orderByClause: '_identifier asc'
    };
    OB.Dal.find(OB.Model.Country, criteria, function (data, args) {
      me.dataReadyFunction(data, args);
    }, function (error) {
      OB.UTIL.showError(OB.I18N.getLabel('OBPOS_ErrorGettingCountries'));
      me.dataReadyFunction(null, args);
    }, args);
  },
  displayLogic: function () {
    return OB.MobileApp.model.get('terminal').bp_showcategoryselector;
  }
});
enyo.kind({
  name: 'OB.UI.CustomerAddrComboPropertyNew',
  handlers: {
    onLoadValue: 'loadValue',
    onSaveChange: 'saveChange',
    onSetValue: 'valueSet',
    onRetrieveValues: 'retrieveValue',
    onchange: 'change'
  },
  events: {
    onSaveProperty: '',
    onSetValues: ''
  },
  components: [{
    kind: 'OB.UI.List',
    name: 'customerAddrCombo',
    classes: 'combo',
    style: 'width: 50%; float: left; margin: 0;',
    renderLine: enyo.kind({
      kind: 'enyo.Option',
      initComponents: function () {
        this.inherited(arguments);
        this.setValue(this.model.get(this.model.get('countryIDDCode')));
        this.setContent(this.model.get(this.parent.parent.retrievedPropertyForText) + ' (+' + this.model.get('countryIDDCode') + ')');
      }
    }),
    renderEmpty: 'enyo.Control'
  }, {
    kind: 'OB.UI.CustomerTextProperty',
    name: 'mobileNoLength',
    placeholder: 'Mobile Number',
    mandatory: true,
    style: 'width: 40%; float: left;',
  }],
  valueSet: function (inSender, inEvent) {
    var i;
    if (inEvent.data.hasOwnProperty(this.modelProperty)) {
      for (i = 0; i < this.$.customerAddrCombo.getCollection().length; i++) {
        if (this.$.customerAddrCombo.getCollection().models[i].get('id') === inEvent.data[this.modelProperty]) {
          this.$.customerAddrCombo.setSelected(i);
          break;
        }
      }
    }
  },
  retrieveValue: function (inSender, inEvent) {
    inEvent[this.modelProperty] = this.$.customerAddrCombo.getValue();
  },
  loadValue: function (inSender, inEvent) {
    this.$.customerAddrCombo.setCollection(this.collection);
    this.fetchDataFunction(inEvent);
  },
  change: function () {},
  dataReadyFunction: function (data, inEvent) {
    var index = 0,
        result = null;
    if (this.destroyed) {
      return;
    }
    if (data) {
      this.collection.reset(data.models);
    } else {
      this.collection.reset(null);
      return;
    }
    result = _.find(this.collection.models, function (categ) {
      if (inEvent.customerAddr) {
        //Edit: select actual value
        if (categ.get(this.retrievedPropertyForValue) === inEvent.customerAddr.get(this.modelProperty)) {
          return true;
        }
      } else {
        //New: select default value
        if (categ.get(this.retrievedPropertyForValue) === this.defaultValue()) {
          return true;
        }
      }
      index += 1;
    }, this);
    if (result) {
      this.$.customerAddrCombo.setSelected(index);
    } else {
      this.$.customerAddrCombo.setSelected(0);
    }
  },
  saveChange: function (inSender, inEvent) {
    var selected = this.collection.at(this.$.customerAddrCombo.getSelected());
    inEvent.customer.set(this.modelProperty, this.$.mobileNoLength.getValue());
    inEvent.customer.set('iddCode', selected.get('countryIDDCode'));
    inEvent.customer.set('iddCountryName', selected.get('country'));
    inEvent.customer.set('maxMobileNoLength', selected.get('mobileNoLength'));
  },
  initComponents: function () {
    if (this.collectionName && OB && OB.Collection && OB.Collection[this.collectionName]) {
      this.collection = new OB.Collection[this.collectionName]();
    } else {
      OB.info('OB.UI.CustomerAddrComboPropertyNew: Collection is required');
    }
    this.inherited(arguments);
    if (this.readOnly) {
      this.setAttribute('readonly', 'readonly');
    }
  }
});
OB.UTIL.HookManager.registerHook('OBPOS_BeforeCustomerSave', function (args, callbacks) {
  var mobileNo = '';
  if (!(args.customer.get('mobileNoEntered') === '') && (args.customer.get('phone') === '') ) {
    mobileNo = args.customer.get('iddCode') + args.customer.get('mobileNoEntered');
  } else {
    mobileNo = args.customer.get('phone');
  }
  var maxMobileNo = args.customer.get('maxMobileNoLength');
  var iddCountry = args.customer.get('iddCountryName');
  args.customer.set('phone', mobileNo);
  args.customer.set('maxMobileNoLength', maxMobileNo);
  args.customer.set('iddCountry', iddCountry);
  OB.UTIL.HookManager.callbackExecutor(args, callbacks);
});

OB.OBPOSPointOfSale.UI.customeraddr.editcustomers_impl.prototype.newAttributes.push({
  kind: 'OB.UI.CustomerAddrTextProperty',
  name: 'customerAddrLatLng',
  i18nLabel: 'CUSTSHA_Location',
  modelProperty: 'custshaLatLng',
  readOnly: true,
  loadValue: function(inSender, inEvent) {
     if (!OB.UTIL.isNullOrUndefined(inEvent.customerAddr) && !OB.UTIL.isNullOrUndefined(inEvent.customerAddr.get('custshaLongitude'))
         && !OB.UTIL.isNullOrUndefined(inEvent.customerAddr.get('custshaLatitude'))) {
      this.setValue('Lat: ' + inEvent.customerAddr.get('custshaLatitude') + ' / Lng:' + inEvent.customerAddr.get('custshaLongitude'));
    } else {
     this.setValue('');
    }
  }	
});

OB.OBPOSPointOfSale.UI.customeraddr.edit_createcustomers_impl.prototype.newAttributes.push({
  kind: 'OB.UI.CustomerAddrTextProperty',
  name: 'customerAddrLatLng',
  i18nLabel: 'CUSTSHA_Location',
  modelProperty: 'custshaLatLng',
  placeholder: 'Click here to select location on the map',
  loadValue: function(inSender, inEvent) {
     if (!OB.UTIL.isNullOrUndefined(inEvent.customerAddr) && !OB.UTIL.isNullOrUndefined(inEvent.customerAddr.get('custshaLongitude'))
         && !OB.UTIL.isNullOrUndefined(inEvent.customerAddr.get('custshaLatitude'))) {
      this.setValue('Lat: ' + inEvent.customerAddr.get('custshaLatitude') + ' / Lng:' + inEvent.customerAddr.get('custshaLongitude'));
    } else {
     this.setValue('');
    }
  },
  saveChange: function(inSender, inEvent) {
    if (!OB.UTIL.isNullOrUndefined(this.getValue()) && this.getValue() !== '' && !this.getValue().includes('-')) {
      inEvent.customerAddr.set('custshaLatitude', +this.getValue().split('/')[0].replace('Lat:', '').trim());
      inEvent.customerAddr.set('custshaLongitude', +this.getValue().split('/')[1].replace('Lng:', '').trim());
    }
  },
  tap: function () {
	var me = this;
	var setLatLng = function (lat, lng) {
        me.setValue("Lat: " + lat + " / Lng:" + lng);
        
	}
	OB.MobileApp.view.waterfallDown('onShowPopup', {
       popup: 'OB_UI_ModalGoogleMaps',
          args:{
               args: me.getValue(),
               callbacks: null,
               setLatLng: setLatLng
          }
    });
  } 
});
OB.OBPOSPointOfSale.UI.customeraddr.newcustomeraddr.extend({
	autoDismiss: false
});

OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.newAttributes.unshift({
  kind: 'OB.UI.CustomerTextProperty',
  name: 'customerPhone',
  modelProperty: 'phone',
  i18nLabel: 'OBPOS_LblPhone',
  maxlength: 40
});

// ksa -------------start-----------------------


OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.newAttributes.unshift({
  kind: 'OB.UI.customerIdProperty',
  name: 'customerIdProperty',
  modelProperty: 'customerIdentity',
  modelPropertyText: 'customerId',
  i18nLabel: 'CPSKSA_PartyId',
  maxLength: 60,
  collectionName: 'customerIdlist',
  defaultValue: function () {
    this.owner.owner.owner.$.line_customerIdProperty.setStyle('float:left; width: 100%');
  },
  retrievedPropertyForValue: 'id',
  retrievedPropertyForText: '_identifier',
  fetchDataFunction: function (args) {
  },
  displayLogic: function () {
    return OB.MobileApp.model.get('terminal').client === '63FE5171371A45D48E5322531C1EFEFC' ;
  }
});
enyo.kind({
  name: 'OB.UI.customerIdProperty',
  handlers: {
    onLoadValue: 'loadValue',
    onSaveChange: 'saveChange',
    onSetValue: 'valueSet',
    onRetrieveValues: 'retrieveValue',
    onchange: 'change'
  },
  events: {
    onSaveProperty: '',
    onSetValues: ''
  },
  components: [{
    kind: 'OB.UI.List',
    name: 'customerIdCombo',
    classes: 'combo',
    style: 'width: 50%; float: left; margin: 0;',
    renderLine: enyo.kind({
      kind: 'enyo.Option',
      initComponents: function () {
        this.inherited(arguments);
        this.setValue(this.model.get('id'));
        this.setContent(this.model.get('description'));
      }
    }),
    renderEmpty: 'enyo.Control'
  }, {
    kind: 'OB.UI.CustomerTextProperty',
    name: 'customerIdText',
    placeholder: 'Customer Id Value',
    style: 'width: 40%; float: left;',
    loadValue: function (inSender, inEvent) {
      if (inEvent.customer !== undefined && (!OB.UTIL.isNullOrUndefined(inEvent.customer.get('cpsksaCustomeridvalue')))
         && inEvent.customer.get('cpsksaCustomeridvalue') != '' ) {
        this.setValue(inEvent.customer.get('cpsksaCustomeridvalue'));
      } else {
        this.setValue('');
      }
    }   
  }],
  valueSet: function (inSender, inEvent) {
      console.log();
    var i;
    if (inEvent.data.hasOwnProperty(this.modelProperty)) {
      for (i = 0; i < this.$.customerIdCombo.getCollection().length; i++) {
        if (this.$.customerIdCombo.getCollection().models[i].get('id') === inEvent.data[this.modelProperty]) {
          this.$.customerIdCombo.setSelected(i);
          break;
        }
      }
    }
  },
  retrieveValue: function (inSender, inEvent) {
    inEvent[this.modelProperty] = this.$.customerIdCombo.getValue();
  },
  loadValue: function (inSender, inEvent) {
    this.collection = new Backbone.Collection();
    this.$.customerIdCombo.setCollection(this.collection);
    
    var i = 0;
    for (i; i < OB.MobileApp.model.get('businessPartnerIdentityInfo').length; i++) {
      model = new Backbone.Model(OB.MobileApp.model.get('businessPartnerIdentityInfo')[i]);
      if(i === 0) {
         this.collection.unshift(''); 
      }
      this.collection.add(model);
      
    }   
  this.dataReadyFunction(this.collection,inEvent);
  },
  change: function () {},
  dataReadyFunction: function (data, inEvent) {
    var index = 0,
        result = null;
    if (this.destroyed) {
      return;
    }
    if (data) {
      this.collection.reset(data.models);
    } else {
      this.collection.reset(null);
      return;
    }
  
  if (inEvent.customer !== undefined && (!OB.UTIL.isNullOrUndefined(inEvent.customer.get('cpsksaCustomerid')) 
        && !OB.UTIL.isNullOrUndefined(inEvent.customer.get('cpsksaCustomeridvalue')))) {
    for(var i=0; i<this.collection.models.length; i++) {
        var obj = this.collection.models[i];
        if(obj.get('id') === inEvent.customer.get('cpsksaCustomerid')) {
          index = i;
          break;
        }
  }        
     this.$.customerIdCombo.setSelected(index);
  }
  
  },
  saveChange: function (inSender, inEvent) {
   if(OB.MobileApp.model.get('terminal').client === '63FE5171371A45D48E5322531C1EFEFC') 
   {  
    var selected = this.collection.at(this.$.customerIdCombo.getSelected());
    inEvent.customer.set('cpsksaCustomerid', selected.get('id'));
    inEvent.customer.set('cpsksaCustomeridvalue',this.$.customerIdText.getValue());
   } 
  },
  initComponents: function () {
    if (this.collectionName && OB && OB.Collection && OB.Collection[this.collectionName]) {
      this.collection = new OB.Collection[this.collectionName]();
    } else {
      OB.info('OB.UI.CustomerAddrComboPropertyNew: Collection is required');
    }
    this.inherited(arguments);
    if (this.readOnly) {
      this.setAttribute('readonly', 'readonly');
    }
  }
});



// ksa ---------------------------------end--------------------------------------------------------

OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.newAttributes.unshift({
  kind: 'OB.UI.CustomerTextProperty',
  name: 'customerTaxId',
  modelProperty: 'taxID',
  i18nLabel: 'OBPOS_LblTaxId',
  displayLogic: function () {
    return OB.MobileApp.model.get('terminal').bp_showtaxid;
  },
  maxlength: 20
});
OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.newAttributes.unshift({
  kind: 'OB.UI.CustomerComboProperty',
  name: 'customerCategory',
  modelProperty: 'businessPartnerCategory',
  //Required: property where the selected value will be get and where the value will be saved
  modelPropertyText: 'businessPartnerCategory_name',
  //optional: When saving, the property which will store the selected text
  collectionName: 'BPCategoryList',
  defaultValue: function () {
    return OB.MobileApp.model.get('terminal').defaultbp_bpcategory;
  },
  //Default value for new lines
  retrievedPropertyForValue: 'id',
  //property of the retrieved model to get the value of the combo item
  retrievedPropertyForText: '_identifier',
  //property of the retrieved model to get the text of the combo item
  //function to retrieve the data
  fetchDataFunction: function (args) {
    var me = this,
        criteria;
    criteria = {
      _orderByClause: '_identifier asc'
    };
    OB.Dal.find(OB.Model.BPCategory, criteria, function (data, args) {
      //This function must be called when the data is ready
      me.dataReadyFunction(data, args);
    }, function (error) {
      OB.UTIL.showError(OB.I18N.getLabel('OBPOS_ErrorGettingBPCategories'));
      //This function must be called when the data is ready
      me.dataReadyFunction(null, args);
    }, args);
  },
  i18nLabel: 'OBPOS_BPCategory',
  mandatory: true,
  displayLogic: function () {
    return OB.MobileApp.model.get('terminal').bp_showcategoryselector;
  }
});

//

OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.newAttributes.unshift({
  kind: 'OB.UI.CustomerTextProperty',
  name: 'custshaLoyaltyCard',
  modelProperty: 'custshaLoyaltyCard',
  isFirstFocus: true,
  i18nLabel: 'CUSTSHA_LblLoyalityCard',
  type:'NUMBER',
  maxlength: 12,
  
  input: function() {
	   var input = this.getValue();
       var size = input.length;
       if (size > 12) {
          this.setValue(input.substring(0, input.length - 1));
       } 
	},
	loadValue: function (inSender, inEvent) {
      if (inEvent.customer !== undefined && (!OB.UTIL.isNullOrUndefined(inEvent.customer.get('custshaLoyaltyCard')))
         && inEvent.customer.get('custshaLoyaltyCard') !=0 ) {
        this.setValue(inEvent.customer.get('custshaLoyaltyCard'));
      } else {
        this.setValue('');
      }
    }		  
});

//

OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.newAttributes.unshift({
  kind: 'OB.UI.CustomerTextProperty',
  name: 'lastName',
  modelProperty: 'lastName',
  isFirstFocus: true,
  i18nLabel: 'OBPOS_LblLastName',
  maxlength: 60
});

OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.newAttributes.unshift({
  kind: 'OB.UI.CustomerTextProperty',
  name: 'firstName',
  modelProperty: 'firstName',
  isFirstFocus: true,
  i18nLabel: 'OBPOS_LblName',
  maxlength: 60,
  mandatory: true
});


OB.OBPOSPointOfSale.UI.customers.edit_createcustomers.extend({
  autoDismiss: false,
  saveCustomer: function (inSender, inEvent) {
    var me = this,
        customerEdited;
        
    var saveLatitudeLongitude = function() {
	    var custshaLatLng = me.owner.owner.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_customerLatLng.$.newAttribute.$.customerLatLng.getValue();
	    if (!OB.UTIL.isNullOrUndefined(custshaLatLng) && !custshaLatLng.includes('-') && custshaLatLng !== '') {
		  var custshaLat = custshaLatLng.split('/')[0].replace('Lat:', '').trim();
		  var custshaLng = custshaLatLng.split('/')[1].replace('Lng:', '').trim();
		  
		  me.model.get('customer').set('custshaLatitude', +custshaLat);
		  me.model.get('customer').set('custshaLongitude', +custshaLng);
		  
		  me.model.get('customerAddr').set('custshaLatitude', +custshaLat);
		  me.model.get('customerAddr').set('custshaLongitude', +custshaLng);
	    }
    }

    function getCustomerValues(params) {
      me.waterfall('onSaveChange', {
        customer: params.customer
      });
    }
  
    function checkMandatoryFields(items, customer) {
      var errors = '',
          maxLength = Math.round(customer.get('maxMobileNoLength'));
      _.each(items, function (item) {
        if (item.newAttribute.mandatory && item.newAttribute.modelProperty != 'phone' && (customer.get('phone') === '')) {
          var value = customer.get(item.newAttribute.modelProperty);
          if (!value) {
            if (errors) {
              errors += ', ';
            }
            errors += OB.I18N.getLabel(item.newAttribute.i18nLabel);
          } else {
            if (item.newAttribute.modelProperty === 'mobileNoEntered') {
              if (!(value.match(/^(\d|[-+ ]?)*$/)) || !(value.length === maxLength)) {
                if (errors) {
                  errors += ', ';
                }
                errors += OB.I18N.getLabel('CUSTSHA_InvalidPhone');
              }
            }
            if (item.newAttribute.modelProperty === 'email') {
              if (!(value.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,60})+$/))) {
                if (errors) {
                  errors += ', ';
                }
                errors += OB.I18N.getLabel('CUSTSHA_InvalidEmail');
              }
            }
          }
        } else if (item.newAttribute.mandatory && item.newAttribute.modelProperty != 'phone' && (customer.get('phone') != '')) {
          var value = customer.get(item.newAttribute.modelProperty);
          if (item.newAttribute.modelProperty === 'mobileNoEntered' && customer.get(item.newAttribute.modelProperty) == '') {
            value = customer.get('phone');
          }
          if (!value) {
            if (errors) {
              errors += ', ';
            }
            errors += OB.I18N.getLabel(item.newAttribute.i18nLabel);
          } else {
            if (item.newAttribute.modelProperty === 'email') {
              if (!(value.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,60})+$/))) {
                if (errors) {
                  errors += ', ';
                }
                errors += OB.I18N.getLabel('CUSTSHA_InvalidEmail');
              }
            }
          }
        }
      });
      return errors;
    }

    function validateForm(form) {
      if (inEvent.validations) {
        var customer = form.model.get('customer'),
        errors =  checkMandatoryFields(form.$.customerOnlyFields.children, customer);
        if (form.$.invoicingAddrFields.showing) {
          var invoicingErrors = checkMandatoryFields(form.$.invoicingAddrFields.children, customer);
          if (invoicingErrors) {
            if (errors) {
              errors += ', ';
            }
            errors += (form.$.invoicingAddrFields.getClassAttribute().indexOf("twoAddrLayout") === 0 ? OB.I18N.getLabel('OBPOS_LblBillAddr') + ' [' + invoicingErrors + ']' : invoicingErrors);
          }
        }
        if (form.$.shippingAddrFields.showing && form.$.shippingAddrFields.getClassAttribute().indexOf("twoAddrLayout") === 0) {
          var shippingErrors = checkMandatoryFields(form.$.shippingAddrFields.children, customer);
          if (shippingErrors) {
            if (errors) {
              errors += ', ';
            }
            errors += OB.I18N.getLabel('OBPOS_LblShipAddr') + ' [' + shippingErrors + ']';
          }
        }
        if (errors) {
          OB.UTIL.showError(OB.I18N.getLabel('OBPOS_BPartnerRequiredFields', [errors]));
          return false;
        }
        if (customer.get('firstName').length + customer.get('lastName').length >= 60) {
          OB.UTIL.showError(OB.I18N.getLabel('OBPOS_TooLongName'));
          return false;
        }
       var loyalno = customer.get('custshaLoyaltyCard');
       var spaceCheck = false;
       if(loyalno.length != 0 ) {
			if (!(loyalno.match(/^[0-9]*$/) && loyalno.length === 12)) {
              spaceCheck = true;
           } 
		 }
         if(spaceCheck) {
          OB.UTIL.showError(OB.I18N.getLabel('CUSTSHA_InvalidLoyalityCardno'));
          return false;
         } else {
            if(loyalno.length === 0 || loyalno === '' ) {
		   		 customer.set('custshaLoyaltyCard', 0);
			 }
		 }
      }
      return true;
    }

    function beforeCustomerSave(customer, isNew) {
        customer.adjustNames();
        saveLatitudeLongitude();

        var mobileNoEntered, emailEntered;
        var maxRetries = 3;
        var retryCount = 0;
        var allowDefaultEmail = (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.attributes.permissions.CUSTSHA_AllowDefaultEmail)) 
        ? OB.MobileApp.model.attributes.permissions.CUSTSHA_AllowDefaultEmail 
        : null;

    var enableValidation = (!OB.UTIL.isNullOrUndefined(OB.MobileApp.model.attributes.permissions.CUSTSHA_EnableEmailAndPhoneValidation)) 
        ? OB.MobileApp.model.attributes.permissions.CUSTSHA_EnableEmailAndPhoneValidation === true 
        : false;


        if (!OB.UTIL.isNullOrUndefined(customer.get('mobileNoEntered'))) {
            if (!(customer.get('mobileNoEntered') === '') && (customer.get('phone') === '')) {
                mobileNoEntered = customer.get('iddCode') + customer.get('mobileNoEntered');
            } else {
                mobileNoEntered = customer.get('phone');
            }
        }
        if (!OB.UTIL.isNullOrUndefined(customer.get('email'))) {
            emailEntered = customer.get('email');
        }

        function executeProcess() {
            // If POS is offline and we have retried max times, skip validation
            if (!OB.MobileApp.model.get('connectedToERP') && retryCount >= maxRetries) {
                console.log("POS is offline, skipping API validation.");
                executeSave();
                return;
            }

            // If validation is disabled, execute save directly
            if (!enableValidation) {
                console.log("Email and Phone validation is disabled. Skipping validation.");
                executeSave();
                return;
            }

            // If entered email matches allowed default email, skip email validation but continue phone validation
            var skipEmailValidation = (!OB.UTIL.isNullOrUndefined(emailEntered) && emailEntered === allowDefaultEmail);

            var process = new OB.DS.Process('com.openbravo.sharaf.retail.customdevelopments.process.CheckEmailAndPhoneNoValidation');
            process.exec({
                phoneNo: mobileNoEntered,
                email: skipEmailValidation ? null : emailEntered // Skip email validation if condition matches
            }, function(data) {
                if (data && data.exception) {
                    if (!OB.UTIL.isNullOrUndefined(data.exception.message)) {
                        console.log(data.exception.message);
                        if (data.exception.message.includes('Application server is not available.')) {
                            retryCount++;
                            console.log(`Retry attempt: ${retryCount}`);
                            setTimeout(executeProcess, 1000); // Retry after 1 second
                            return;
                        }
                    }
                } else {
                    if (data.isPhoneNoPresent) {
                        OB.UTIL.showError(OB.I18N.getLabel('CUSTSHA_alreadyphoneNoPresent'));
                        me.waterfall('onDisableButton', { disabled: false });
                        return false;
                    } else if (!skipEmailValidation && data.isEmailPresent) { // Only check email if validation wasn't skipped
                        OB.UTIL.showError(OB.I18N.getLabel('CUSTSHA_alreadyEmailPresent'));
                        me.waterfall('onDisableButton', { disabled: false });
                        return false;
                    } else {
                        executeSave();
                    }
                }
            });
        }

        function executeSave() {
            OB.UTIL.HookManager.executeHooks('OBPOS_BeforeCustomerSave', {
                customer: customer,
                isNew: isNew,
                validations: inEvent.validations,
                windowComponent: me
            }, function(args) {
                if (args && args.cancellation && args.cancellation === true) {
                    return true;
                }
                customerEdited = args.customer;
                args.windowComponent.waterfall('onDisableButton', { disabled: true });
                args.customer.saveCustomer(function(result) {
                    args.windowComponent.waterfall('onDisableButton', { disabled: false });
                    if (result && !inEvent.silent) {
                        me.bubble('onCancelClose', { customer: customerEdited });
                    }
                });
            });
        }

        // Start the process
        executeProcess();
    }

    if (this.customer === undefined) {
      this.model.get('customer').newCustomer();
      this.waterfall('onSaveChange', {
        customer: this.model.get('customer')
      });
      if (validateForm(this)) {
        beforeCustomerSave(this.model.get('customer'), true);
      } else {
        this.waterfall('onDisableButton', {
          disabled: false
        });
      }
    } else {
      this.model.get('customer').loadModel(this.customer, function (customer) {
        getCustomerValues({
          customer: customer
        });
        if (validateForm(me)) {
          beforeCustomerSave(customer, false);
        } else {
          me.waterfall('onDisableButton', {
            disabled: false
          });
        }
      });
    }
  }
});

/* customer shipping Address city drop down */

OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.shipAddrAttributes.pop({
  kind: 'OB.UI.CustomerTextPropertyAddr',
  name: 'customerCity'
 });

OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.shipAddrAttributes.push({
    kind: 'OB.UI.CustomerShipCityComboProperty',
    name: 'customerCity',
    modelProperty: 'shipCityName',
    collectionName: 'CityList',
    i18nLabel: 'OBPOS_LblCity',
    maxlength: 60,
   
   hideShow: function(inSender, inEvent) {
        if (inEvent.checked) {
          this.owner.removeClass('width52');
          this.owner.owner.$.labelLine.removeClass('width40');
          this.owner.owner.hide();
        } else {
          this.owner.addClass('width52');
          this.owner.owner.$.labelLine.addClass('width40');
          this.owner.owner.show();
        }
      }, 
      
   defaultValue: function(args) {
      var selectedCountryId = OB.MobileApp.model.get('terminal').defaultbp_bpcountry; 
        var me = this,
        criteria;
        criteria = {
          country :selectedCountryId,  
          _orderByClause: 'name asc'
        };
        OB.Dal.find(
          OB.Model.City,
          criteria,
          function(data, args) {
            console.log('This function must be called when the data is ready');
            me.dataReadyFunction(data, args);
          },
          function(error) {
              console.log('This function must be called when the data is error');
              console.log(error);
           // OB.UTIL.showError(OB.I18N.getLabel('OBPOS_ErrorGettingCountries'));
            //This function must be called when the data is ready
            me.dataReadyFunction(null, args);
        }, args); 
    
   },
    retrievedPropertyForValue: 'id',
    retrievedPropertyForText: 'name',
    
    fetchDataFunction: function(args) {
       var selectedCountryId = OB.MobileApp.model.get('terminal').defaultbp_bpcountry; 
       var selectedCountryIndex = this.owner.owner.owner.$.line_customerShipCountry.$.newAttribute.$.customerShipCountry.$.customerCombo.getSelected();
      if(!OB.UTIL.isNullOrUndefined(selectedCountryIndex) 
           && !OB.UTIL.isNullOrUndefined(this.owner.owner.owner.$.line_customerShipCountry.$.newAttribute.$.customerShipCountry.$.customerCombo.getSelected())
           && !OB.UTIL.isNullOrUndefined(this.owner.owner.owner.$.line_customerShipCountry.$.newAttribute.$.customerShipCountry.$.customerCombo.children[selectedCountryIndex])
           && !OB.UTIL.isNullOrUndefined(this.owner.owner.owner.$.line_customerShipCountry.$.newAttribute.$.customerShipCountry.$.customerCombo.children[selectedCountryIndex].attributes.value)){
           selectedCountryId = this.owner.owner.owner.$.line_customerShipCountry.$.newAttribute.$.customerShipCountry.$.customerCombo.children[selectedCountryIndex].attributes.value;
           }
        var me = this,
        criteria;
        criteria = {
          country :selectedCountryId,  
          _orderByClause: 'name asc'
        };
        OB.Dal.find(
          OB.Model.City,
          criteria,
          function(data, args) {
            console.log('This function must be called when the data is ready');
            me.dataReadyFunction(data, args);
          },
          function(error) {
              console.log('This function must be called when the data is error');
              console.log(error);
              me.dataReadyFunction(null, args);
        }, args);
    }
    
  });


enyo.kind({
  name: 'OB.UI.CustomerShipCityComboProperty',
  handlers: {
    onLoadValue: 'loadValue',
    onSaveChange: 'saveChange',
    onSetValue: 'valueSet',
    onRetrieveValues: 'retrieveValue',
    onchange: 'change',
    onHideShow: 'hideShow'
  },
  events: {
    onSaveProperty: '',
    onSetValues: ''
  },
  components: [{
    kind: 'OB.UI.List',
    name: 'customerShipCityCombo',
    classes: 'combo',
    style: 'width: 101%; margin:0;',
    renderLine: enyo.kind({
      kind: 'enyo.Option',
      initComponents: function () {
        this.inherited(arguments);
        this.setValue(this.model.get(this.model.get('id')));
        this.setContent(this.model.get('name'));
      }
    }),
    renderEmpty: 'enyo.Control'
  }
  ],
  
  valueSet: function (inSender, inEvent) {
    var i;
    if (inEvent.data.hasOwnProperty(this.modelProperty)) {
      for (i = 0; i < this.$.customerShipCityCombo.getCollection().length; i++) {
        if (this.$.customerShipCityCombo.getCollection().models[i].get('id') === inEvent.data[this.modelProperty]) {
          this.$.customerShipCityCombo.setSelected(i);
          break;
        }
      }
    }
  },
  retrieveValue: function (inSender, inEvent) {
    inEvent[this.modelProperty] = this.$.customerShipCityCombo.getValue();
  },
  loadValue: function (inSender, inEvent) {
  //  this.collection.add('');  
    this.$.customerShipCityCombo.setCollection(this.collection);
   // this.fetchDataFunction(inEvent);
   this.defaultValue(inEvent);
  },
  change: function () {},
  dataReadyFunction: function (data, inEvent) {
    var index = 0,
        result = null;
    if (this.destroyed) {
      return;
    }
    if (data) {
      data.models.unshift('')
      this.collection.reset(data.models);
    } else {
      this.collection.reset(null);
      return;
    }
    result = _.find(this.collection.models, function (categ) {
        if (!OB.UTIL.isNullOrUndefined (inEvent) &&  !OB.UTIL.isNullOrUndefined(inEvent.customerAddr)) {
        //Edit: select actual value
        if (categ.get(this.retrievedPropertyForText) === inEvent.customerAddr.get('cityName')) {
          return true;
        }
      } 
      index += 1;
    }, this);
    if (result) {
      this.$.customerShipCityCombo.setSelected(index);
    } else {//New: select default value
      this.$.customerShipCityCombo.setSelected(0);
    }
  },
  
  saveChange: function (inSender, inEvent) {
    var selected = this.collection.at(this.$.customerShipCityCombo.getSelected());
    if(!OB.UTIL.isNullOrUndefined (selected) && !OB.UTIL.isNullOrUndefined (selected.get('name')) && selected.get('name') != ''){
        inEvent.customer.set('cityName', selected.get('name'));
    } 
  },
  
  initComponents: function () {
    if (this.collectionName && OB && OB.Collection && OB.Collection[this.collectionName]) {
      this.collection = new OB.Collection[this.collectionName]();
    } else {
      OB.info('OB.UI.customerShipCityCombo: Collection is required');
    }
    this.inherited(arguments);
    if (this.readOnly) {
      this.setAttribute('readonly', 'readonly');
    }
  }
});


OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.shipAddrAttributes.find(item => item.name == 'customerShipCountry').change = function() {
      this.owner.owner.owner.$.line_customerCity.$.newAttribute.$.customerCity.fetchDataFunction(null);
 };




/* Code for city drop down in customers invoice*/  

OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.invAddrAttributes.pop({
  kind: 'OB.UI.CustomerTextPropertyAddr',
  name: 'customerInvCity'
 });

OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.invAddrAttributes.push({
    kind: 'OB.UI.CustomerCityComboPropertyNew',
    name: 'customerInvCity',
    modelProperty: 'cityName',
    i18nLabel: 'OBPOS_LblCity',
    collectionName: 'CityList',
    maxlength: 60,
    mandatory: true,
   
   hideShow: function(inSender, inEvent) {
        if (inEvent.checked) {
          this.owner.removeClass('width52');
          this.owner.owner.$.labelLine.removeClass('width40');
        //  this.owner.owner.hide();
        } else {
          this.owner.addClass('width52');
          this.owner.owner.$.labelLine.addClass('width40');
          this.owner.owner.show();
        }
      }, 
      
      
   defaultValue: function(args) {
      var selectedCountryId = OB.MobileApp.model.get('terminal').defaultbp_bpcountry; 
        var me = this,
        criteria;
        criteria = {
          country :selectedCountryId,  
          _orderByClause: 'name asc'
        };
        OB.Dal.find(
          OB.Model.City,
          criteria,
          function(data, args) {
            console.log('This function must be called when the data is ready');
            me.dataReadyFunction(data, args);
          },
          function(error) {
              console.log('This function must be called when the data is error');
              console.log(error);
            //This function must be called when the data is ready
            me.dataReadyFunction(null, args);
        }, args); 
    
   },
    retrievedPropertyForValue: 'id',
    retrievedPropertyForText: 'name',
    
    fetchDataFunction: function(args) {
       var selectedCountryId = OB.MobileApp.model.get('terminal').defaultbp_bpcountry; 
       var selectedCountryIndex = this.owner.owner.owner.$.line_customerCountry.$.newAttribute.$.customerCountry.$.customerCombo.getSelected();
      if(!OB.UTIL.isNullOrUndefined(selectedCountryIndex) 
           && !OB.UTIL.isNullOrUndefined(this.owner.owner.owner.$.line_customerCountry.$.newAttribute.$.customerCountry.$.customerCombo.getSelected())
           && !OB.UTIL.isNullOrUndefined(this.owner.owner.owner.$.line_customerCountry.$.newAttribute.$.customerCountry.$.customerCombo.children[selectedCountryIndex])
           && !OB.UTIL.isNullOrUndefined(this.owner.owner.owner.$.line_customerCountry.$.newAttribute.$.customerCountry.$.customerCombo.children[selectedCountryIndex].attributes.value)){
           selectedCountryId = this.owner.owner.owner.$.line_customerCountry.$.newAttribute.$.customerCountry.$.customerCombo.children[selectedCountryIndex].attributes.value;
           }
        var me = this,
        criteria;
        criteria = {
          country :selectedCountryId,  
          _orderByClause: 'name asc'
        };
        OB.Dal.find(
          OB.Model.City,
          criteria,
          function(data, args) {
            console.log('This function must be called when the data is ready');
            me.dataReadyFunction(data, args);
          },
          function(error) {
              console.log('This function must be called when the data is error');
              console.log(error);
           // OB.UTIL.showError(OB.I18N.getLabel('OBPOS_ErrorGettingCountries'));
            //This function must be called when the data is ready
            me.dataReadyFunction(null, args);
        }, args);
    }
    
  });
 

enyo.kind({
  name: 'OB.UI.CustomerCityComboPropertyNew',
  handlers: {
    onLoadValue: 'loadValue',
    onSaveChange: 'saveChange',
    onSetValue: 'valueSet',
    onRetrieveValues: 'retrieveValue',
    onchange: 'change',
    onHideShow: 'hideShow'
  },
  events: {
    onSaveProperty: '',
    onSetValues: ''
  },
  components: [{
    kind: 'OB.UI.List',
    name: 'customerCityCombo',
    classes: 'combo',
    style: 'width: 101%; margin: 0;',
    renderLine: enyo.kind({
      kind: 'enyo.Option',
      initComponents: function () {
        this.inherited(arguments);
        this.setValue(this.model.get(this.model.get('id')));
        this.setContent(this.model.get('name'));
      }
    }),
    renderEmpty: 'enyo.Control'
  }
  ],
  
  valueSet: function (inSender, inEvent) {
    var i;
    if (inEvent.data.hasOwnProperty(this.modelProperty)) {
      for (i = 0; i < this.$.customerCityCombo.getCollection().length; i++) {
        if (this.$.customerCityCombo.getCollection().models[i].get('id') === inEvent.data[this.modelProperty]) {
          this.$.customerCityCombo.setSelected(i);
          break;
        }
      }
    }
  },
  retrieveValue: function (inSender, inEvent) {
    inEvent[this.modelProperty] = this.$.customerCityCombo.getValue();
  },
  loadValue: function (inSender, inEvent) {
    this.$.customerCityCombo.setCollection(this.collection);
   this.defaultValue(inEvent);
  },
  change: function () {},
  dataReadyFunction: function (data, inEvent) {
    var index = 0,
        result = null;
    if (this.destroyed) {
      return;
    }
    if (data) {
      data.models.unshift('')
      //data.models.reverse();  
      this.collection.reset(data.models);
    } else {
      this.collection.reset(null);
      return;
    }
    result = _.find(this.collection.models, function (categ) {
        if (!OB.UTIL.isNullOrUndefined (inEvent) &&  !OB.UTIL.isNullOrUndefined(inEvent.customer)) {
        //Edit: select actual value
        if (categ.get(this.retrievedPropertyForText) === inEvent.customer.get('cityName')) {
          return true;
        }
      } 
      index += 1;
    }, this);
    if (result) {
      this.$.customerCityCombo.setSelected(index);
    } else {//New: select default value
      this.$.customerCityCombo.setSelected(0);
    }
  },
  
  saveChange: function (inSender, inEvent) {
    var selected = this.collection.at(this.$.customerCityCombo.getSelected());
    if(!OB.UTIL.isNullOrUndefined (selected) && !OB.UTIL.isNullOrUndefined (selected.get('name')) && selected.get('name') != ''){
        inEvent.customer.set('cityName', selected.get('name'));
    }else{ 
        if(OB.UTIL.isNullOrUndefined(OB.MobileApp.model.receipt.get('bp').get('cityName'))){
             OB.UTIL.showError(OB.I18N.getLabel('OBPOS_CityReqForBPAddress'));  
        } else {
            inEvent.customer.set('cityName',OB.MobileApp.model.receipt.get('bp').get('cityName'));
        } 
    }
    
  },
  
  initComponents: function () {
    if (this.collectionName && OB && OB.Collection && OB.Collection[this.collectionName]) {
      this.collection = new OB.Collection[this.collectionName]();
    } else {
      OB.info('OB.UI.CustomerCityComboPropertyNew: Collection is required');
    }
    this.inherited(arguments);
    if (this.readOnly) {
      this.setAttribute('readonly', 'readonly');
    }
  }
});

 OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.invAddrAttributes.find(item => item.name == 'customerCountry').change = function() {
     this.owner.owner.owner.$.line_customerInvCity.$.newAttribute.$.customerInvCity.fetchDataFunction(null);
 };
 
 
 /* end Code for city drop down in customers */
 
 
/* Code for city drop down in customeraddr */
 
 
 OB.OBPOSPointOfSale.UI.customeraddr.edit_createcustomers_impl.prototype.newAttributes.shift({
  kind: 'OB.UI.CustomerAddrTextProperty',
  name: 'customerAddrCustomerName'
      
 });

OB.OBPOSPointOfSale.UI.customeraddr.edit_createcustomers_impl.prototype.newAttributes.shift({
   kind: 'OB.UI.CustomerAddrTextProperty',
      name: 'customerAddrName'
      
 });

OB.OBPOSPointOfSale.UI.customeraddr.edit_createcustomers_impl.prototype.newAttributes.shift({
  kind: 'OB.UI.CustomerAddrTextProperty',
      name: 'customerAddrPostalCode'
      
 });

OB.OBPOSPointOfSale.UI.customeraddr.edit_createcustomers_impl.prototype.newAttributes.shift({
  kind: 'OB.UI.CustomerAddrTextProperty',
  name: 'customerAddrCity'
      
 });
 
 
OB.OBPOSPointOfSale.UI.customeraddr.edit_createcustomers_impl.prototype.newAttributes.shift({
  kind: 'OB.UI.CustomerAddrComboProperty',
  name: 'customerAddrCountry'
      
 }); 


 OB.OBPOSPointOfSale.UI.customeraddr.edit_createcustomers_impl.prototype.newAttributes.unshift({
      kind: 'OB.UI.CustomerAddrCityComboPropertyNew',
      name: 'customerAddrCity',
      modelProperty: 'cityName',
      i18nLabel: 'OBPOS_LblCity',
      collectionName: 'CityList',
      maxlength: 60,
      mandatory: true,
   
   defaultValue: function(args) {
        var selectedCountryId = OB.MobileApp.model.get('terminal').defaultbp_bpcountry; 
         if(!OB.UTIL.isNullOrUndefined(args.customerAddr) && !OB.UTIL.isNullOrUndefined(args.customerAddr.attributes) && !OB.UTIL.isNullOrUndefined(args.customerAddr.attributes.countryId)) {
          selectedCountryId = args.customerAddr.attributes.countryId;
        }
        var me = this,
        criteria;
        criteria = {
          country :selectedCountryId,  
          _orderByClause: 'name asc'
        };
        OB.Dal.find(
          OB.Model.City,
          criteria,
          function(data, args) {
            console.log('This function must be called when the data is ready');
            me.dataReadyFunction(data, args);
          },
          function(error) {
              console.log('This function must be called when the data is error');
              console.log(error);
           // OB.UTIL.showError(OB.I18N.getLabel('OBPOS_ErrorGettingCountries'));
            //This function must be called when the data is ready
            me.dataReadyFunction(null, args);
        }, args); 
    
   },
   
    retrievedPropertyForValue: 'id',
    retrievedPropertyForText: 'name',
    
    fetchDataFunction: function(args) {
       var selectedCountryId = OB.MobileApp.model.get('terminal').defaultbp_bpcountry; 
        var selectedCountryIndex = this.owner.owner.owner.$.line_customerAddrCountry.$.newAttribute.$.customerAddrCountry.$.customerAddrCombo.getSelected();
      
      if(!OB.UTIL.isNullOrUndefined(selectedCountryIndex) 
            && !OB.UTIL.isNullOrUndefined(this.owner.owner.owner.$.line_customerAddrCountry.$.newAttribute.$.customerAddrCountry.$.customerAddrCombo.getSelected())
            && !OB.UTIL.isNullOrUndefined(this.owner.owner.owner.$.line_customerAddrCountry.$.newAttribute.$.customerAddrCountry.$.customerAddrCombo.children[selectedCountryIndex])
            && !OB.UTIL.isNullOrUndefined(this.owner.owner.owner.$.line_customerAddrCountry.$.newAttribute.$.customerAddrCountry.$.customerAddrCombo.children[selectedCountryIndex].attributes.value)){
            selectedCountryId = this.owner.owner.owner.$.line_customerAddrCountry.$.newAttribute.$.customerAddrCountry.$.customerAddrCombo.children[selectedCountryIndex].attributes.value;
       }
       
        var me = this,
        criteria;
        criteria = {
          country :selectedCountryId,  
          _orderByClause: 'name asc'
        };
        OB.Dal.find(
          OB.Model.City,
          criteria,
          function(data, args) {
            console.log('This function must be called when the data is ready');
            me.dataReadyFunction(data, args);
          },
          function(error) {
              console.log('This function must be called when the data is error');
              console.log(error);
            me.dataReadyFunction(null, args);
        }, args);
    }
    
  });
 

enyo.kind({
  name: 'OB.UI.CustomerAddrCityComboPropertyNew',
  handlers: {
    onLoadValue: 'loadValue',
    onSaveChange: 'saveChange',
    onSetValue: 'valueSet',
    onRetrieveValues: 'retrieveValue',
    onchange: 'change'
  },
  events: {
    onSaveProperty: '',
    onSetValues: ''
  },
  components: [{
    kind: 'OB.UI.List',
    name: 'customerAddrCityCombo',
    classes: 'combo',
    style: 'width: 50%; float: left; margin: 0;',
    renderLine: enyo.kind({
      kind: 'enyo.Option',
      initComponents: function () {
        this.inherited(arguments);
        this.setValue(this.model.get(this.model.get('id')));
        this.setContent(this.model.get('name'));
      }
    }),
    renderEmpty: 'enyo.Control'
  }
  ],
  
  valueSet: function (inSender, inEvent) {
    var i;
    if (inEvent.data.hasOwnProperty(this.modelProperty)) {
      for (i = 0; i < this.$.customerAddrCityCombo.getCollection().length; i++) {
        if (this.$.customerAddrCityCombo.getCollection().models[i].get('id') === inEvent.data[this.modelProperty]) {
          this.$.customerAddrCityCombo.setSelected(i);
          break;
        }
      }
    }
  },
  retrieveValue: function (inSender, inEvent) {
    inEvent[this.modelProperty] = this.$.customerAddrCityCombo.getValue();
  },
  loadValue: function (inSender, inEvent) {
    this.$.customerAddrCityCombo.setCollection(this.collection);
   this.defaultValue(inEvent);
  },
  change: function () {},
  dataReadyFunction: function (data, inEvent) {
    var index = 0,
        result = null;
    if (this.destroyed) {
      return;
    }
    if (data) {
      data.models.unshift('');
      this.collection.reset(data.models);
    } else {
      this.collection.reset(null);
      return;
    }
    result = _.find(this.collection.models, function (categ) {
      if (!OB.UTIL.isNullOrUndefined(inEvent) && !OB.UTIL.isNullOrUndefined(inEvent.customerAddr) && !OB.UTIL.isNullOrUndefined(inEvent.customerAddr.get('cityName'))) {
        //Edit: select actual value
        if (categ.get(this.retrievedPropertyForText) === inEvent.customerAddr.get('cityName')) {
          return true;
        }
      } 
      index += 1;
    }, this);
    if (result) {
      this.$.customerAddrCityCombo.setSelected(index);
    } else {
      if(!OB.UTIL.isNullOrUndefined(inEvent) && !OB.UTIL.isNullOrUndefined(inEvent.customerAddr) && !OB.UTIL.isNullOrUndefined(inEvent.customerAddr.get('cityName')) ){
          this.$.customerAddrCityCombo.setSelected(inEvent.customerAddr.get('cityName'));
      } else {
          this.$.customerAddrCityCombo.setSelected(0);
      }
    }
  },
    
  saveChange: function (inSender, inEvent) {
    var selected = this.collection.at(this.$.customerAddrCityCombo.getSelected());
    if(!OB.UTIL.isNullOrUndefined (selected) && !OB.UTIL.isNullOrUndefined (selected.get('name')) && selected.get('name') != ''){
         inEvent.customerAddr.set('cityName', selected.get('name'));
    }else{
         OB.UTIL.showError(OB.I18N.getLabel('OBPOS_CityReqForBPAddress'));  
         inEvent.customerAddr.set('cityName', '');
    }
  },
  initComponents: function () {
    if (this.collectionName && OB && OB.Collection && OB.Collection[this.collectionName]) {
      this.collection = new OB.Collection[this.collectionName]();
    } else {
      OB.info('OB.UI.CustomerCityComboPropertyNew: Collection is required');
    }
    this.inherited(arguments);
    if (this.readOnly) {
      this.setAttribute('readonly', 'readonly');
    }
  }
});
 

OB.OBPOSPointOfSale.UI.customeraddr.edit_createcustomers_impl.prototype.newAttributes.unshift({
  kind: 'OB.UI.CustomerAddrComboProperty',
      name: 'customerAddrCountry',
      modelProperty: 'countryId',
      modelPropertyText: 'countryName',
      collectionName: 'CountryList',
      i18nLabel: 'OBPOS_LblCountry',
      defaultValue: function() {
        return OB.MobileApp.model.get('terminal').defaultbp_bpcountry;
      },
      //Default value for new lines
      retrievedPropertyForValue: 'id',
      //property of the retrieved model to get the value of the combo item
      retrievedPropertyForText: '_identifier',
      //property of the retrieved model to get the text of the combo item
      //function to retrieve the data
      fetchDataFunction: function(args) {
        var me = this,
          criteria;
        criteria = {
          _orderByClause: '_identifier asc'
        };
        OB.Dal.find(
          OB.Model.Country,
          criteria,
          function(data, args) {
            //This function must be called when the data is ready
            me.dataReadyFunction(data, args);
          },
          function(error) {
            OB.UTIL.showError(OB.I18N.getLabel('OBPOS_ErrorGettingCountries'));
            //This function must be called when the data is ready
            me.dataReadyFunction(null, args);
          },
          args
        );
      }
      
 });
 
 
OB.OBPOSPointOfSale.UI.customeraddr.edit_createcustomers_impl.prototype.newAttributes.unshift({
      kind: 'OB.UI.CustomerAddrTextProperty',
      name: 'customerAddrPostalCode',
      modelProperty: 'postalCode',
      i18nLabel: 'OBPOS_LblPostalCode',
      maxlength: 10
      
 });
 
 
 OB.OBPOSPointOfSale.UI.customeraddr.edit_createcustomers_impl.prototype.newAttributes.unshift({
   kind: 'OB.UI.CustomerAddrTextProperty',
      name: 'customerAddrName',
      modelProperty: 'name',
      i18nLabel: 'OBPOS_LblAddress',
      maxlength: 60,
      mandatory: true
      
 }); 
 
OB.OBPOSPointOfSale.UI.customeraddr.edit_createcustomers_impl.prototype.newAttributes.unshift({
  kind: 'OB.UI.CustomerAddrTextProperty',
      name: 'customerAddrCustomerName',
      modelProperty: 'customerName',
      i18nLabel: 'OBPOS_LblCustomer',
      readOnly: true
      
 });

OB.OBPOSPointOfSale.UI.customeraddr.edit_createcustomers_impl.prototype.newAttributes.find(item => item.name == 'customerAddrCountry').change = function() {
     this.owner.owner.owner.$.line_customerAddrCity.$.newAttribute.$.customerAddrCity.fetchDataFunction(null);
 };
 