/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB */

(function() {
	var promotionalBasis = OB.Data.ExtensibleModel
			.extend({
				modelName : 'PromotionalBasis',
				tableName : 'custsha_prombasis',
				entityName : 'custsha_prombasis',
				source : 'com.openbravo.sharaf.retail.customdevelopments.service.PromotionalBasis'
			});
	promotionalBasis.addProperties([ {
		name : 'id',
		column : 'id',
		primaryKey : true,
		type : 'TEXT'
	}, {
		name : 'PaymentmethodId',
		column : 'fINPaymentmethod',
		primaryKey : false,
		type : 'TEXT'
	}, {
		name : 'PromotionalBasis',
		column : 'promotionalBasis',
		type : 'TEXT'
	}, {
		name : 'client',
		column : 'client',
		type : 'TEXT'
	} ]);
	OB.Data.Registry.registerModel(promotionalBasis);
	OB.OBPOSPointOfSale.Model.PointOfSale.prototype.models
			.push(promotionalBasis);
})();
