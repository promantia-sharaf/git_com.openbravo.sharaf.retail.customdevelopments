/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB */

(function() {
  var transactioncount = OB.Data.ExtensibleModel.extend({
    modelName: 'CUSTSHA_transactioncount',
    tableName: 'CUSTSHA_transactioncount',
    entityName: 'custsha_transcount',
    source: '',
    local: true
  });
  transactioncount.addProperties([
	  {
      name: 'id',
      column: 'id',
      primaryKey: true,
      type: 'TEXT'
    },
    {
      name: 'posterminal',
      column: 'posterminal',
      type: 'TEXT'
    },
      {
      name: 'count',
      column: 'count',
      type: 'TEXT'
    }
    ]);
  OB.Data.Registry.registerModel(transactioncount);
  OB.MobileApp.model.get('dataSyncModels').push({
      model: OB.Model.CUSTSHA_transactioncount,
      criteria: {},
      className: 'com.openbravo.sharaf.retail.customdevelopments.CashUpLoader'
  });
})();
