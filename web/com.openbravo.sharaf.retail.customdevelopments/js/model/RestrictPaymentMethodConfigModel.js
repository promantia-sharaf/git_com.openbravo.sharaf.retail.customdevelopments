/**
 * 
 */

(function() {
  var cpmConfig = OB.Data.ExtensibleModel.extend({
    modelName: 'RestrictPaymentMethodConfig',
    tableName: 'CUSTSHA_PaymentMethod',
    entityName: 'CUSTSHA_PaymentMethod',
    source:
      'com.openbravo.sharaf.retail.customdevelopments.service.RestrictPaymentMethodConfigModel'
  });
  cpmConfig.addProperties([
    {
      name: 'id',
      column: 'id',
      primaryKey: true,
      type: 'TEXT'
    },
    {
      name: 'orgId',
      column: 'orgId',
      primaryKey: false,
      type: 'TEXT'
    },
    {
      name: 'clientId',
      column: 'clientId',
      type: 'TEXT'
    },
    {
        name: 'paymtId',
        column: 'paymtId',
        type: 'TEXT'
    },
    {
        name: 'productId',
        column: 'productId',
        type: 'TEXT'
    }
  ]);
  OB.Data.Registry.registerModel(cpmConfig);
  OB.OBPOSPointOfSale.Model.PointOfSale.prototype.models.push(cpmConfig);
})();
