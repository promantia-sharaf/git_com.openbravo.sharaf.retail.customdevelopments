/*
 ************************************************************************************
 * Copyright (C) 2017 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

OB.Model.Product.addProperties([{
  name: 'custshaExtdwtyEnabled',
  column: 'custshaExtdwtyEnabled',
  type: 'TEXT'
}, {
  name: 'custshaDgshieldEnabled',
  column: 'custshaDgshieldEnabled',
  type: 'TEXT'
},
{
  name: 'custshaDgspremiumEnabled',
  column: 'custshaDgspremiumEnabled',
  type: 'TEXT'
},
{
  name: 'custshaServiceType',
  column: 'custshaServiceType',
  type: 'TEXT'
},
{
  name: 'custshaApplecareEnabled',
  column: 'custshaApplecareEnabled',
  type: 'TEXT'
},
{
  name: 'custshaDgsplusEnabled',
  column: 'custshaDgsplusEnabled',
  type: 'TEXT'
},
{
  name: 'custshaEwdpdoctypeEnabled',
  column: 'custshaEwdpdoctypeEnabled',
  type: 'TEXT'
}
]);

