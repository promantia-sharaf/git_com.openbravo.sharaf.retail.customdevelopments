/*
 ************************************************************************************
 * Copyright (C) 2017-2018 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB, _ */

OB.Model.BusinessPartner.addProperties([{
  name: 'custshaDgpstatus',
  column: 'custshaDgpstatus',
  primaryKey: false,
  filter: false,
  type: 'TEXT'
}, {
  name: 'custshaBPCatManagescredit',
  column: 'custshaBPCatManagescredit',
  primaryKey: false,
  filter: false,
  type: 'BOOL'
}, {
	  name: 'cUSTSHADGInvoiceCount',
	  column: 'cUSTSHADGInvoiceCount',
	  primaryKey: false,
	  filter: false,
	  type: 'TEXT'
}, {
	  name: 'cUSTSHADGItemQtyCount',
	  column: 'cUSTSHADGItemQtyCount',
	  primaryKey: false,
	  filter: false,
	  type: 'TEXT'
},
{
      name: 'custshaCaptureoriginvoice',
      column: 'custshaCaptureoriginvoice',
      primaryKey: false,
      filter: false,
      type: 'TEXT'
}
]);

// Set mandatory field and default values for Sharaf DG
_.each(OB.OBPOSPointOfSale.UI.customers.edit_createcustomers_impl.prototype.newAttributes, function (prop) {
  if (prop.name === 'customerPhone' || prop.name === 'customerEmail' || prop.name === 'customerAddrCountry') {
    prop.mandatory = true;
  }
  if (prop.name === 'customerEmail') {
    prop.loadValue = function (inSender, inEvent) {
      if (inEvent.customer === undefined) {
        this.setValue('na@sharafdg.com');
      } else {
        this.setValue(inEvent.customer.get('email'));
      }
    };
  }
});

OB.OBPOSPointOfSale.UI.customers.newcustomer.extend({
  executeOnShown: function () {
    if (this.args.businessPartner) {
	    this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_customerLatLng.hide();
    	this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_customerAddrCountry.$.labelLine.hide();
    	this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_customerAddrCountry.$.newAttribute.$.customerAddrCountry.hide();
    	this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_customerPhone.$.labelLine.show();
    	this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_customerPhone.$.newAttribute.$.customerPhone.show();
        this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_customerPhone.$.newAttribute.$.customerPhone.setAttribute('readonly', 'readonly');
        // OBDEV-50 -- Tax ID field of business partner should not be editable
      if(OB.MobileApp.model.attributes.context.client.id !== 'C69FDC3AE9C745EAA52E10664AF9BABD' &&  OB.MobileApp.model.attributes.context.client.id !== '432A6BD8CD2944C4BFC2D8B3C1CB6292') {
        this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_customerTaxId.$.newAttribute.$.customerTaxId.setAttribute('readonly', 'readonly');
      } else {
        this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_customerTaxId.$.labelLine.setContent(OB.I18N.getLabel('SHFPT_CustomerVATIN'));
     }
        this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_firstName.$.newAttribute.$.firstName.setAttribute('readonly', 'readonly');
        this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_lastName.$.newAttribute.$.lastName.setAttribute('readonly', 'readonly');
        // OBDEV-253 
        if(!OB.UTIL.isNullOrUndefined(this.args.businessPartner.attributes.custshaLoyaltyCard) 
           && this.args.businessPartner.attributes.custshaLoyaltyCard !='') {
          this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_custshaLoyaltyCard.$.newAttribute.$.custshaLoyaltyCard.setAttribute('readonly', 'readonly');
        } else {
          this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_custshaLoyaltyCard.$.newAttribute.$.custshaLoyaltyCard.setAttribute('readonly', false);
        }
    } else {
	    this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_customerLatLng.show();
    //	this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_customerAddrCountry.$.newAttribute.$.customerAddrCountry.setAttribute('readonly', '');
    	this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_customerAddrCountry.$.labelLine.show();
    	this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_customerAddrCountry.$.newAttribute.$.customerAddrCountry.show();
    	this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_customerPhone.$.labelLine.hide();
    	this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_customerPhone.$.newAttribute.$.customerPhone.hide();
    	// OBDEV-50 -- Tax ID field of business partner should not be editable
      if(OB.MobileApp.model.attributes.context.client.id !== 'C69FDC3AE9C745EAA52E10664AF9BABD' &&  OB.MobileApp.model.attributes.context.client.id !== '432A6BD8CD2944C4BFC2D8B3C1CB6292') {
        this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_customerTaxId.$.newAttribute.$.customerTaxId.setAttribute('readonly', 'readonly');
      } else {
        this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_customerTaxId.$.labelLine.setContent(OB.I18N.getLabel('SHFPT_CustomerVATIN'));
      }
    	this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_firstName.$.newAttribute.$.firstName.setAttribute('readonly', false);
    	this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_lastName.$.newAttribute.$.lastName.setAttribute('readonly', false);
    	this.$.body.$.edit_createcustomers_impl.$.customerAttributes.$.line_custshaLoyaltyCard.$.newAttribute.$.custshaLoyaltyCard.setAttribute('readonly', false);
    }
  }
});

OB.OBPOSPointOfSale.UI.customers.editcustomer.extend({
  executeOnShown: function () {
    if (this.args.businessPartner) {
      if(OB.MobileApp.model.attributes.context.client.id === 'C69FDC3AE9C745EAA52E10664AF9BABD' || OB.MobileApp.model.attributes.context.client.id === '432A6BD8CD2944C4BFC2D8B3C1CB6292') {
        this.$.body.$.editcustomers_impl.$.customerAttributes.$.line_customerTaxId.$.labelLine.setContent(OB.I18N.getLabel('SHFPT_CustomerVATIN'));
     }
    } 
  }
});