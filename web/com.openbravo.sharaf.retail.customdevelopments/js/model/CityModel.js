/*
 ************************************************************************************
 * Copyright (C) 2021 Openbravo S.L.U.
 * Licensed under the Openbravo Commercial License version 1.0
 * You may obtain a copy of the License at http://www.openbravo.com/legal/obcl.html
 * or in the legal folder of this module distribution.
 ************************************************************************************
 */

/*global OB */

(function() {
  var city = OB.Data.ExtensibleModel.extend({
    modelName: 'City',
    tableName: 'custsha_city',
    entityName: 'custsha_city',
    source:
      'com.openbravo.sharaf.retail.customdevelopments.service.CityModel'
  });
  city.addProperties([
    {
      name: 'id',
      column: 'id',
      primaryKey: true,
      type: 'TEXT'
    },
    {
        name: 'name',
        column: 'name',
        type: 'TEXT'
      },
   {
      name: 'country',
      column: 'country',
      type: 'TEXT'
    },
    {
        name: 'client',
        column: 'client',
        type: 'TEXT'
    }
  ]);
  OB.Data.Registry.registerModel(city);
  OB.OBPOSPointOfSale.Model.PointOfSale.prototype.models.push(city);
})();
